--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3
-- Dumped by pg_dump version 16.3

-- Started on 2024-10-16 23:03:42

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 5263 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 17518)
-- Name: Estado; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Estado" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 216 (class 1259 OID 17521)
-- Name: Estado_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Estado_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5264 (class 0 OID 0)
-- Dependencies: 216
-- Name: Estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Estado_id_seq" OWNED BY public."Estado".id;


--
-- TOC entry 217 (class 1259 OID 17522)
-- Name: GrupoDestino; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."GrupoDestino" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 218 (class 1259 OID 17525)
-- Name: GrupoDestino_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."GrupoDestino_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5265 (class 0 OID 0)
-- Dependencies: 218
-- Name: GrupoDestino_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."GrupoDestino_id_seq" OWNED BY public."GrupoDestino".id;


--
-- TOC entry 219 (class 1259 OID 17526)
-- Name: animal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.animal (
    id bigint NOT NULL,
    "id_Animal" character varying(50) NOT NULL,
    fecha_nacimiento date NOT NULL,
    sexo character varying(1) NOT NULL,
    estado_id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    grupo_destino_id bigint NOT NULL,
    raza_id bigint NOT NULL
);


--
-- TOC entry 220 (class 1259 OID 17529)
-- Name: animal_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.animal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5266 (class 0 OID 0)
-- Dependencies: 220
-- Name: animal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.animal_id_seq OWNED BY public.animal.id;


--
-- TOC entry 221 (class 1259 OID 17530)
-- Name: animales_lote; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.animales_lote (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Lote_id" bigint NOT NULL
);


--
-- TOC entry 222 (class 1259 OID 17533)
-- Name: animales_lote_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.animales_lote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5267 (class 0 OID 0)
-- Dependencies: 222
-- Name: animales_lote_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.animales_lote_id_seq OWNED BY public.animales_lote.id;


--
-- TOC entry 281 (class 1259 OID 23484)
-- Name: aplicacion_tp; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.aplicacion_tp (
    id integer NOT NULL,
    "id_Animal_id" integer NOT NULL,
    "id_TPreventivo_id" integer NOT NULL,
    fecha_creacion date DEFAULT now() NOT NULL,
    "fechaInicio" date DEFAULT now() NOT NULL,
    "fechaFin" date NOT NULL,
    observacion character varying
);


--
-- TOC entry 280 (class 1259 OID 23483)
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.aplicacion_tp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5268 (class 0 OID 0)
-- Dependencies: 280
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.aplicacion_tp_id_seq OWNED BY public.aplicacion_tp.id;


--
-- TOC entry 223 (class 1259 OID 17538)
-- Name: articulo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.articulo (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    "cantidadActual" integer NOT NULL,
    "cantidadIdeal" integer NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 17541)
-- Name: articulo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.articulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5269 (class 0 OID 0)
-- Dependencies: 224
-- Name: articulo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.articulo_id_seq OWNED BY public.articulo.id;


--
-- TOC entry 225 (class 1259 OID 17542)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


--
-- TOC entry 226 (class 1259 OID 17545)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_group ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 227 (class 1259 OID 17546)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- TOC entry 228 (class 1259 OID 17549)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_group_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 229 (class 1259 OID 17550)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- TOC entry 230 (class 1259 OID 17553)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_permission ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 231 (class 1259 OID 17554)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


--
-- TOC entry 232 (class 1259 OID 17559)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 17562)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_user_groups ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 234 (class 1259 OID 17563)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 235 (class 1259 OID 17564)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 17567)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 237 (class 1259 OID 17568)
-- Name: compra; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.compra (
    id bigint NOT NULL,
    fecha_confirmado date,
    fecha_creacion date NOT NULL,
    estimacion_id bigint NOT NULL,
    estado character varying(1) DEFAULT 'P'::character varying NOT NULL,
    fecha_recibido date,
    vendedor_id bigint NOT NULL
);


--
-- TOC entry 238 (class 1259 OID 17571)
-- Name: compra_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5270 (class 0 OID 0)
-- Dependencies: 238
-- Name: compra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.compra_id_seq OWNED BY public.compra.id;


--
-- TOC entry 239 (class 1259 OID 17572)
-- Name: comprador; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.comprador (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    telefono character varying(50) NOT NULL,
    mail character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 240 (class 1259 OID 17575)
-- Name: comprador_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.comprador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5271 (class 0 OID 0)
-- Dependencies: 240
-- Name: comprador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.comprador_id_seq OWNED BY public.comprador.id;


--
-- TOC entry 241 (class 1259 OID 17576)
-- Name: detalle_compra; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.detalle_compra (
    id bigint NOT NULL,
    cantidad integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Articulo_id" bigint NOT NULL,
    "id_Compra_id" bigint NOT NULL
);


--
-- TOC entry 242 (class 1259 OID 17579)
-- Name: detalle_compra_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.detalle_compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5272 (class 0 OID 0)
-- Dependencies: 242
-- Name: detalle_compra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.detalle_compra_id_seq OWNED BY public.detalle_compra.id;


--
-- TOC entry 275 (class 1259 OID 18879)
-- Name: detalle_estimacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.detalle_estimacion (
    id bigint NOT NULL,
    cantidad integer NOT NULL,
    fecha_creacion date DEFAULT now() NOT NULL,
    "id_Articulo_id" bigint NOT NULL,
    "id_Estimacion_id" bigint NOT NULL,
    comprado integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 274 (class 1259 OID 18878)
-- Name: detalle_estimacion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.detalle_estimacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5273 (class 0 OID 0)
-- Dependencies: 274
-- Name: detalle_estimacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.detalle_estimacion_id_seq OWNED BY public.detalle_estimacion.id;


--
-- TOC entry 243 (class 1259 OID 17580)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


--
-- TOC entry 244 (class 1259 OID 17586)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.django_admin_log ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 245 (class 1259 OID 17587)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- TOC entry 246 (class 1259 OID 17590)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.django_content_type ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 247 (class 1259 OID 17591)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- TOC entry 248 (class 1259 OID 17596)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5274 (class 0 OID 0)
-- Dependencies: 248
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- TOC entry 249 (class 1259 OID 17597)
-- Name: django_session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


--
-- TOC entry 273 (class 1259 OID 18872)
-- Name: estimacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.estimacion (
    id bigint NOT NULL,
    "fechaEstimacion" date DEFAULT now() NOT NULL,
    fecha_creacion date DEFAULT now() NOT NULL
);


--
-- TOC entry 272 (class 1259 OID 18871)
-- Name: estimacion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.estimacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5275 (class 0 OID 0)
-- Dependencies: 272
-- Name: estimacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.estimacion_id_seq OWNED BY public.estimacion.id;


--
-- TOC entry 250 (class 1259 OID 17602)
-- Name: fallecimiento; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fallecimiento (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    fecha date NOT NULL
);


--
-- TOC entry 251 (class 1259 OID 17605)
-- Name: fallecimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fallecimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5276 (class 0 OID 0)
-- Dependencies: 251
-- Name: fallecimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fallecimiento_id_seq OWNED BY public.fallecimiento.id;


--
-- TOC entry 252 (class 1259 OID 17606)
-- Name: fecundacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fecundacion (
    id bigint NOT NULL,
    fecha date NOT NULL,
    id_madre_id bigint NOT NULL,
    id_padre_id bigint NOT NULL
);


--
-- TOC entry 253 (class 1259 OID 17609)
-- Name: lote_venta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lote_venta (
    id bigint NOT NULL,
    estado character varying(1) NOT NULL,
    fecha_envio date NOT NULL,
    fecha_confirmado date NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Comprador_id" bigint NOT NULL
);


--
-- TOC entry 254 (class 1259 OID 17612)
-- Name: lote_venta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.lote_venta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5277 (class 0 OID 0)
-- Dependencies: 254
-- Name: lote_venta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.lote_venta_id_seq OWNED BY public.lote_venta.id;


--
-- TOC entry 255 (class 1259 OID 17613)
-- Name: marcado; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.marcado (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


--
-- TOC entry 256 (class 1259 OID 17616)
-- Name: marcado_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.marcado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5278 (class 0 OID 0)
-- Dependencies: 256
-- Name: marcado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.marcado_id_seq OWNED BY public.marcado.id;


--
-- TOC entry 257 (class 1259 OID 17617)
-- Name: medida; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.medida (
    id bigint NOT NULL,
    "pesoKG" integer NOT NULL,
    "alturaCM" integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


--
-- TOC entry 258 (class 1259 OID 17620)
-- Name: medida_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.medida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5279 (class 0 OID 0)
-- Dependencies: 258
-- Name: medida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.medida_id_seq OWNED BY public.medida.id;


--
-- TOC entry 259 (class 1259 OID 17621)
-- Name: nacimiento; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nacimiento (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Fecundacion_id" bigint NOT NULL,
    fecha date NOT NULL
);


--
-- TOC entry 260 (class 1259 OID 17624)
-- Name: nacimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.nacimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5280 (class 0 OID 0)
-- Dependencies: 260
-- Name: nacimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.nacimiento_id_seq OWNED BY public.nacimiento.id;


--
-- TOC entry 261 (class 1259 OID 17625)
-- Name: palpacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.palpacion (
    id bigint NOT NULL,
    resultado character varying(1) NOT NULL,
    observacion character varying(100),
    fecha date NOT NULL,
    id_fecundacion_id bigint NOT NULL
);


--
-- TOC entry 262 (class 1259 OID 17628)
-- Name: palpacion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.palpacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5281 (class 0 OID 0)
-- Dependencies: 262
-- Name: palpacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.palpacion_id_seq OWNED BY public.palpacion.id;


--
-- TOC entry 263 (class 1259 OID 17629)
-- Name: perdida; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.perdida (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Palpacion_id" bigint NOT NULL,
    fecha date NOT NULL
);


--
-- TOC entry 264 (class 1259 OID 17632)
-- Name: perdida_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.perdida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5282 (class 0 OID 0)
-- Dependencies: 264
-- Name: perdida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.perdida_id_seq OWNED BY public.perdida.id;


--
-- TOC entry 265 (class 1259 OID 17633)
-- Name: raza; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.raza (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 266 (class 1259 OID 17636)
-- Name: raza_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.raza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5283 (class 0 OID 0)
-- Dependencies: 266
-- Name: raza_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.raza_id_seq OWNED BY public.raza.id;


--
-- TOC entry 267 (class 1259 OID 17637)
-- Name: tcurativo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tcurativo (
    id bigint NOT NULL,
    observacion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "fechaInicio" date NOT NULL,
    periodicidad integer NOT NULL,
    "fechaFin" date NOT NULL,
    nombre character varying(100) NOT NULL
);


--
-- TOC entry 268 (class 1259 OID 17640)
-- Name: tcurativo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tcurativo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5284 (class 0 OID 0)
-- Dependencies: 268
-- Name: tcurativo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tcurativo_id_seq OWNED BY public.tcurativo.id;


--
-- TOC entry 269 (class 1259 OID 17641)
-- Name: tipo_tprev; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tipo_tprev (
    id bigint NOT NULL,
    "nombreTipo" character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 270 (class 1259 OID 17644)
-- Name: tipo_tprev_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tipo_tprev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5285 (class 0 OID 0)
-- Dependencies: 270
-- Name: tipo_tprev_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tipo_tprev_id_seq OWNED BY public.tipo_tprev.id;


--
-- TOC entry 279 (class 1259 OID 23468)
-- Name: tpreventivo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tpreventivo (
    id integer NOT NULL,
    "id_TipoTPrev_id" integer NOT NULL,
    fecha_creacion date DEFAULT now() NOT NULL,
    nombre character varying NOT NULL,
    periodicidad integer DEFAULT 1 NOT NULL
);


--
-- TOC entry 278 (class 1259 OID 23467)
-- Name: tpreventivo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tpreventivo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5286 (class 0 OID 0)
-- Dependencies: 278
-- Name: tpreventivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tpreventivo_id_seq OWNED BY public.tpreventivo.id;


--
-- TOC entry 277 (class 1259 OID 19247)
-- Name: vendedor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vendedor (
    id bigint NOT NULL,
    nombre character varying(50) NOT NULL,
    telefono character varying(100) NOT NULL,
    mail character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


--
-- TOC entry 276 (class 1259 OID 19246)
-- Name: vendedor_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vendedor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5287 (class 0 OID 0)
-- Dependencies: 276
-- Name: vendedor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vendedor_id_seq OWNED BY public.vendedor.id;


--
-- TOC entry 271 (class 1259 OID 17649)
-- Name: vientre_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vientre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 5288 (class 0 OID 0)
-- Dependencies: 271
-- Name: vientre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vientre_id_seq OWNED BY public.fecundacion.id;


--
-- TOC entry 4852 (class 2604 OID 17650)
-- Name: Estado id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Estado" ALTER COLUMN id SET DEFAULT nextval('public."Estado_id_seq"'::regclass);


--
-- TOC entry 4853 (class 2604 OID 17651)
-- Name: GrupoDestino id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."GrupoDestino" ALTER COLUMN id SET DEFAULT nextval('public."GrupoDestino_id_seq"'::regclass);


--
-- TOC entry 4854 (class 2604 OID 17652)
-- Name: animal id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal ALTER COLUMN id SET DEFAULT nextval('public.animal_id_seq'::regclass);


--
-- TOC entry 4855 (class 2604 OID 17653)
-- Name: animales_lote id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animales_lote ALTER COLUMN id SET DEFAULT nextval('public.animales_lote_id_seq'::regclass);


--
-- TOC entry 4883 (class 2604 OID 23487)
-- Name: aplicacion_tp id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aplicacion_tp ALTER COLUMN id SET DEFAULT nextval('public.aplicacion_tp_id_seq'::regclass);


--
-- TOC entry 4856 (class 2604 OID 17655)
-- Name: articulo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articulo ALTER COLUMN id SET DEFAULT nextval('public.articulo_id_seq'::regclass);


--
-- TOC entry 4857 (class 2604 OID 17656)
-- Name: compra id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.compra ALTER COLUMN id SET DEFAULT nextval('public.compra_id_seq'::regclass);


--
-- TOC entry 4859 (class 2604 OID 17657)
-- Name: comprador id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comprador ALTER COLUMN id SET DEFAULT nextval('public.comprador_id_seq'::regclass);


--
-- TOC entry 4860 (class 2604 OID 17658)
-- Name: detalle_compra id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_compra ALTER COLUMN id SET DEFAULT nextval('public.detalle_compra_id_seq'::regclass);


--
-- TOC entry 4876 (class 2604 OID 18882)
-- Name: detalle_estimacion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_estimacion ALTER COLUMN id SET DEFAULT nextval('public.detalle_estimacion_id_seq'::regclass);


--
-- TOC entry 4861 (class 2604 OID 17659)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- TOC entry 4873 (class 2604 OID 18875)
-- Name: estimacion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estimacion ALTER COLUMN id SET DEFAULT nextval('public.estimacion_id_seq'::regclass);


--
-- TOC entry 4862 (class 2604 OID 17660)
-- Name: fallecimiento id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fallecimiento ALTER COLUMN id SET DEFAULT nextval('public.fallecimiento_id_seq'::regclass);


--
-- TOC entry 4863 (class 2604 OID 17661)
-- Name: fecundacion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fecundacion ALTER COLUMN id SET DEFAULT nextval('public.vientre_id_seq'::regclass);


--
-- TOC entry 4864 (class 2604 OID 17662)
-- Name: lote_venta id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lote_venta ALTER COLUMN id SET DEFAULT nextval('public.lote_venta_id_seq'::regclass);


--
-- TOC entry 4865 (class 2604 OID 17663)
-- Name: marcado id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.marcado ALTER COLUMN id SET DEFAULT nextval('public.marcado_id_seq'::regclass);


--
-- TOC entry 4866 (class 2604 OID 17664)
-- Name: medida id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medida ALTER COLUMN id SET DEFAULT nextval('public.medida_id_seq'::regclass);


--
-- TOC entry 4867 (class 2604 OID 17665)
-- Name: nacimiento id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nacimiento ALTER COLUMN id SET DEFAULT nextval('public.nacimiento_id_seq'::regclass);


--
-- TOC entry 4868 (class 2604 OID 17666)
-- Name: palpacion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.palpacion ALTER COLUMN id SET DEFAULT nextval('public.palpacion_id_seq'::regclass);


--
-- TOC entry 4869 (class 2604 OID 17667)
-- Name: perdida id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perdida ALTER COLUMN id SET DEFAULT nextval('public.perdida_id_seq'::regclass);


--
-- TOC entry 4870 (class 2604 OID 17668)
-- Name: raza id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raza ALTER COLUMN id SET DEFAULT nextval('public.raza_id_seq'::regclass);


--
-- TOC entry 4871 (class 2604 OID 17669)
-- Name: tcurativo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tcurativo ALTER COLUMN id SET DEFAULT nextval('public.tcurativo_id_seq'::regclass);


--
-- TOC entry 4872 (class 2604 OID 17670)
-- Name: tipo_tprev id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tipo_tprev ALTER COLUMN id SET DEFAULT nextval('public.tipo_tprev_id_seq'::regclass);


--
-- TOC entry 4880 (class 2604 OID 23471)
-- Name: tpreventivo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tpreventivo ALTER COLUMN id SET DEFAULT nextval('public.tpreventivo_id_seq'::regclass);


--
-- TOC entry 4879 (class 2604 OID 19250)
-- Name: vendedor id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vendedor ALTER COLUMN id SET DEFAULT nextval('public.vendedor_id_seq'::regclass);


--
-- TOC entry 5191 (class 0 OID 17518)
-- Dependencies: 215
-- Data for Name: Estado; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public."Estado" VALUES (1, 'Vivo', '2023-03-30');
INSERT INTO public."Estado" VALUES (2, 'Muerto', '2023-03-30');
INSERT INTO public."Estado" VALUES (3, 'Vendido', '2023-03-30');


--
-- TOC entry 5193 (class 0 OID 17522)
-- Dependencies: 217
-- Data for Name: GrupoDestino; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public."GrupoDestino" VALUES (1, 'Engorde', '2023-03-30');
INSERT INTO public."GrupoDestino" VALUES (2, 'Reproducción', '2023-03-30');


--
-- TOC entry 5195 (class 0 OID 17526)
-- Dependencies: 219
-- Data for Name: animal; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.animal VALUES (2, 'M001', '2020-03-30', 'M', 1, '2023-03-30', 2, 1);
INSERT INTO public.animal VALUES (3, 'AAAO', '2023-09-26', 'F', 1, '2023-09-26', 1, 4);
INSERT INTO public.animal VALUES (4, 'M002', '2021-03-30', 'M', 1, '2023-03-30', 2, 1);
INSERT INTO public.animal VALUES (1, 'F001', '2020-03-30', 'F', 1, '2023-03-30', 2, 1);


--
-- TOC entry 5197 (class 0 OID 17530)
-- Dependencies: 221
-- Data for Name: animales_lote; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5257 (class 0 OID 23484)
-- Dependencies: 281
-- Data for Name: aplicacion_tp; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.aplicacion_tp VALUES (1, 1, 1, '2024-10-17', '2024-11-01', '2024-11-30', '');


--
-- TOC entry 5199 (class 0 OID 17538)
-- Dependencies: 223
-- Data for Name: articulo; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.articulo VALUES (1, 'Forraje', 10, 60, '2023-05-18');


--
-- TOC entry 5201 (class 0 OID 17542)
-- Dependencies: 225
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.auth_group VALUES (1, 'Administrador');
INSERT INTO public.auth_group VALUES (2, 'Cajero');
INSERT INTO public.auth_group VALUES (3, 'Compra');
INSERT INTO public.auth_group VALUES (4, 'Animal');


--
-- TOC entry 5203 (class 0 OID 17546)
-- Dependencies: 227
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.auth_group_permissions VALUES (1, 1, 1);
INSERT INTO public.auth_group_permissions VALUES (2, 1, 2);
INSERT INTO public.auth_group_permissions VALUES (3, 1, 3);
INSERT INTO public.auth_group_permissions VALUES (4, 1, 4);
INSERT INTO public.auth_group_permissions VALUES (5, 1, 5);
INSERT INTO public.auth_group_permissions VALUES (6, 1, 6);
INSERT INTO public.auth_group_permissions VALUES (7, 1, 7);
INSERT INTO public.auth_group_permissions VALUES (8, 1, 8);
INSERT INTO public.auth_group_permissions VALUES (9, 1, 9);
INSERT INTO public.auth_group_permissions VALUES (10, 1, 10);
INSERT INTO public.auth_group_permissions VALUES (11, 1, 11);
INSERT INTO public.auth_group_permissions VALUES (12, 1, 12);
INSERT INTO public.auth_group_permissions VALUES (13, 1, 13);
INSERT INTO public.auth_group_permissions VALUES (14, 1, 14);
INSERT INTO public.auth_group_permissions VALUES (15, 1, 15);
INSERT INTO public.auth_group_permissions VALUES (16, 1, 16);
INSERT INTO public.auth_group_permissions VALUES (17, 1, 17);
INSERT INTO public.auth_group_permissions VALUES (18, 1, 18);
INSERT INTO public.auth_group_permissions VALUES (19, 1, 19);
INSERT INTO public.auth_group_permissions VALUES (20, 1, 20);
INSERT INTO public.auth_group_permissions VALUES (21, 1, 21);
INSERT INTO public.auth_group_permissions VALUES (22, 1, 22);
INSERT INTO public.auth_group_permissions VALUES (23, 1, 23);
INSERT INTO public.auth_group_permissions VALUES (24, 1, 24);
INSERT INTO public.auth_group_permissions VALUES (25, 1, 25);
INSERT INTO public.auth_group_permissions VALUES (26, 1, 26);
INSERT INTO public.auth_group_permissions VALUES (27, 1, 27);
INSERT INTO public.auth_group_permissions VALUES (28, 1, 28);
INSERT INTO public.auth_group_permissions VALUES (29, 1, 29);
INSERT INTO public.auth_group_permissions VALUES (30, 1, 30);
INSERT INTO public.auth_group_permissions VALUES (31, 1, 31);
INSERT INTO public.auth_group_permissions VALUES (32, 1, 32);
INSERT INTO public.auth_group_permissions VALUES (33, 1, 33);
INSERT INTO public.auth_group_permissions VALUES (34, 1, 34);
INSERT INTO public.auth_group_permissions VALUES (35, 1, 35);
INSERT INTO public.auth_group_permissions VALUES (36, 1, 36);
INSERT INTO public.auth_group_permissions VALUES (37, 1, 37);
INSERT INTO public.auth_group_permissions VALUES (38, 1, 38);
INSERT INTO public.auth_group_permissions VALUES (39, 1, 39);
INSERT INTO public.auth_group_permissions VALUES (40, 1, 40);
INSERT INTO public.auth_group_permissions VALUES (41, 1, 41);
INSERT INTO public.auth_group_permissions VALUES (42, 1, 42);
INSERT INTO public.auth_group_permissions VALUES (43, 1, 43);
INSERT INTO public.auth_group_permissions VALUES (44, 1, 44);
INSERT INTO public.auth_group_permissions VALUES (45, 1, 45);
INSERT INTO public.auth_group_permissions VALUES (46, 1, 46);
INSERT INTO public.auth_group_permissions VALUES (47, 1, 47);
INSERT INTO public.auth_group_permissions VALUES (48, 1, 48);
INSERT INTO public.auth_group_permissions VALUES (49, 1, 49);
INSERT INTO public.auth_group_permissions VALUES (50, 1, 50);
INSERT INTO public.auth_group_permissions VALUES (51, 1, 51);
INSERT INTO public.auth_group_permissions VALUES (52, 1, 52);
INSERT INTO public.auth_group_permissions VALUES (53, 1, 53);
INSERT INTO public.auth_group_permissions VALUES (54, 1, 54);
INSERT INTO public.auth_group_permissions VALUES (55, 1, 55);
INSERT INTO public.auth_group_permissions VALUES (56, 1, 56);
INSERT INTO public.auth_group_permissions VALUES (57, 1, 57);
INSERT INTO public.auth_group_permissions VALUES (58, 1, 58);
INSERT INTO public.auth_group_permissions VALUES (59, 1, 59);
INSERT INTO public.auth_group_permissions VALUES (60, 1, 60);
INSERT INTO public.auth_group_permissions VALUES (61, 1, 61);
INSERT INTO public.auth_group_permissions VALUES (62, 1, 62);
INSERT INTO public.auth_group_permissions VALUES (63, 1, 63);
INSERT INTO public.auth_group_permissions VALUES (64, 1, 64);
INSERT INTO public.auth_group_permissions VALUES (65, 1, 65);
INSERT INTO public.auth_group_permissions VALUES (66, 1, 66);
INSERT INTO public.auth_group_permissions VALUES (67, 1, 67);
INSERT INTO public.auth_group_permissions VALUES (68, 1, 68);
INSERT INTO public.auth_group_permissions VALUES (69, 1, 69);
INSERT INTO public.auth_group_permissions VALUES (70, 1, 70);
INSERT INTO public.auth_group_permissions VALUES (71, 1, 71);
INSERT INTO public.auth_group_permissions VALUES (72, 1, 72);
INSERT INTO public.auth_group_permissions VALUES (73, 1, 73);
INSERT INTO public.auth_group_permissions VALUES (74, 1, 74);
INSERT INTO public.auth_group_permissions VALUES (75, 1, 75);
INSERT INTO public.auth_group_permissions VALUES (76, 1, 76);
INSERT INTO public.auth_group_permissions VALUES (77, 1, 77);
INSERT INTO public.auth_group_permissions VALUES (78, 1, 78);
INSERT INTO public.auth_group_permissions VALUES (79, 1, 79);
INSERT INTO public.auth_group_permissions VALUES (80, 1, 80);
INSERT INTO public.auth_group_permissions VALUES (81, 1, 81);
INSERT INTO public.auth_group_permissions VALUES (82, 1, 82);
INSERT INTO public.auth_group_permissions VALUES (83, 1, 83);
INSERT INTO public.auth_group_permissions VALUES (84, 1, 84);
INSERT INTO public.auth_group_permissions VALUES (85, 1, 85);
INSERT INTO public.auth_group_permissions VALUES (86, 1, 86);
INSERT INTO public.auth_group_permissions VALUES (87, 1, 87);
INSERT INTO public.auth_group_permissions VALUES (88, 1, 88);
INSERT INTO public.auth_group_permissions VALUES (89, 1, 89);
INSERT INTO public.auth_group_permissions VALUES (90, 1, 90);
INSERT INTO public.auth_group_permissions VALUES (91, 1, 91);
INSERT INTO public.auth_group_permissions VALUES (92, 1, 92);
INSERT INTO public.auth_group_permissions VALUES (93, 1, 93);
INSERT INTO public.auth_group_permissions VALUES (94, 1, 94);
INSERT INTO public.auth_group_permissions VALUES (95, 1, 95);
INSERT INTO public.auth_group_permissions VALUES (96, 1, 96);
INSERT INTO public.auth_group_permissions VALUES (97, 1, 97);
INSERT INTO public.auth_group_permissions VALUES (98, 1, 98);
INSERT INTO public.auth_group_permissions VALUES (99, 1, 99);
INSERT INTO public.auth_group_permissions VALUES (100, 1, 100);
INSERT INTO public.auth_group_permissions VALUES (101, 1, 101);
INSERT INTO public.auth_group_permissions VALUES (102, 1, 102);
INSERT INTO public.auth_group_permissions VALUES (103, 1, 103);
INSERT INTO public.auth_group_permissions VALUES (104, 1, 104);
INSERT INTO public.auth_group_permissions VALUES (105, 1, 105);
INSERT INTO public.auth_group_permissions VALUES (106, 1, 106);
INSERT INTO public.auth_group_permissions VALUES (107, 1, 107);
INSERT INTO public.auth_group_permissions VALUES (108, 1, 108);
INSERT INTO public.auth_group_permissions VALUES (109, 3, 97);
INSERT INTO public.auth_group_permissions VALUES (110, 3, 98);
INSERT INTO public.auth_group_permissions VALUES (111, 3, 99);
INSERT INTO public.auth_group_permissions VALUES (112, 3, 100);
INSERT INTO public.auth_group_permissions VALUES (113, 3, 101);
INSERT INTO public.auth_group_permissions VALUES (114, 3, 102);
INSERT INTO public.auth_group_permissions VALUES (115, 3, 103);
INSERT INTO public.auth_group_permissions VALUES (116, 3, 104);
INSERT INTO public.auth_group_permissions VALUES (117, 3, 105);
INSERT INTO public.auth_group_permissions VALUES (118, 3, 106);
INSERT INTO public.auth_group_permissions VALUES (119, 3, 107);
INSERT INTO public.auth_group_permissions VALUES (120, 3, 108);
INSERT INTO public.auth_group_permissions VALUES (121, 3, 85);
INSERT INTO public.auth_group_permissions VALUES (122, 3, 86);
INSERT INTO public.auth_group_permissions VALUES (123, 3, 87);
INSERT INTO public.auth_group_permissions VALUES (124, 3, 88);
INSERT INTO public.auth_group_permissions VALUES (125, 4, 25);
INSERT INTO public.auth_group_permissions VALUES (126, 4, 26);
INSERT INTO public.auth_group_permissions VALUES (127, 4, 27);
INSERT INTO public.auth_group_permissions VALUES (128, 4, 28);
INSERT INTO public.auth_group_permissions VALUES (129, 4, 29);
INSERT INTO public.auth_group_permissions VALUES (130, 4, 30);
INSERT INTO public.auth_group_permissions VALUES (131, 4, 31);
INSERT INTO public.auth_group_permissions VALUES (132, 4, 32);
INSERT INTO public.auth_group_permissions VALUES (133, 4, 33);
INSERT INTO public.auth_group_permissions VALUES (134, 4, 34);
INSERT INTO public.auth_group_permissions VALUES (135, 4, 35);
INSERT INTO public.auth_group_permissions VALUES (136, 4, 36);
INSERT INTO public.auth_group_permissions VALUES (137, 4, 37);
INSERT INTO public.auth_group_permissions VALUES (138, 4, 38);
INSERT INTO public.auth_group_permissions VALUES (139, 4, 39);
INSERT INTO public.auth_group_permissions VALUES (140, 4, 40);
INSERT INTO public.auth_group_permissions VALUES (141, 4, 41);
INSERT INTO public.auth_group_permissions VALUES (142, 4, 42);
INSERT INTO public.auth_group_permissions VALUES (143, 4, 43);
INSERT INTO public.auth_group_permissions VALUES (144, 4, 44);
INSERT INTO public.auth_group_permissions VALUES (145, 4, 45);
INSERT INTO public.auth_group_permissions VALUES (146, 4, 46);
INSERT INTO public.auth_group_permissions VALUES (147, 4, 47);
INSERT INTO public.auth_group_permissions VALUES (148, 4, 48);
INSERT INTO public.auth_group_permissions VALUES (149, 4, 49);
INSERT INTO public.auth_group_permissions VALUES (150, 4, 50);
INSERT INTO public.auth_group_permissions VALUES (151, 4, 51);
INSERT INTO public.auth_group_permissions VALUES (152, 4, 52);
INSERT INTO public.auth_group_permissions VALUES (153, 4, 53);
INSERT INTO public.auth_group_permissions VALUES (154, 4, 54);
INSERT INTO public.auth_group_permissions VALUES (155, 4, 55);
INSERT INTO public.auth_group_permissions VALUES (156, 4, 56);
INSERT INTO public.auth_group_permissions VALUES (157, 4, 57);
INSERT INTO public.auth_group_permissions VALUES (158, 4, 58);
INSERT INTO public.auth_group_permissions VALUES (159, 4, 59);
INSERT INTO public.auth_group_permissions VALUES (160, 4, 60);
INSERT INTO public.auth_group_permissions VALUES (161, 4, 61);
INSERT INTO public.auth_group_permissions VALUES (162, 4, 62);
INSERT INTO public.auth_group_permissions VALUES (163, 4, 63);
INSERT INTO public.auth_group_permissions VALUES (164, 4, 64);
INSERT INTO public.auth_group_permissions VALUES (165, 4, 65);
INSERT INTO public.auth_group_permissions VALUES (166, 4, 66);
INSERT INTO public.auth_group_permissions VALUES (167, 4, 67);
INSERT INTO public.auth_group_permissions VALUES (168, 4, 68);
INSERT INTO public.auth_group_permissions VALUES (169, 4, 69);
INSERT INTO public.auth_group_permissions VALUES (170, 4, 70);
INSERT INTO public.auth_group_permissions VALUES (171, 4, 71);
INSERT INTO public.auth_group_permissions VALUES (172, 4, 72);
INSERT INTO public.auth_group_permissions VALUES (173, 4, 73);
INSERT INTO public.auth_group_permissions VALUES (174, 4, 74);
INSERT INTO public.auth_group_permissions VALUES (175, 4, 75);
INSERT INTO public.auth_group_permissions VALUES (176, 4, 76);
INSERT INTO public.auth_group_permissions VALUES (177, 4, 77);
INSERT INTO public.auth_group_permissions VALUES (178, 4, 78);
INSERT INTO public.auth_group_permissions VALUES (179, 4, 79);
INSERT INTO public.auth_group_permissions VALUES (180, 4, 80);
INSERT INTO public.auth_group_permissions VALUES (181, 4, 81);
INSERT INTO public.auth_group_permissions VALUES (182, 4, 82);
INSERT INTO public.auth_group_permissions VALUES (183, 4, 83);
INSERT INTO public.auth_group_permissions VALUES (184, 4, 84);
INSERT INTO public.auth_group_permissions VALUES (185, 4, 89);
INSERT INTO public.auth_group_permissions VALUES (186, 4, 90);
INSERT INTO public.auth_group_permissions VALUES (187, 4, 91);
INSERT INTO public.auth_group_permissions VALUES (188, 4, 92);
INSERT INTO public.auth_group_permissions VALUES (189, 4, 93);
INSERT INTO public.auth_group_permissions VALUES (190, 4, 94);
INSERT INTO public.auth_group_permissions VALUES (191, 4, 95);
INSERT INTO public.auth_group_permissions VALUES (192, 4, 96);


--
-- TOC entry 5205 (class 0 OID 17550)
-- Dependencies: 229
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.auth_permission VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO public.auth_permission VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO public.auth_permission VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO public.auth_permission VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO public.auth_permission VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO public.auth_permission VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO public.auth_permission VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO public.auth_permission VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO public.auth_permission VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO public.auth_permission VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO public.auth_permission VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO public.auth_permission VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO public.auth_permission VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO public.auth_permission VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO public.auth_permission VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO public.auth_permission VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO public.auth_permission VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO public.auth_permission VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO public.auth_permission VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO public.auth_permission VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO public.auth_permission VALUES (21, 'Can add session', 6, 'add_session');
INSERT INTO public.auth_permission VALUES (22, 'Can change session', 6, 'change_session');
INSERT INTO public.auth_permission VALUES (23, 'Can delete session', 6, 'delete_session');
INSERT INTO public.auth_permission VALUES (24, 'Can view session', 6, 'view_session');
INSERT INTO public.auth_permission VALUES (25, 'Can add Raza', 7, 'add_raza');
INSERT INTO public.auth_permission VALUES (26, 'Can change Raza', 7, 'change_raza');
INSERT INTO public.auth_permission VALUES (27, 'Can delete Raza', 7, 'delete_raza');
INSERT INTO public.auth_permission VALUES (28, 'Can view Raza', 7, 'view_raza');
INSERT INTO public.auth_permission VALUES (29, 'Can add GrupoDestino', 8, 'add_grupodestino');
INSERT INTO public.auth_permission VALUES (30, 'Can change GrupoDestino', 8, 'change_grupodestino');
INSERT INTO public.auth_permission VALUES (31, 'Can delete GrupoDestino', 8, 'delete_grupodestino');
INSERT INTO public.auth_permission VALUES (32, 'Can view GrupoDestino', 8, 'view_grupodestino');
INSERT INTO public.auth_permission VALUES (33, 'Can add Estado', 9, 'add_estado');
INSERT INTO public.auth_permission VALUES (34, 'Can change Estado', 9, 'change_estado');
INSERT INTO public.auth_permission VALUES (35, 'Can delete Estado', 9, 'delete_estado');
INSERT INTO public.auth_permission VALUES (36, 'Can view Estado', 9, 'view_estado');
INSERT INTO public.auth_permission VALUES (37, 'Can add Animal', 10, 'add_animal');
INSERT INTO public.auth_permission VALUES (38, 'Can change Animal', 10, 'change_animal');
INSERT INTO public.auth_permission VALUES (39, 'Can delete Animal', 10, 'delete_animal');
INSERT INTO public.auth_permission VALUES (40, 'Can view Animal', 10, 'view_animal');
INSERT INTO public.auth_permission VALUES (41, 'Can add Vientre', 11, 'add_vientre');
INSERT INTO public.auth_permission VALUES (42, 'Can change Vientre', 11, 'change_vientre');
INSERT INTO public.auth_permission VALUES (43, 'Can delete Vientre', 11, 'delete_vientre');
INSERT INTO public.auth_permission VALUES (44, 'Can view Vientre', 11, 'view_vientre');
INSERT INTO public.auth_permission VALUES (45, 'Can add Palapcion', 12, 'add_palpacion');
INSERT INTO public.auth_permission VALUES (46, 'Can change Palapcion', 12, 'change_palpacion');
INSERT INTO public.auth_permission VALUES (47, 'Can delete Palapcion', 12, 'delete_palpacion');
INSERT INTO public.auth_permission VALUES (48, 'Can view Palapcion', 12, 'view_palpacion');
INSERT INTO public.auth_permission VALUES (49, 'Can add Perdida', 13, 'add_perdida');
INSERT INTO public.auth_permission VALUES (50, 'Can change Perdida', 13, 'change_perdida');
INSERT INTO public.auth_permission VALUES (51, 'Can delete Perdida', 13, 'delete_perdida');
INSERT INTO public.auth_permission VALUES (52, 'Can view Perdida', 13, 'view_perdida');
INSERT INTO public.auth_permission VALUES (53, 'Can add Nacimiento', 14, 'add_nacimiento');
INSERT INTO public.auth_permission VALUES (54, 'Can change Nacimiento', 14, 'change_nacimiento');
INSERT INTO public.auth_permission VALUES (55, 'Can delete Nacimiento', 14, 'delete_nacimiento');
INSERT INTO public.auth_permission VALUES (56, 'Can view Nacimiento', 14, 'view_nacimiento');
INSERT INTO public.auth_permission VALUES (57, 'Can add Fallecimiento', 15, 'add_fallecimiento');
INSERT INTO public.auth_permission VALUES (58, 'Can change Fallecimiento', 15, 'change_fallecimiento');
INSERT INTO public.auth_permission VALUES (59, 'Can delete Fallecimiento', 15, 'delete_fallecimiento');
INSERT INTO public.auth_permission VALUES (60, 'Can view Fallecimiento', 15, 'view_fallecimiento');
INSERT INTO public.auth_permission VALUES (61, 'Can add Marcado', 16, 'add_marcado');
INSERT INTO public.auth_permission VALUES (62, 'Can change Marcado', 16, 'change_marcado');
INSERT INTO public.auth_permission VALUES (63, 'Can delete Marcado', 16, 'delete_marcado');
INSERT INTO public.auth_permission VALUES (64, 'Can view Marcado', 16, 'view_marcado');
INSERT INTO public.auth_permission VALUES (65, 'Can add Medida', 17, 'add_medida');
INSERT INTO public.auth_permission VALUES (66, 'Can change Medida', 17, 'change_medida');
INSERT INTO public.auth_permission VALUES (67, 'Can delete Medida', 17, 'delete_medida');
INSERT INTO public.auth_permission VALUES (68, 'Can view Medida', 17, 'view_medida');
INSERT INTO public.auth_permission VALUES (69, 'Can add TipoTPrev', 18, 'add_tipotprev');
INSERT INTO public.auth_permission VALUES (70, 'Can change TipoTPrev', 18, 'change_tipotprev');
INSERT INTO public.auth_permission VALUES (71, 'Can delete TipoTPrev', 18, 'delete_tipotprev');
INSERT INTO public.auth_permission VALUES (72, 'Can view TipoTPrev', 18, 'view_tipotprev');
INSERT INTO public.auth_permission VALUES (73, 'Can add TCurativo', 19, 'add_tcurativo');
INSERT INTO public.auth_permission VALUES (74, 'Can change TCurativo', 19, 'change_tcurativo');
INSERT INTO public.auth_permission VALUES (75, 'Can delete TCurativo', 19, 'delete_tcurativo');
INSERT INTO public.auth_permission VALUES (76, 'Can view TCurativo', 19, 'view_tcurativo');
INSERT INTO public.auth_permission VALUES (77, 'Can add TPreventivo', 20, 'add_tpreventivo');
INSERT INTO public.auth_permission VALUES (78, 'Can change TPreventivo', 20, 'change_tpreventivo');
INSERT INTO public.auth_permission VALUES (79, 'Can delete TPreventivo', 20, 'delete_tpreventivo');
INSERT INTO public.auth_permission VALUES (80, 'Can view TPreventivo', 20, 'view_tpreventivo');
INSERT INTO public.auth_permission VALUES (81, 'Can add AplicacionTP', 21, 'add_aplicaciontp');
INSERT INTO public.auth_permission VALUES (82, 'Can change AplicacionTP', 21, 'change_aplicaciontp');
INSERT INTO public.auth_permission VALUES (83, 'Can delete AplicacionTP', 21, 'delete_aplicaciontp');
INSERT INTO public.auth_permission VALUES (84, 'Can view AplicacionTP', 21, 'view_aplicaciontp');
INSERT INTO public.auth_permission VALUES (85, 'Can add Comprador', 22, 'add_comprador');
INSERT INTO public.auth_permission VALUES (86, 'Can change Comprador', 22, 'change_comprador');
INSERT INTO public.auth_permission VALUES (87, 'Can delete Comprador', 22, 'delete_comprador');
INSERT INTO public.auth_permission VALUES (88, 'Can view Comprador', 22, 'view_comprador');
INSERT INTO public.auth_permission VALUES (89, 'Can add LoteVenta', 23, 'add_loteventa');
INSERT INTO public.auth_permission VALUES (90, 'Can change LoteVenta', 23, 'change_loteventa');
INSERT INTO public.auth_permission VALUES (91, 'Can delete LoteVenta', 23, 'delete_loteventa');
INSERT INTO public.auth_permission VALUES (92, 'Can view LoteVenta', 23, 'view_loteventa');
INSERT INTO public.auth_permission VALUES (93, 'Can add AnimalesLote', 24, 'add_animaleslote');
INSERT INTO public.auth_permission VALUES (94, 'Can change AnimalesLote', 24, 'change_animaleslote');
INSERT INTO public.auth_permission VALUES (95, 'Can delete AnimalesLote', 24, 'delete_animaleslote');
INSERT INTO public.auth_permission VALUES (96, 'Can view AnimalesLote', 24, 'view_animaleslote');
INSERT INTO public.auth_permission VALUES (97, 'Can add Articulo', 25, 'add_articulo');
INSERT INTO public.auth_permission VALUES (98, 'Can change Articulo', 25, 'change_articulo');
INSERT INTO public.auth_permission VALUES (99, 'Can delete Articulo', 25, 'delete_articulo');
INSERT INTO public.auth_permission VALUES (100, 'Can view Articulo', 25, 'view_articulo');
INSERT INTO public.auth_permission VALUES (101, 'Can add Compra', 26, 'add_compra');
INSERT INTO public.auth_permission VALUES (102, 'Can change Compra', 26, 'change_compra');
INSERT INTO public.auth_permission VALUES (103, 'Can delete Compra', 26, 'delete_compra');
INSERT INTO public.auth_permission VALUES (104, 'Can view Compra', 26, 'view_compra');
INSERT INTO public.auth_permission VALUES (105, 'Can add DetalleCompra', 27, 'add_detallecompra');
INSERT INTO public.auth_permission VALUES (106, 'Can change DetalleCompra', 27, 'change_detallecompra');
INSERT INTO public.auth_permission VALUES (107, 'Can delete DetalleCompra', 27, 'delete_detallecompra');
INSERT INTO public.auth_permission VALUES (108, 'Can view DetalleCompra', 27, 'view_detallecompra');
INSERT INTO public.auth_permission VALUES (109, 'Can add Fecundacion', 28, 'add_fecundacion');
INSERT INTO public.auth_permission VALUES (110, 'Can change Fecundacion', 28, 'change_fecundacion');
INSERT INTO public.auth_permission VALUES (111, 'Can delete Fecundacion', 28, 'delete_fecundacion');
INSERT INTO public.auth_permission VALUES (112, 'Can view Fecundacion', 28, 'view_fecundacion');
INSERT INTO public.auth_permission VALUES (113, 'Can add Estimación', 29, 'add_estimacion');
INSERT INTO public.auth_permission VALUES (114, 'Can change Estimación', 29, 'change_estimacion');
INSERT INTO public.auth_permission VALUES (115, 'Can delete Estimación', 29, 'delete_estimacion');
INSERT INTO public.auth_permission VALUES (116, 'Can view Estimación', 29, 'view_estimacion');
INSERT INTO public.auth_permission VALUES (117, 'Can add DetalleEstimacion', 30, 'add_detalleestimacion');
INSERT INTO public.auth_permission VALUES (118, 'Can change DetalleEstimacion', 30, 'change_detalleestimacion');
INSERT INTO public.auth_permission VALUES (119, 'Can delete DetalleEstimacion', 30, 'delete_detalleestimacion');
INSERT INTO public.auth_permission VALUES (120, 'Can view DetalleEstimacion', 30, 'view_detalleestimacion');
INSERT INTO public.auth_permission VALUES (121, 'Can add Proveedor', 31, 'add_proveedor');
INSERT INTO public.auth_permission VALUES (122, 'Can change Proveedor', 31, 'change_proveedor');
INSERT INTO public.auth_permission VALUES (123, 'Can delete Proveedor', 31, 'delete_proveedor');
INSERT INTO public.auth_permission VALUES (124, 'Can view Proveedor', 31, 'view_proveedor');
INSERT INTO public.auth_permission VALUES (125, 'Can add Cliente', 32, 'add_cliente');
INSERT INTO public.auth_permission VALUES (126, 'Can change Cliente', 32, 'change_cliente');
INSERT INTO public.auth_permission VALUES (127, 'Can delete Cliente', 32, 'delete_cliente');
INSERT INTO public.auth_permission VALUES (128, 'Can view Cliente', 32, 'view_cliente');
INSERT INTO public.auth_permission VALUES (129, 'Can add Vendedor', 33, 'add_vendedor');
INSERT INTO public.auth_permission VALUES (130, 'Can change Vendedor', 33, 'change_vendedor');
INSERT INTO public.auth_permission VALUES (131, 'Can delete Vendedor', 33, 'delete_vendedor');
INSERT INTO public.auth_permission VALUES (132, 'Can view Vendedor', 33, 'view_vendedor');


--
-- TOC entry 5207 (class 0 OID 17554)
-- Dependencies: 231
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.auth_user VALUES (2, 'pbkdf2_sha256$600000$WtdKyToEQyibvyw7TlTz3M$+WozXlKJOWM66QpJCfMi784jy21njlZC+6I8y87v+bk=', '2023-12-27 19:25:09.2802-03', false, 'test', '', '', '', false, true, '2023-12-27 18:05:02-03');
INSERT INTO public.auth_user VALUES (1, 'pbkdf2_sha256$720000$FB59OwO0G0oOhAUpXEOF3n$6bbSATaCnvLpVm5jsZB0TATh0lzhmLQBVU4ziX0LoO0=', '2024-10-09 17:37:34.573872-03', true, 'admin', '', '', 'test@mail.com', true, true, '2023-11-16 21:47:11-03');


--
-- TOC entry 5208 (class 0 OID 17559)
-- Dependencies: 232
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.auth_user_groups VALUES (1, 1, 1);
INSERT INTO public.auth_user_groups VALUES (2, 2, 3);


--
-- TOC entry 5211 (class 0 OID 17564)
-- Dependencies: 235
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5213 (class 0 OID 17568)
-- Dependencies: 237
-- Data for Name: compra; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.compra VALUES (1, NULL, '2024-10-09', 1, 'P', NULL, 1);


--
-- TOC entry 5215 (class 0 OID 17572)
-- Dependencies: 239
-- Data for Name: comprador; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.comprador VALUES (1, 'Alexis Zaracho', '0992458762', 'alezar@gmail.com', '2024-10-04');


--
-- TOC entry 5217 (class 0 OID 17576)
-- Dependencies: 241
-- Data for Name: detalle_compra; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5251 (class 0 OID 18879)
-- Dependencies: 275
-- Data for Name: detalle_estimacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.detalle_estimacion VALUES (1, 2, '2024-09-24', 1, 1, 0);


--
-- TOC entry 5219 (class 0 OID 17580)
-- Dependencies: 243
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.django_admin_log VALUES (1, '2023-12-27 17:59:15.634895-03', '1', 'Administrador', 1, '[{"added": {}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (2, '2023-12-27 18:00:07.48646-03', '2', 'Cajero', 1, '[{"added": {}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (3, '2023-12-27 18:01:19.774039-03', '3', 'Compras', 1, '[{"added": {}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (4, '2023-12-27 18:01:43.391406-03', '3', 'Compra', 2, '[{"changed": {"fields": ["Name"]}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (5, '2023-12-27 18:02:50.80991-03', '4', 'Anil', 1, '[{"added": {}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (6, '2023-12-27 18:03:08.73408-03', '4', 'Animal', 2, '[{"changed": {"fields": ["Name"]}}]', 3, 1);
INSERT INTO public.django_admin_log VALUES (7, '2023-12-27 18:04:03.868271-03', '1', 'admin', 2, '[{"changed": {"fields": ["Groups"]}}]', 4, 1);
INSERT INTO public.django_admin_log VALUES (8, '2023-12-27 18:05:02.855148-03', '2', 'test', 1, '[{"added": {}}]', 4, 1);
INSERT INTO public.django_admin_log VALUES (9, '2023-12-27 18:05:32.955457-03', '2', 'test', 2, '[{"changed": {"fields": ["Groups"]}}]', 4, 1);


--
-- TOC entry 5221 (class 0 OID 17587)
-- Dependencies: 245
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.django_content_type VALUES (1, 'admin', 'logentry');
INSERT INTO public.django_content_type VALUES (2, 'auth', 'permission');
INSERT INTO public.django_content_type VALUES (3, 'auth', 'group');
INSERT INTO public.django_content_type VALUES (4, 'auth', 'user');
INSERT INTO public.django_content_type VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO public.django_content_type VALUES (6, 'sessions', 'session');
INSERT INTO public.django_content_type VALUES (7, 'apl', 'raza');
INSERT INTO public.django_content_type VALUES (8, 'apl', 'grupodestino');
INSERT INTO public.django_content_type VALUES (9, 'apl', 'estado');
INSERT INTO public.django_content_type VALUES (10, 'apl', 'animal');
INSERT INTO public.django_content_type VALUES (11, 'apl', 'vientre');
INSERT INTO public.django_content_type VALUES (12, 'apl', 'palpacion');
INSERT INTO public.django_content_type VALUES (13, 'apl', 'perdida');
INSERT INTO public.django_content_type VALUES (14, 'apl', 'nacimiento');
INSERT INTO public.django_content_type VALUES (15, 'apl', 'fallecimiento');
INSERT INTO public.django_content_type VALUES (16, 'apl', 'marcado');
INSERT INTO public.django_content_type VALUES (17, 'apl', 'medida');
INSERT INTO public.django_content_type VALUES (18, 'apl', 'tipotprev');
INSERT INTO public.django_content_type VALUES (19, 'apl', 'tcurativo');
INSERT INTO public.django_content_type VALUES (20, 'apl', 'tpreventivo');
INSERT INTO public.django_content_type VALUES (21, 'apl', 'aplicaciontp');
INSERT INTO public.django_content_type VALUES (22, 'apl', 'comprador');
INSERT INTO public.django_content_type VALUES (23, 'apl', 'loteventa');
INSERT INTO public.django_content_type VALUES (24, 'apl', 'animaleslote');
INSERT INTO public.django_content_type VALUES (25, 'apl', 'articulo');
INSERT INTO public.django_content_type VALUES (26, 'apl', 'compra');
INSERT INTO public.django_content_type VALUES (27, 'apl', 'detallecompra');
INSERT INTO public.django_content_type VALUES (28, 'apl', 'fecundacion');
INSERT INTO public.django_content_type VALUES (29, 'apl', 'estimacion');
INSERT INTO public.django_content_type VALUES (30, 'apl', 'detalleestimacion');
INSERT INTO public.django_content_type VALUES (31, 'apl', 'proveedor');
INSERT INTO public.django_content_type VALUES (32, 'apl', 'cliente');
INSERT INTO public.django_content_type VALUES (33, 'apl', 'vendedor');


--
-- TOC entry 5223 (class 0 OID 17591)
-- Dependencies: 247
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.django_migrations VALUES (1, 'apl', '0001_initial', '2023-03-30 10:36:03.943281-04');
INSERT INTO public.django_migrations VALUES (2, 'apl', '0002_auto_20230330_1029', '2023-03-30 10:36:03.972665-04');
INSERT INTO public.django_migrations VALUES (3, 'contenttypes', '0001_initial', '2023-11-16 21:46:21.596759-03');
INSERT INTO public.django_migrations VALUES (4, 'auth', '0001_initial', '2023-11-16 21:46:21.780694-03');
INSERT INTO public.django_migrations VALUES (5, 'admin', '0001_initial', '2023-11-16 21:46:21.81734-03');
INSERT INTO public.django_migrations VALUES (6, 'admin', '0002_logentry_remove_auto_add', '2023-11-16 21:46:21.830885-03');
INSERT INTO public.django_migrations VALUES (7, 'admin', '0003_logentry_add_action_flag_choices', '2023-11-16 21:46:21.845496-03');
INSERT INTO public.django_migrations VALUES (8, 'contenttypes', '0002_remove_content_type_name', '2023-11-16 21:46:21.886081-03');
INSERT INTO public.django_migrations VALUES (9, 'auth', '0002_alter_permission_name_max_length', '2023-11-16 21:46:21.899084-03');
INSERT INTO public.django_migrations VALUES (10, 'auth', '0003_alter_user_email_max_length', '2023-11-16 21:46:21.911303-03');
INSERT INTO public.django_migrations VALUES (11, 'auth', '0004_alter_user_username_opts', '2023-11-16 21:46:21.922354-03');
INSERT INTO public.django_migrations VALUES (12, 'auth', '0005_alter_user_last_login_null', '2023-11-16 21:46:21.931334-03');
INSERT INTO public.django_migrations VALUES (13, 'auth', '0006_require_contenttypes_0002', '2023-11-16 21:46:21.936307-03');
INSERT INTO public.django_migrations VALUES (14, 'auth', '0007_alter_validators_add_error_messages', '2023-11-16 21:46:21.948159-03');
INSERT INTO public.django_migrations VALUES (15, 'auth', '0008_alter_user_username_max_length', '2023-11-16 21:46:21.971166-03');
INSERT INTO public.django_migrations VALUES (16, 'auth', '0009_alter_user_last_name_max_length', '2023-11-16 21:46:21.981859-03');
INSERT INTO public.django_migrations VALUES (17, 'auth', '0010_alter_group_name_max_length', '2023-11-16 21:46:21.993435-03');
INSERT INTO public.django_migrations VALUES (18, 'auth', '0011_update_proxy_permissions', '2023-11-16 21:46:22.020509-03');
INSERT INTO public.django_migrations VALUES (19, 'auth', '0012_alter_user_first_name_max_length', '2023-11-16 21:46:22.030067-03');
INSERT INTO public.django_migrations VALUES (20, 'sessions', '0001_initial', '2023-11-16 21:46:22.055939-03');


--
-- TOC entry 5225 (class 0 OID 17597)
-- Dependencies: 249
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.django_session VALUES ('vbynjr2ob4wt1bgb5o4q6usn93kq5u6q', '.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1r3n19:rMIeh3Uc6S8JP_w8Lefy5UBv2pdPThQNwj9WWyWSIR4', '2023-11-30 21:47:31.12745-03');
INSERT INTO public.django_session VALUES ('pxrj1tt9z2egdxpe59jjy5n12rihdw8c', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rXrds:Wq5KhNI5at5ywMh1WjJ8pKNs4d8ER6IbfTG2Bv14_QM', '2024-02-21 17:47:48.262478-03');
INSERT INTO public.django_session VALUES ('mxzdqqgzrw4myb155p1a5z9j5hs50304', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rfr76:sAeReYfNA2hoSWGvIUyp3NmACuYsMQSmpfuD6csONjk', '2024-03-14 18:51:00.5691-03');
INSERT INTO public.django_session VALUES ('dyzda3sk6k0phm05do1cer8ypefme9ku', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rmjY8:gVLkofCdqKlj3KINKTYG7GECu2N-JaDr9D2rq-Bc5DQ', '2024-04-02 17:11:20.420434-04');
INSERT INTO public.django_session VALUES ('edka99tmhcoys7h5ov53s7vupsvkg868', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1ruLyM:SMp8LDUU-NczcW8bI6qh_4JuNLcQiygj9s9yUWqYffM', '2024-04-23 16:37:54.762978-04');
INSERT INTO public.django_session VALUES ('q9gqhl1wr6yyj24skoie73pygpcfkkai', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rznlV:tqRF-ufV7F5g74bWIqJ_-4hHdsEI7O1fQhY7ejBe6SA', '2024-05-08 17:19:09.284863-04');
INSERT INTO public.django_session VALUES ('gzh5x0uuuuhll43inhc8qfvdyo4klzqd', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1s7mVU:JeYjqYDeK2QnvzsL1Pc_Hlxc1Cami9nq0BIwXY0eaXk', '2024-05-30 17:35:36.797046-04');
INSERT INTO public.django_session VALUES ('7kv9kor9wxdfqsuga8hrbr6s9rnr9tib', '.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rDCby:KKDOKjkdsLAqjPj1gtSytArWlKAKqQs9WO545KUBPPk', '2023-12-26 20:56:26.784873-03');
INSERT INTO public.django_session VALUES ('isegctudl6tplponfaniv946hpe68dpu', '.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rFNQR:ltTTw7z84LUjXh9VAHGCEGWMslN5BRi6vTFH0gbHsvk', '2024-01-01 20:53:31.444866-03');
INSERT INTO public.django_session VALUES ('xdks2z4jxp0rwd14vumbp74lfa6r8dqd', '.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rFOFT:nCM4GG1KYv_NXN6d0eybRJiwgOqeV-hZLlLP-9CG9ms', '2024-01-01 18:46:15.025613-03');
INSERT INTO public.django_session VALUES ('v9xp4kmivkn5xyidlcu4yghu3urvqdip', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rIfOM:S-vlicueDUcYT5XteyAkXxqgestruSr8Ld04CUkbuUw', '2024-01-10 19:40:58.68598-03');
INSERT INTO public.django_session VALUES ('qo7r9bn889tz9wo9vhi9r4z3q54n6e3r', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rLB1f:b2CbvN-xq-GsQ3wh_INiov_2FVi-H0PKj9Qwys2HjPw', '2024-01-17 17:51:55.652087-03');
INSERT INTO public.django_session VALUES ('dtfxxjhdrlc4s5l67awuf4v5wuutun09', '.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rSQgE:-C9jxgjA1h53wWxFMwRy4-ptJxTz9guYYsfdC8PqGGo', '2024-02-06 17:59:46.839471-03');
INSERT INTO public.django_session VALUES ('th2wda31v5v8d1h0yp2c2e4xyk6xxqn4', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1sYatr:2K2gOQ9PdlPlxFmZBRa_EoGAFdUFZTe2vzyD9jKrRKk', '2024-08-12 16:39:35.728513-04');
INSERT INTO public.django_session VALUES ('90s7eif416ots2tgndlwsdaiqzqcl9il', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1sdfqf:wrAYYM2YkYV_gWOki_j9Xs8s1jHCeRgldNPPlL2JqCE', '2024-08-26 16:57:17.515435-04');
INSERT INTO public.django_session VALUES ('ydruvdnu5cf1d139x8apw30mukmbf5z4', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1slHdM:xgNSTsileYperrsAK2RWZKwyDqSu-jqVzSAgl_rtkXw', '2024-09-16 16:43:00.09198-04');
INSERT INTO public.django_session VALUES ('6p4a7f4nvibzttqtacm8bsom12a3zs2r', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1snpVo:Wt6UQk9oZ6_qeL4pV4WQwfbYVonSR1BAzjrUMV6ObK0', '2024-09-23 17:17:44.853346-04');
INSERT INTO public.django_session VALUES ('2sc10cidg771qqtmvc21fsqcjit7otka', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1stGGA:4LhHc2YSm_bsXOvBub4nfcOkEag9Zp2pVu841froqrg', '2024-10-08 17:52:02.826685-03');
INSERT INTO public.django_session VALUES ('i50so8fu9uk3af6nlcsww71j1s7raa2g', '.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1sygFK:_NKVWqEypZ48pHNapileDs18exZLl4m3NqdXrb94ELQ', '2024-10-23 17:37:34.583681-03');


--
-- TOC entry 5249 (class 0 OID 18872)
-- Dependencies: 273
-- Data for Name: estimacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.estimacion VALUES (1, '2024-09-24', '2024-09-24');


--
-- TOC entry 5226 (class 0 OID 17602)
-- Dependencies: 250
-- Data for Name: fallecimiento; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5228 (class 0 OID 17606)
-- Dependencies: 252
-- Data for Name: fecundacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fecundacion VALUES (1, '2023-04-18', 1, 2);


--
-- TOC entry 5229 (class 0 OID 17609)
-- Dependencies: 253
-- Data for Name: lote_venta; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5231 (class 0 OID 17613)
-- Dependencies: 255
-- Data for Name: marcado; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5233 (class 0 OID 17617)
-- Dependencies: 257
-- Data for Name: medida; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.medida VALUES (1, 1000, 155, '2023-03-30', 1);
INSERT INTO public.medida VALUES (2, 1300, 165, '2023-03-30', 2);
INSERT INTO public.medida VALUES (3, 1000, 178, '2023-09-26', 3);
INSERT INTO public.medida VALUES (4, 1005, 158, '2024-05-16', 1);
INSERT INTO public.medida VALUES (5, 1005, 158, '2024-05-16', 1);
INSERT INTO public.medida VALUES (6, 1006, 158, '2024-05-16', 1);


--
-- TOC entry 5235 (class 0 OID 17621)
-- Dependencies: 259
-- Data for Name: nacimiento; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5237 (class 0 OID 17625)
-- Dependencies: 261
-- Data for Name: palpacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.palpacion VALUES (1, 'V', 'asdf', '2023-04-18', 1);


--
-- TOC entry 5239 (class 0 OID 17629)
-- Dependencies: 263
-- Data for Name: perdida; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 5241 (class 0 OID 17633)
-- Dependencies: 265
-- Data for Name: raza; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.raza VALUES (1, 'Brahman', '2023-03-30');
INSERT INTO public.raza VALUES (2, 'Nelore', '2023-03-30');
INSERT INTO public.raza VALUES (3, 'Angus', '2023-03-30');
INSERT INTO public.raza VALUES (4, 'NuevoEdit', '2023-09-26');


--
-- TOC entry 5243 (class 0 OID 17637)
-- Dependencies: 267
-- Data for Name: tcurativo; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tcurativo VALUES (1, '', '2024-10-17', 1, '2024-10-01', 5, '2024-10-30', 'Test');


--
-- TOC entry 5245 (class 0 OID 17641)
-- Dependencies: 269
-- Data for Name: tipo_tprev; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tipo_tprev VALUES (2, 'test', '2024-01-30');


--
-- TOC entry 5255 (class 0 OID 23468)
-- Dependencies: 279
-- Data for Name: tpreventivo; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tpreventivo VALUES (1, 2, '2024-10-16', 'Antibiótico', 5);


--
-- TOC entry 5253 (class 0 OID 19247)
-- Dependencies: 277
-- Data for Name: vendedor; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.vendedor VALUES (1, 'José Domínguez', '+595981654987', 'jdomin@outlook.com', '2024-10-04');


--
-- TOC entry 5289 (class 0 OID 0)
-- Dependencies: 216
-- Name: Estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."Estado_id_seq"', 5, true);


--
-- TOC entry 5290 (class 0 OID 0)
-- Dependencies: 218
-- Name: GrupoDestino_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."GrupoDestino_id_seq"', 2, true);


--
-- TOC entry 5291 (class 0 OID 0)
-- Dependencies: 220
-- Name: animal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.animal_id_seq', 4, true);


--
-- TOC entry 5292 (class 0 OID 0)
-- Dependencies: 222
-- Name: animales_lote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.animales_lote_id_seq', 1, false);


--
-- TOC entry 5293 (class 0 OID 0)
-- Dependencies: 280
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.aplicacion_tp_id_seq', 1, true);


--
-- TOC entry 5294 (class 0 OID 0)
-- Dependencies: 224
-- Name: articulo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.articulo_id_seq', 1, true);


--
-- TOC entry 5295 (class 0 OID 0)
-- Dependencies: 226
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- TOC entry 5296 (class 0 OID 0)
-- Dependencies: 228
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 192, true);


--
-- TOC entry 5297 (class 0 OID 0)
-- Dependencies: 230
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 132, true);


--
-- TOC entry 5298 (class 0 OID 0)
-- Dependencies: 233
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 2, true);


--
-- TOC entry 5299 (class 0 OID 0)
-- Dependencies: 234
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- TOC entry 5300 (class 0 OID 0)
-- Dependencies: 236
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 5301 (class 0 OID 0)
-- Dependencies: 238
-- Name: compra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.compra_id_seq', 1, true);


--
-- TOC entry 5302 (class 0 OID 0)
-- Dependencies: 240
-- Name: comprador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.comprador_id_seq', 1, true);


--
-- TOC entry 5303 (class 0 OID 0)
-- Dependencies: 242
-- Name: detalle_compra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.detalle_compra_id_seq', 1, false);


--
-- TOC entry 5304 (class 0 OID 0)
-- Dependencies: 274
-- Name: detalle_estimacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.detalle_estimacion_id_seq', 4, true);


--
-- TOC entry 5305 (class 0 OID 0)
-- Dependencies: 244
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 9, true);


--
-- TOC entry 5306 (class 0 OID 0)
-- Dependencies: 246
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 33, true);


--
-- TOC entry 5307 (class 0 OID 0)
-- Dependencies: 248
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 20, true);


--
-- TOC entry 5308 (class 0 OID 0)
-- Dependencies: 272
-- Name: estimacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.estimacion_id_seq', 6, true);


--
-- TOC entry 5309 (class 0 OID 0)
-- Dependencies: 251
-- Name: fallecimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fallecimiento_id_seq', 1, false);


--
-- TOC entry 5310 (class 0 OID 0)
-- Dependencies: 254
-- Name: lote_venta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.lote_venta_id_seq', 1, false);


--
-- TOC entry 5311 (class 0 OID 0)
-- Dependencies: 256
-- Name: marcado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.marcado_id_seq', 1, false);


--
-- TOC entry 5312 (class 0 OID 0)
-- Dependencies: 258
-- Name: medida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.medida_id_seq', 6, true);


--
-- TOC entry 5313 (class 0 OID 0)
-- Dependencies: 260
-- Name: nacimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.nacimiento_id_seq', 1, false);


--
-- TOC entry 5314 (class 0 OID 0)
-- Dependencies: 262
-- Name: palpacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.palpacion_id_seq', 1, true);


--
-- TOC entry 5315 (class 0 OID 0)
-- Dependencies: 264
-- Name: perdida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.perdida_id_seq', 1, false);


--
-- TOC entry 5316 (class 0 OID 0)
-- Dependencies: 266
-- Name: raza_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.raza_id_seq', 4, true);


--
-- TOC entry 5317 (class 0 OID 0)
-- Dependencies: 268
-- Name: tcurativo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tcurativo_id_seq', 1, true);


--
-- TOC entry 5318 (class 0 OID 0)
-- Dependencies: 270
-- Name: tipo_tprev_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tipo_tprev_id_seq', 2, true);


--
-- TOC entry 5319 (class 0 OID 0)
-- Dependencies: 278
-- Name: tpreventivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tpreventivo_id_seq', 1, true);


--
-- TOC entry 5320 (class 0 OID 0)
-- Dependencies: 276
-- Name: vendedor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.vendedor_id_seq', 1, true);


--
-- TOC entry 5321 (class 0 OID 0)
-- Dependencies: 271
-- Name: vientre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.vientre_id_seq', 1, true);


--
-- TOC entry 4889 (class 2606 OID 17673)
-- Name: Estado Estado_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_descripcion_key" UNIQUE (descripcion);


--
-- TOC entry 4891 (class 2606 OID 17675)
-- Name: Estado Estado_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_pkey" PRIMARY KEY (id);


--
-- TOC entry 4894 (class 2606 OID 17677)
-- Name: GrupoDestino GrupoDestino_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_descripcion_key" UNIQUE (descripcion);


--
-- TOC entry 4896 (class 2606 OID 17679)
-- Name: GrupoDestino GrupoDestino_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_pkey" PRIMARY KEY (id);


--
-- TOC entry 4901 (class 2606 OID 17681)
-- Name: animal animal_id_Animal_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_id_Animal_key" UNIQUE ("id_Animal");


--
-- TOC entry 4903 (class 2606 OID 17683)
-- Name: animal animal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_pkey PRIMARY KEY (id);


--
-- TOC entry 4908 (class 2606 OID 17685)
-- Name: animales_lote animales_lote_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT animales_lote_pkey PRIMARY KEY (id);


--
-- TOC entry 5013 (class 2606 OID 23493)
-- Name: aplicacion_tp aplicacion_tp_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT aplicacion_tp_pk PRIMARY KEY (id);


--
-- TOC entry 4910 (class 2606 OID 17689)
-- Name: articulo articulo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articulo
    ADD CONSTRAINT articulo_pkey PRIMARY KEY (id);


--
-- TOC entry 4913 (class 2606 OID 17691)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 4918 (class 2606 OID 17693)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 4921 (class 2606 OID 17695)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 4915 (class 2606 OID 17697)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 4924 (class 2606 OID 17699)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 4926 (class 2606 OID 17701)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 4934 (class 2606 OID 17703)
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 4937 (class 2606 OID 17705)
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 4928 (class 2606 OID 17707)
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 4940 (class 2606 OID 17709)
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 4943 (class 2606 OID 17711)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 4931 (class 2606 OID 17713)
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 4945 (class 2606 OID 17715)
-- Name: compra compra_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_pkey PRIMARY KEY (id);


--
-- TOC entry 4947 (class 2606 OID 19273)
-- Name: compra compra_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_unique UNIQUE (vendedor_id, estimacion_id);


--
-- TOC entry 4949 (class 2606 OID 17717)
-- Name: comprador comprador_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comprador
    ADD CONSTRAINT comprador_pkey PRIMARY KEY (id);


--
-- TOC entry 5009 (class 2606 OID 19252)
-- Name: vendedor comprador_pkey_1; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vendedor
    ADD CONSTRAINT comprador_pkey_1 PRIMARY KEY (id);


--
-- TOC entry 4953 (class 2606 OID 17719)
-- Name: detalle_compra detalle_compra_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT detalle_compra_pkey PRIMARY KEY (id);


--
-- TOC entry 5007 (class 2606 OID 18885)
-- Name: detalle_estimacion detalle_estimacion_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_estimacion
    ADD CONSTRAINT detalle_estimacion_pk PRIMARY KEY (id);


--
-- TOC entry 4956 (class 2606 OID 17721)
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 4959 (class 2606 OID 17723)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 4961 (class 2606 OID 17725)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4963 (class 2606 OID 17727)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 4966 (class 2606 OID 17729)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 5005 (class 2606 OID 18887)
-- Name: estimacion estimacion_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estimacion
    ADD CONSTRAINT estimacion_pk PRIMARY KEY (id);


--
-- TOC entry 4970 (class 2606 OID 17731)
-- Name: fallecimiento fallecimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT fallecimiento_pkey PRIMARY KEY (id);


--
-- TOC entry 4977 (class 2606 OID 17733)
-- Name: lote_venta lote_venta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT lote_venta_pkey PRIMARY KEY (id);


--
-- TOC entry 4980 (class 2606 OID 17735)
-- Name: marcado marcado_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT marcado_pkey PRIMARY KEY (id);


--
-- TOC entry 4983 (class 2606 OID 17737)
-- Name: medida medida_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medida
    ADD CONSTRAINT medida_pkey PRIMARY KEY (id);


--
-- TOC entry 4987 (class 2606 OID 17739)
-- Name: nacimiento nacimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT nacimiento_pkey PRIMARY KEY (id);


--
-- TOC entry 4990 (class 2606 OID 17741)
-- Name: palpacion palpacion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_pkey PRIMARY KEY (id);


--
-- TOC entry 4993 (class 2606 OID 17743)
-- Name: perdida perdida_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT perdida_pkey PRIMARY KEY (id);


--
-- TOC entry 4996 (class 2606 OID 17745)
-- Name: raza raza_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 4998 (class 2606 OID 17747)
-- Name: raza raza_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_pkey PRIMARY KEY (id);


--
-- TOC entry 5001 (class 2606 OID 17749)
-- Name: tcurativo tcurativo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT tcurativo_pkey PRIMARY KEY (id);


--
-- TOC entry 5003 (class 2606 OID 17751)
-- Name: tipo_tprev tipo_tprev_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tipo_tprev
    ADD CONSTRAINT tipo_tprev_pkey PRIMARY KEY (id);


--
-- TOC entry 5011 (class 2606 OID 23482)
-- Name: tpreventivo tpreventivo_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_pk PRIMARY KEY (id);


--
-- TOC entry 4974 (class 2606 OID 17755)
-- Name: fecundacion vientre_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_pkey PRIMARY KEY (id);


--
-- TOC entry 4887 (class 1259 OID 17756)
-- Name: Estado_descripcion_b8dc8091_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Estado_descripcion_b8dc8091_like" ON public."Estado" USING btree (descripcion varchar_pattern_ops);


--
-- TOC entry 4892 (class 1259 OID 17757)
-- Name: GrupoDestino_descripcion_2c045343_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "GrupoDestino_descripcion_2c045343_like" ON public."GrupoDestino" USING btree (descripcion varchar_pattern_ops);


--
-- TOC entry 4897 (class 1259 OID 17758)
-- Name: animal_estado_id_944692c8; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX animal_estado_id_944692c8 ON public.animal USING btree (estado_id);


--
-- TOC entry 4898 (class 1259 OID 17759)
-- Name: animal_grupo_destino_id_b68236c3; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX animal_grupo_destino_id_b68236c3 ON public.animal USING btree (grupo_destino_id);


--
-- TOC entry 4899 (class 1259 OID 17760)
-- Name: animal_id_Animal_38e077e5_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "animal_id_Animal_38e077e5_like" ON public.animal USING btree ("id_Animal" varchar_pattern_ops);


--
-- TOC entry 4904 (class 1259 OID 17761)
-- Name: animal_raza_id_01c071b4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX animal_raza_id_01c071b4 ON public.animal USING btree (raza_id);


--
-- TOC entry 4905 (class 1259 OID 17762)
-- Name: animales_lote_id_Animal_id_6feaf46b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "animales_lote_id_Animal_id_6feaf46b" ON public.animales_lote USING btree ("id_Animal_id");


--
-- TOC entry 4906 (class 1259 OID 17763)
-- Name: animales_lote_id_Lote_id_31dec68e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "animales_lote_id_Lote_id_31dec68e" ON public.animales_lote USING btree ("id_Lote_id");


--
-- TOC entry 4911 (class 1259 OID 17766)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 4916 (class 1259 OID 17767)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- TOC entry 4919 (class 1259 OID 17768)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 4922 (class 1259 OID 17769)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- TOC entry 4932 (class 1259 OID 17770)
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- TOC entry 4935 (class 1259 OID 17771)
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- TOC entry 4938 (class 1259 OID 17772)
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 4941 (class 1259 OID 17773)
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 4929 (class 1259 OID 17774)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 4950 (class 1259 OID 17775)
-- Name: detalle_compra_id_Articulo_id_ba37a9fa; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "detalle_compra_id_Articulo_id_ba37a9fa" ON public.detalle_compra USING btree ("id_Articulo_id");


--
-- TOC entry 4951 (class 1259 OID 17776)
-- Name: detalle_compra_id_Compra_id_43b8a91e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "detalle_compra_id_Compra_id_43b8a91e" ON public.detalle_compra USING btree ("id_Compra_id");


--
-- TOC entry 4954 (class 1259 OID 17777)
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- TOC entry 4957 (class 1259 OID 17778)
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- TOC entry 4964 (class 1259 OID 17779)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- TOC entry 4967 (class 1259 OID 17780)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 4968 (class 1259 OID 17781)
-- Name: fallecimiento_id_Animal_id_29c69888; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "fallecimiento_id_Animal_id_29c69888" ON public.fallecimiento USING btree ("id_Animal_id");


--
-- TOC entry 4975 (class 1259 OID 17782)
-- Name: lote_venta_id_Comprador_id_3c61c9be; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "lote_venta_id_Comprador_id_3c61c9be" ON public.lote_venta USING btree ("id_Comprador_id");


--
-- TOC entry 4978 (class 1259 OID 17783)
-- Name: marcado_id_Animal_id_3776f20e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "marcado_id_Animal_id_3776f20e" ON public.marcado USING btree ("id_Animal_id");


--
-- TOC entry 4981 (class 1259 OID 17784)
-- Name: medida_id_Animal_id_0432812b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "medida_id_Animal_id_0432812b" ON public.medida USING btree ("id_Animal_id");


--
-- TOC entry 4984 (class 1259 OID 17785)
-- Name: nacimiento_id_Animal_id_de50946e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "nacimiento_id_Animal_id_de50946e" ON public.nacimiento USING btree ("id_Animal_id");


--
-- TOC entry 4985 (class 1259 OID 17786)
-- Name: nacimiento_id_Vientre_id_66d6bd7f; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "nacimiento_id_Vientre_id_66d6bd7f" ON public.nacimiento USING btree ("id_Fecundacion_id");


--
-- TOC entry 4988 (class 1259 OID 17787)
-- Name: palpacion_id_vientre_id_6762db22; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX palpacion_id_vientre_id_6762db22 ON public.palpacion USING btree (id_fecundacion_id);


--
-- TOC entry 4991 (class 1259 OID 17788)
-- Name: perdida_id_Palpacion_id_7f0a21a1; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "perdida_id_Palpacion_id_7f0a21a1" ON public.perdida USING btree ("id_Palpacion_id");


--
-- TOC entry 4994 (class 1259 OID 17789)
-- Name: raza_descripcion_320960d6_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX raza_descripcion_320960d6_like ON public.raza USING btree (descripcion varchar_pattern_ops);


--
-- TOC entry 4999 (class 1259 OID 17790)
-- Name: tcurativo_id_Animal_id_8970efeb; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "tcurativo_id_Animal_id_8970efeb" ON public.tcurativo USING btree ("id_Animal_id");


--
-- TOC entry 4971 (class 1259 OID 17792)
-- Name: vientre_id_madre_id_b449b968; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX vientre_id_madre_id_b449b968 ON public.fecundacion USING btree (id_madre_id);


--
-- TOC entry 4972 (class 1259 OID 17793)
-- Name: vientre_id_padre_id_d133132e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX vientre_id_padre_id_d133132e ON public.fecundacion USING btree (id_padre_id);


--
-- TOC entry 5014 (class 2606 OID 17794)
-- Name: animal animal_estado_id_944692c8_fk_Estado_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_estado_id_944692c8_fk_Estado_id" FOREIGN KEY (estado_id) REFERENCES public."Estado"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5015 (class 2606 OID 17799)
-- Name: animal animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id" FOREIGN KEY (grupo_destino_id) REFERENCES public."GrupoDestino"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5016 (class 2606 OID 17804)
-- Name: animal animal_raza_id_01c071b4_fk_raza_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_raza_id_01c071b4_fk_raza_id FOREIGN KEY (raza_id) REFERENCES public.raza(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5017 (class 2606 OID 17809)
-- Name: animales_lote animales_lote_id_Animal_id_6feaf46b_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Animal_id_6feaf46b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5018 (class 2606 OID 17814)
-- Name: animales_lote animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id" FOREIGN KEY ("id_Lote_id") REFERENCES public.lote_venta(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5046 (class 2606 OID 23494)
-- Name: aplicacion_tp aplicacion_tp_animal_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT aplicacion_tp_animal_fk FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id);


--
-- TOC entry 5047 (class 2606 OID 23499)
-- Name: aplicacion_tp aplicacion_tp_tpreventivo_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT aplicacion_tp_tpreventivo_fk FOREIGN KEY ("id_TPreventivo_id") REFERENCES public.tpreventivo(id);


--
-- TOC entry 5019 (class 2606 OID 17829)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5020 (class 2606 OID 17834)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5021 (class 2606 OID 17839)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5022 (class 2606 OID 17844)
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5023 (class 2606 OID 17849)
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5024 (class 2606 OID 17854)
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5025 (class 2606 OID 17859)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5026 (class 2606 OID 19267)
-- Name: compra compra_estimacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_estimacion_fk FOREIGN KEY (estimacion_id) REFERENCES public.estimacion(id);


--
-- TOC entry 5027 (class 2606 OID 19262)
-- Name: compra compra_vendedor_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_vendedor_fk FOREIGN KEY (vendedor_id) REFERENCES public.vendedor(id);


--
-- TOC entry 5028 (class 2606 OID 17864)
-- Name: detalle_compra detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id" FOREIGN KEY ("id_Articulo_id") REFERENCES public.articulo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5029 (class 2606 OID 17869)
-- Name: detalle_compra detalle_compra_id_Compra_id_43b8a91e_fk_compra_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Compra_id_43b8a91e_fk_compra_id" FOREIGN KEY ("id_Compra_id") REFERENCES public.compra(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5043 (class 2606 OID 18893)
-- Name: detalle_estimacion detalle_estimacion_articulo_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_estimacion
    ADD CONSTRAINT detalle_estimacion_articulo_fk FOREIGN KEY ("id_Articulo_id") REFERENCES public.articulo(id);


--
-- TOC entry 5044 (class 2606 OID 18888)
-- Name: detalle_estimacion detalle_estimacion_estimacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.detalle_estimacion
    ADD CONSTRAINT detalle_estimacion_estimacion_fk FOREIGN KEY ("id_Estimacion_id") REFERENCES public.estimacion(id);


--
-- TOC entry 5030 (class 2606 OID 17874)
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5031 (class 2606 OID 17879)
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5032 (class 2606 OID 17884)
-- Name: fallecimiento fallecimiento_id_Animal_id_29c69888_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT "fallecimiento_id_Animal_id_29c69888_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5035 (class 2606 OID 17889)
-- Name: lote_venta lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT "lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id" FOREIGN KEY ("id_Comprador_id") REFERENCES public.comprador(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5036 (class 2606 OID 17894)
-- Name: marcado marcado_id_Animal_id_3776f20e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT "marcado_id_Animal_id_3776f20e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5037 (class 2606 OID 17899)
-- Name: medida medida_id_Animal_id_0432812b_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.medida
    ADD CONSTRAINT "medida_id_Animal_id_0432812b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5038 (class 2606 OID 17904)
-- Name: nacimiento nacimiento_id_Animal_id_de50946e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Animal_id_de50946e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5039 (class 2606 OID 17909)
-- Name: nacimiento nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id" FOREIGN KEY ("id_Fecundacion_id") REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5040 (class 2606 OID 17914)
-- Name: palpacion palpacion_id_vientre_id_6762db22_fk_vientre_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_id_vientre_id_6762db22_fk_vientre_id FOREIGN KEY (id_fecundacion_id) REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5041 (class 2606 OID 17919)
-- Name: perdida perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT "perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id" FOREIGN KEY ("id_Palpacion_id") REFERENCES public.palpacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5042 (class 2606 OID 17924)
-- Name: tcurativo tcurativo_id_Animal_id_8970efeb_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT "tcurativo_id_Animal_id_8970efeb_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5045 (class 2606 OID 23476)
-- Name: tpreventivo tpreventivo_tipo_tprev_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_tipo_tprev_fk FOREIGN KEY ("id_TipoTPrev_id") REFERENCES public.tipo_tprev(id);


--
-- TOC entry 5033 (class 2606 OID 17939)
-- Name: fecundacion vientre_id_madre_id_b449b968_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_madre_id_b449b968_fk_animal_id FOREIGN KEY (id_madre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 5034 (class 2606 OID 17944)
-- Name: fecundacion vientre_id_padre_id_d133132e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_padre_id_d133132e_fk_animal_id FOREIGN KEY (id_padre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2024-10-16 23:03:43

--
-- PostgreSQL database dump complete
--

