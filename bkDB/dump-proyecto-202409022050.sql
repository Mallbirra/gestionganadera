toc.dat                                                                                             0000600 0004000 0002000 00000260744 14665456514 0014474 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP       2                |            proyecto    16.3    16.3    c           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false         d           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         e           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         f           1262    17517    proyecto    DATABASE     ~   CREATE DATABASE proyecto WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Paraguay.1252';
    DROP DATABASE proyecto;
                postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false         g           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    5         �            1259    17518    Estado    TABLE     �   CREATE TABLE public."Estado" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public."Estado";
       public         heap    postgres    false    5         �            1259    17521    Estado_id_seq    SEQUENCE     x   CREATE SEQUENCE public."Estado_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public."Estado_id_seq";
       public          postgres    false    215    5         h           0    0    Estado_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public."Estado_id_seq" OWNED BY public."Estado".id;
          public          postgres    false    216         �            1259    17522    GrupoDestino    TABLE     �   CREATE TABLE public."GrupoDestino" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);
 "   DROP TABLE public."GrupoDestino";
       public         heap    postgres    false    5         �            1259    17525    GrupoDestino_id_seq    SEQUENCE     ~   CREATE SEQUENCE public."GrupoDestino_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."GrupoDestino_id_seq";
       public          postgres    false    217    5         i           0    0    GrupoDestino_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public."GrupoDestino_id_seq" OWNED BY public."GrupoDestino".id;
          public          postgres    false    218         �            1259    17526    animal    TABLE     7  CREATE TABLE public.animal (
    id bigint NOT NULL,
    "id_Animal" character varying(50) NOT NULL,
    fecha_nacimiento date NOT NULL,
    sexo character varying(1) NOT NULL,
    estado_id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    grupo_destino_id bigint NOT NULL,
    raza_id bigint NOT NULL
);
    DROP TABLE public.animal;
       public         heap    postgres    false    5         �            1259    17529    animal_id_seq    SEQUENCE     v   CREATE SEQUENCE public.animal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.animal_id_seq;
       public          postgres    false    219    5         j           0    0    animal_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.animal_id_seq OWNED BY public.animal.id;
          public          postgres    false    220         �            1259    17530    animales_lote    TABLE     �   CREATE TABLE public.animales_lote (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Lote_id" bigint NOT NULL
);
 !   DROP TABLE public.animales_lote;
       public         heap    postgres    false    5         �            1259    17533    animales_lote_id_seq    SEQUENCE     }   CREATE SEQUENCE public.animales_lote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.animales_lote_id_seq;
       public          postgres    false    221    5         k           0    0    animales_lote_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.animales_lote_id_seq OWNED BY public.animales_lote.id;
          public          postgres    false    222         �            1259    17534    aplicacion_tp    TABLE     �   CREATE TABLE public.aplicacion_tp (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_TPreventivo_id" bigint NOT NULL
);
 !   DROP TABLE public.aplicacion_tp;
       public         heap    postgres    false    5         �            1259    17537    aplicacion_tp_id_seq    SEQUENCE     }   CREATE SEQUENCE public.aplicacion_tp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.aplicacion_tp_id_seq;
       public          postgres    false    5    223         l           0    0    aplicacion_tp_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.aplicacion_tp_id_seq OWNED BY public.aplicacion_tp.id;
          public          postgres    false    224         �            1259    17538    articulo    TABLE     �   CREATE TABLE public.articulo (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    "cantidadActual" integer NOT NULL,
    "cantidadIdeal" integer NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public.articulo;
       public         heap    postgres    false    5         �            1259    17541    articulo_id_seq    SEQUENCE     x   CREATE SEQUENCE public.articulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.articulo_id_seq;
       public          postgres    false    225    5         m           0    0    articulo_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.articulo_id_seq OWNED BY public.articulo.id;
          public          postgres    false    226         �            1259    17542 
   auth_group    TABLE     f   CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE public.auth_group;
       public         heap    postgres    false    5         �            1259    17545    auth_group_id_seq    SEQUENCE     �   ALTER TABLE public.auth_group ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    5    227         �            1259    17546    auth_group_permissions    TABLE     �   CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         heap    postgres    false    5         �            1259    17549    auth_group_permissions_id_seq    SEQUENCE     �   ALTER TABLE public.auth_group_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    229    5         �            1259    17550    auth_permission    TABLE     �   CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         heap    postgres    false    5         �            1259    17553    auth_permission_id_seq    SEQUENCE     �   ALTER TABLE public.auth_permission ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    231    5         �            1259    17554 	   auth_user    TABLE     �  CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         heap    postgres    false    5         �            1259    17559    auth_user_groups    TABLE     ~   CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         heap    postgres    false    5         �            1259    17562    auth_user_groups_id_seq    SEQUENCE     �   ALTER TABLE public.auth_user_groups ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    234    5         �            1259    17563    auth_user_id_seq    SEQUENCE     �   ALTER TABLE public.auth_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    5    233         �            1259    17564    auth_user_user_permissions    TABLE     �   CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         heap    postgres    false    5         �            1259    17567 !   auth_user_user_permissions_id_seq    SEQUENCE     �   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    237    5         �            1259    17568    compra    TABLE     �   CREATE TABLE public.compra (
    id bigint NOT NULL,
    "fechaCompra" date NOT NULL,
    "totalCompra" integer NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public.compra;
       public         heap    postgres    false    5         �            1259    17571    compra_id_seq    SEQUENCE     v   CREATE SEQUENCE public.compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.compra_id_seq;
       public          postgres    false    5    239         n           0    0    compra_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.compra_id_seq OWNED BY public.compra.id;
          public          postgres    false    240         �            1259    17572 	   comprador    TABLE     �   CREATE TABLE public.comprador (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    telefono character varying(100) NOT NULL,
    mail character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public.comprador;
       public         heap    postgres    false    5         �            1259    17575    comprador_id_seq    SEQUENCE     y   CREATE SEQUENCE public.comprador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.comprador_id_seq;
       public          postgres    false    241    5         o           0    0    comprador_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.comprador_id_seq OWNED BY public.comprador.id;
          public          postgres    false    242         �            1259    17576    detalle_compra    TABLE     �   CREATE TABLE public.detalle_compra (
    id bigint NOT NULL,
    cantidad integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Articulo_id" bigint NOT NULL,
    "id_Compra_id" bigint NOT NULL
);
 "   DROP TABLE public.detalle_compra;
       public         heap    postgres    false    5         �            1259    17579    detalle_compra_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.detalle_compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.detalle_compra_id_seq;
       public          postgres    false    5    243         p           0    0    detalle_compra_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.detalle_compra_id_seq OWNED BY public.detalle_compra.id;
          public          postgres    false    244         �            1259    17580    django_admin_log    TABLE     �  CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         heap    postgres    false    5         �            1259    17586    django_admin_log_id_seq    SEQUENCE     �   ALTER TABLE public.django_admin_log ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    5    245         �            1259    17587    django_content_type    TABLE     �   CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         heap    postgres    false    5         �            1259    17590    django_content_type_id_seq    SEQUENCE     �   ALTER TABLE public.django_content_type ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    247    5         �            1259    17591    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         heap    postgres    false    5         �            1259    17596    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public          postgres    false    249    5         q           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
          public          postgres    false    250         �            1259    17597    django_session    TABLE     �   CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         heap    postgres    false    5         �            1259    17602    fallecimiento    TABLE     �   CREATE TABLE public.fallecimiento (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);
 !   DROP TABLE public.fallecimiento;
       public         heap    postgres    false    5         �            1259    17605    fallecimiento_id_seq    SEQUENCE     }   CREATE SEQUENCE public.fallecimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.fallecimiento_id_seq;
       public          postgres    false    252    5         r           0    0    fallecimiento_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.fallecimiento_id_seq OWNED BY public.fallecimiento.id;
          public          postgres    false    253         �            1259    17606    fecundacion    TABLE     �   CREATE TABLE public.fecundacion (
    id bigint NOT NULL,
    fecha date NOT NULL,
    id_madre_id bigint NOT NULL,
    id_padre_id bigint NOT NULL
);
    DROP TABLE public.fecundacion;
       public         heap    postgres    false    5         �            1259    17609 
   lote_venta    TABLE     �   CREATE TABLE public.lote_venta (
    id bigint NOT NULL,
    estado character varying(1) NOT NULL,
    fecha_envio date NOT NULL,
    fecha_confirmado date NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Comprador_id" bigint NOT NULL
);
    DROP TABLE public.lote_venta;
       public         heap    postgres    false    5                     1259    17612    lote_venta_id_seq    SEQUENCE     z   CREATE SEQUENCE public.lote_venta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.lote_venta_id_seq;
       public          postgres    false    255    5         s           0    0    lote_venta_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.lote_venta_id_seq OWNED BY public.lote_venta.id;
          public          postgres    false    256                    1259    17613    marcado    TABLE     ~   CREATE TABLE public.marcado (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);
    DROP TABLE public.marcado;
       public         heap    postgres    false    5                    1259    17616    marcado_id_seq    SEQUENCE     w   CREATE SEQUENCE public.marcado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.marcado_id_seq;
       public          postgres    false    5    257         t           0    0    marcado_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.marcado_id_seq OWNED BY public.marcado.id;
          public          postgres    false    258                    1259    17617    medida    TABLE     �   CREATE TABLE public.medida (
    id bigint NOT NULL,
    "pesoKG" integer NOT NULL,
    "alturaCM" integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);
    DROP TABLE public.medida;
       public         heap    postgres    false    5                    1259    17620    medida_id_seq    SEQUENCE     v   CREATE SEQUENCE public.medida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.medida_id_seq;
       public          postgres    false    5    259         u           0    0    medida_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.medida_id_seq OWNED BY public.medida.id;
          public          postgres    false    260                    1259    17621 
   nacimiento    TABLE     �   CREATE TABLE public.nacimiento (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Fecundacion_id" bigint NOT NULL,
    fecha date NOT NULL
);
    DROP TABLE public.nacimiento;
       public         heap    postgres    false    5                    1259    17624    nacimiento_id_seq    SEQUENCE     z   CREATE SEQUENCE public.nacimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.nacimiento_id_seq;
       public          postgres    false    261    5         v           0    0    nacimiento_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.nacimiento_id_seq OWNED BY public.nacimiento.id;
          public          postgres    false    262                    1259    17625 	   palpacion    TABLE     �   CREATE TABLE public.palpacion (
    id bigint NOT NULL,
    resultado character varying(1) NOT NULL,
    observacion character varying(100),
    fecha date NOT NULL,
    id_fecundacion_id bigint NOT NULL
);
    DROP TABLE public.palpacion;
       public         heap    postgres    false    5                    1259    17628    palpacion_id_seq    SEQUENCE     y   CREATE SEQUENCE public.palpacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.palpacion_id_seq;
       public          postgres    false    263    5         w           0    0    palpacion_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.palpacion_id_seq OWNED BY public.palpacion.id;
          public          postgres    false    264         	           1259    17629    perdida    TABLE     �   CREATE TABLE public.perdida (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Palpacion_id" bigint NOT NULL,
    fecha date NOT NULL
);
    DROP TABLE public.perdida;
       public         heap    postgres    false    5         
           1259    17632    perdida_id_seq    SEQUENCE     w   CREATE SEQUENCE public.perdida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.perdida_id_seq;
       public          postgres    false    265    5         x           0    0    perdida_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.perdida_id_seq OWNED BY public.perdida.id;
          public          postgres    false    266                    1259    17633    raza    TABLE     �   CREATE TABLE public.raza (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public.raza;
       public         heap    postgres    false    5                    1259    17636    raza_id_seq    SEQUENCE     t   CREATE SEQUENCE public.raza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.raza_id_seq;
       public          postgres    false    5    267         y           0    0    raza_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.raza_id_seq OWNED BY public.raza.id;
          public          postgres    false    268                    1259    17637 	   tcurativo    TABLE     ?  CREATE TABLE public.tcurativo (
    id bigint NOT NULL,
    observacion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "fechaInicio" date NOT NULL,
    periodicidad integer NOT NULL,
    "fechaFin" date NOT NULL,
    nombre character varying(100) NOT NULL
);
    DROP TABLE public.tcurativo;
       public         heap    postgres    false    5                    1259    17640    tcurativo_id_seq    SEQUENCE     y   CREATE SEQUENCE public.tcurativo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.tcurativo_id_seq;
       public          postgres    false    5    269         z           0    0    tcurativo_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.tcurativo_id_seq OWNED BY public.tcurativo.id;
          public          postgres    false    270                    1259    17641 
   tipo_tprev    TABLE     �   CREATE TABLE public.tipo_tprev (
    id bigint NOT NULL,
    "nombreTipo" character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);
    DROP TABLE public.tipo_tprev;
       public         heap    postgres    false    5                    1259    17644    tipo_tprev_id_seq    SEQUENCE     z   CREATE SEQUENCE public.tipo_tprev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.tipo_tprev_id_seq;
       public          postgres    false    5    271         {           0    0    tipo_tprev_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.tipo_tprev_id_seq OWNED BY public.tipo_tprev.id;
          public          postgres    false    272                    1259    17645    tpreventivo    TABLE     h  CREATE TABLE public.tpreventivo (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    "fechaInicio" date NOT NULL,
    periodicidad integer NOT NULL,
    observacion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_TipoTPrev_id" bigint NOT NULL,
    "fechaFin" date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);
    DROP TABLE public.tpreventivo;
       public         heap    postgres    false    5                    1259    17648    tpreventivo_id_seq    SEQUENCE     {   CREATE SEQUENCE public.tpreventivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tpreventivo_id_seq;
       public          postgres    false    5    273         |           0    0    tpreventivo_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tpreventivo_id_seq OWNED BY public.tpreventivo.id;
          public          postgres    false    274                    1259    17649    vientre_id_seq    SEQUENCE     w   CREATE SEQUENCE public.vientre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.vientre_id_seq;
       public          postgres    false    254    5         }           0    0    vientre_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.vientre_id_seq OWNED BY public.fecundacion.id;
          public          postgres    false    275         �           2604    17650 	   Estado id    DEFAULT     j   ALTER TABLE ONLY public."Estado" ALTER COLUMN id SET DEFAULT nextval('public."Estado_id_seq"'::regclass);
 :   ALTER TABLE public."Estado" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    215         �           2604    17651    GrupoDestino id    DEFAULT     v   ALTER TABLE ONLY public."GrupoDestino" ALTER COLUMN id SET DEFAULT nextval('public."GrupoDestino_id_seq"'::regclass);
 @   ALTER TABLE public."GrupoDestino" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    217         �           2604    17652 	   animal id    DEFAULT     f   ALTER TABLE ONLY public.animal ALTER COLUMN id SET DEFAULT nextval('public.animal_id_seq'::regclass);
 8   ALTER TABLE public.animal ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219         �           2604    17653    animales_lote id    DEFAULT     t   ALTER TABLE ONLY public.animales_lote ALTER COLUMN id SET DEFAULT nextval('public.animales_lote_id_seq'::regclass);
 ?   ALTER TABLE public.animales_lote ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221         �           2604    17654    aplicacion_tp id    DEFAULT     t   ALTER TABLE ONLY public.aplicacion_tp ALTER COLUMN id SET DEFAULT nextval('public.aplicacion_tp_id_seq'::regclass);
 ?   ALTER TABLE public.aplicacion_tp ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    223         �           2604    17655    articulo id    DEFAULT     j   ALTER TABLE ONLY public.articulo ALTER COLUMN id SET DEFAULT nextval('public.articulo_id_seq'::regclass);
 :   ALTER TABLE public.articulo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    226    225         �           2604    17656 	   compra id    DEFAULT     f   ALTER TABLE ONLY public.compra ALTER COLUMN id SET DEFAULT nextval('public.compra_id_seq'::regclass);
 8   ALTER TABLE public.compra ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    240    239         �           2604    17657    comprador id    DEFAULT     l   ALTER TABLE ONLY public.comprador ALTER COLUMN id SET DEFAULT nextval('public.comprador_id_seq'::regclass);
 ;   ALTER TABLE public.comprador ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    242    241         �           2604    17658    detalle_compra id    DEFAULT     v   ALTER TABLE ONLY public.detalle_compra ALTER COLUMN id SET DEFAULT nextval('public.detalle_compra_id_seq'::regclass);
 @   ALTER TABLE public.detalle_compra ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    244    243         �           2604    17659    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    250    249         �           2604    17660    fallecimiento id    DEFAULT     t   ALTER TABLE ONLY public.fallecimiento ALTER COLUMN id SET DEFAULT nextval('public.fallecimiento_id_seq'::regclass);
 ?   ALTER TABLE public.fallecimiento ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    253    252         �           2604    17661    fecundacion id    DEFAULT     l   ALTER TABLE ONLY public.fecundacion ALTER COLUMN id SET DEFAULT nextval('public.vientre_id_seq'::regclass);
 =   ALTER TABLE public.fecundacion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    275    254         �           2604    17662    lote_venta id    DEFAULT     n   ALTER TABLE ONLY public.lote_venta ALTER COLUMN id SET DEFAULT nextval('public.lote_venta_id_seq'::regclass);
 <   ALTER TABLE public.lote_venta ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    256    255         �           2604    17663 
   marcado id    DEFAULT     h   ALTER TABLE ONLY public.marcado ALTER COLUMN id SET DEFAULT nextval('public.marcado_id_seq'::regclass);
 9   ALTER TABLE public.marcado ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    258    257         �           2604    17664 	   medida id    DEFAULT     f   ALTER TABLE ONLY public.medida ALTER COLUMN id SET DEFAULT nextval('public.medida_id_seq'::regclass);
 8   ALTER TABLE public.medida ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    260    259         �           2604    17665    nacimiento id    DEFAULT     n   ALTER TABLE ONLY public.nacimiento ALTER COLUMN id SET DEFAULT nextval('public.nacimiento_id_seq'::regclass);
 <   ALTER TABLE public.nacimiento ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    262    261         �           2604    17666    palpacion id    DEFAULT     l   ALTER TABLE ONLY public.palpacion ALTER COLUMN id SET DEFAULT nextval('public.palpacion_id_seq'::regclass);
 ;   ALTER TABLE public.palpacion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    264    263         �           2604    17667 
   perdida id    DEFAULT     h   ALTER TABLE ONLY public.perdida ALTER COLUMN id SET DEFAULT nextval('public.perdida_id_seq'::regclass);
 9   ALTER TABLE public.perdida ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    266    265         �           2604    17668    raza id    DEFAULT     b   ALTER TABLE ONLY public.raza ALTER COLUMN id SET DEFAULT nextval('public.raza_id_seq'::regclass);
 6   ALTER TABLE public.raza ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    268    267         �           2604    17669    tcurativo id    DEFAULT     l   ALTER TABLE ONLY public.tcurativo ALTER COLUMN id SET DEFAULT nextval('public.tcurativo_id_seq'::regclass);
 ;   ALTER TABLE public.tcurativo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    270    269         �           2604    17670    tipo_tprev id    DEFAULT     n   ALTER TABLE ONLY public.tipo_tprev ALTER COLUMN id SET DEFAULT nextval('public.tipo_tprev_id_seq'::regclass);
 <   ALTER TABLE public.tipo_tprev ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    272    271         �           2604    17671    tpreventivo id    DEFAULT     p   ALTER TABLE ONLY public.tpreventivo ALTER COLUMN id SET DEFAULT nextval('public.tpreventivo_id_seq'::regclass);
 =   ALTER TABLE public.tpreventivo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    274    273         $          0    17518    Estado 
   TABLE DATA           C   COPY public."Estado" (id, descripcion, fecha_creacion) FROM stdin;
    public          postgres    false    215       5156.dat &          0    17522    GrupoDestino 
   TABLE DATA           I   COPY public."GrupoDestino" (id, descripcion, fecha_creacion) FROM stdin;
    public          postgres    false    217       5158.dat (          0    17526    animal 
   TABLE DATA              COPY public.animal (id, "id_Animal", fecha_nacimiento, sexo, estado_id, fecha_creacion, grupo_destino_id, raza_id) FROM stdin;
    public          postgres    false    219       5160.dat *          0    17530    animales_lote 
   TABLE DATA           Y   COPY public.animales_lote (id, fecha_creacion, "id_Animal_id", "id_Lote_id") FROM stdin;
    public          postgres    false    221       5162.dat ,          0    17534    aplicacion_tp 
   TABLE DATA           `   COPY public.aplicacion_tp (id, fecha_creacion, "id_Animal_id", "id_TPreventivo_id") FROM stdin;
    public          postgres    false    223       5164.dat .          0    17538    articulo 
   TABLE DATA           a   COPY public.articulo (id, nombre, "cantidadActual", "cantidadIdeal", fecha_creacion) FROM stdin;
    public          postgres    false    225       5166.dat 0          0    17542 
   auth_group 
   TABLE DATA           .   COPY public.auth_group (id, name) FROM stdin;
    public          postgres    false    227       5168.dat 2          0    17546    auth_group_permissions 
   TABLE DATA           M   COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public          postgres    false    229       5170.dat 4          0    17550    auth_permission 
   TABLE DATA           N   COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
    public          postgres    false    231       5172.dat 6          0    17554 	   auth_user 
   TABLE DATA           �   COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public          postgres    false    233       5174.dat 7          0    17559    auth_user_groups 
   TABLE DATA           A   COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
    public          postgres    false    234       5175.dat :          0    17564    auth_user_user_permissions 
   TABLE DATA           P   COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public          postgres    false    237       5178.dat <          0    17568    compra 
   TABLE DATA           R   COPY public.compra (id, "fechaCompra", "totalCompra", fecha_creacion) FROM stdin;
    public          postgres    false    239       5180.dat >          0    17572 	   comprador 
   TABLE DATA           O   COPY public.comprador (id, nombre, telefono, mail, fecha_creacion) FROM stdin;
    public          postgres    false    241       5182.dat @          0    17576    detalle_compra 
   TABLE DATA           h   COPY public.detalle_compra (id, cantidad, fecha_creacion, "id_Articulo_id", "id_Compra_id") FROM stdin;
    public          postgres    false    243       5184.dat B          0    17580    django_admin_log 
   TABLE DATA           �   COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public          postgres    false    245       5186.dat D          0    17587    django_content_type 
   TABLE DATA           C   COPY public.django_content_type (id, app_label, model) FROM stdin;
    public          postgres    false    247       5188.dat F          0    17591    django_migrations 
   TABLE DATA           C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public          postgres    false    249       5190.dat H          0    17597    django_session 
   TABLE DATA           P   COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
    public          postgres    false    251       5192.dat I          0    17602    fallecimiento 
   TABLE DATA           R   COPY public.fallecimiento (id, causa, fecha_creacion, "id_Animal_id") FROM stdin;
    public          postgres    false    252       5193.dat K          0    17606    fecundacion 
   TABLE DATA           J   COPY public.fecundacion (id, fecha, id_madre_id, id_padre_id) FROM stdin;
    public          postgres    false    254       5195.dat L          0    17609 
   lote_venta 
   TABLE DATA           r   COPY public.lote_venta (id, estado, fecha_envio, fecha_confirmado, fecha_creacion, "id_Comprador_id") FROM stdin;
    public          postgres    false    255       5196.dat N          0    17613    marcado 
   TABLE DATA           E   COPY public.marcado (id, fecha_creacion, "id_Animal_id") FROM stdin;
    public          postgres    false    257       5198.dat P          0    17617    medida 
   TABLE DATA           Z   COPY public.medida (id, "pesoKG", "alturaCM", fecha_creacion, "id_Animal_id") FROM stdin;
    public          postgres    false    259       5200.dat R          0    17621 
   nacimiento 
   TABLE DATA           d   COPY public.nacimiento (id, fecha_creacion, "id_Animal_id", "id_Fecundacion_id", fecha) FROM stdin;
    public          postgres    false    261       5202.dat T          0    17625 	   palpacion 
   TABLE DATA           Y   COPY public.palpacion (id, resultado, observacion, fecha, id_fecundacion_id) FROM stdin;
    public          postgres    false    263       5204.dat V          0    17629    perdida 
   TABLE DATA           V   COPY public.perdida (id, causa, fecha_creacion, "id_Palpacion_id", fecha) FROM stdin;
    public          postgres    false    265       5206.dat X          0    17633    raza 
   TABLE DATA           ?   COPY public.raza (id, descripcion, fecha_creacion) FROM stdin;
    public          postgres    false    267       5208.dat Z          0    17637 	   tcurativo 
   TABLE DATA           �   COPY public.tcurativo (id, observacion, fecha_creacion, "id_Animal_id", "fechaInicio", periodicidad, "fechaFin", nombre) FROM stdin;
    public          postgres    false    269       5210.dat \          0    17641 
   tipo_tprev 
   TABLE DATA           F   COPY public.tipo_tprev (id, "nombreTipo", fecha_creacion) FROM stdin;
    public          postgres    false    271       5212.dat ^          0    17645    tpreventivo 
   TABLE DATA           �   COPY public.tpreventivo (id, nombre, "fechaInicio", periodicidad, observacion, fecha_creacion, "id_TipoTPrev_id", "fechaFin", "id_Animal_id") FROM stdin;
    public          postgres    false    273       5214.dat ~           0    0    Estado_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."Estado_id_seq"', 5, true);
          public          postgres    false    216                    0    0    GrupoDestino_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."GrupoDestino_id_seq"', 2, true);
          public          postgres    false    218         �           0    0    animal_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.animal_id_seq', 4, true);
          public          postgres    false    220         �           0    0    animales_lote_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.animales_lote_id_seq', 1, false);
          public          postgres    false    222         �           0    0    aplicacion_tp_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.aplicacion_tp_id_seq', 1, false);
          public          postgres    false    224         �           0    0    articulo_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.articulo_id_seq', 1, true);
          public          postgres    false    226         �           0    0    auth_group_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);
          public          postgres    false    228         �           0    0    auth_group_permissions_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 192, true);
          public          postgres    false    230         �           0    0    auth_permission_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_permission_id_seq', 108, true);
          public          postgres    false    232         �           0    0    auth_user_groups_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 2, true);
          public          postgres    false    235         �           0    0    auth_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);
          public          postgres    false    236         �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);
          public          postgres    false    238         �           0    0    compra_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.compra_id_seq', 1, false);
          public          postgres    false    240         �           0    0    comprador_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.comprador_id_seq', 1, false);
          public          postgres    false    242         �           0    0    detalle_compra_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.detalle_compra_id_seq', 1, false);
          public          postgres    false    244         �           0    0    django_admin_log_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.django_admin_log_id_seq', 9, true);
          public          postgres    false    246         �           0    0    django_content_type_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.django_content_type_id_seq', 27, true);
          public          postgres    false    248         �           0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 20, true);
          public          postgres    false    250         �           0    0    fallecimiento_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.fallecimiento_id_seq', 1, false);
          public          postgres    false    253         �           0    0    lote_venta_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.lote_venta_id_seq', 1, false);
          public          postgres    false    256         �           0    0    marcado_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.marcado_id_seq', 1, false);
          public          postgres    false    258         �           0    0    medida_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.medida_id_seq', 6, true);
          public          postgres    false    260         �           0    0    nacimiento_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.nacimiento_id_seq', 1, false);
          public          postgres    false    262         �           0    0    palpacion_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.palpacion_id_seq', 1, true);
          public          postgres    false    264         �           0    0    perdida_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.perdida_id_seq', 1, false);
          public          postgres    false    266         �           0    0    raza_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.raza_id_seq', 4, true);
          public          postgres    false    268         �           0    0    tcurativo_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.tcurativo_id_seq', 1, false);
          public          postgres    false    270         �           0    0    tipo_tprev_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.tipo_tprev_id_seq', 2, true);
          public          postgres    false    272         �           0    0    tpreventivo_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.tpreventivo_id_seq', 1, true);
          public          postgres    false    274         �           0    0    vientre_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.vientre_id_seq', 1, true);
          public          postgres    false    275         �           2606    17673    Estado Estado_descripcion_key 
   CONSTRAINT     c   ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_descripcion_key" UNIQUE (descripcion);
 K   ALTER TABLE ONLY public."Estado" DROP CONSTRAINT "Estado_descripcion_key";
       public            postgres    false    215                     2606    17675    Estado Estado_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_pkey" PRIMARY KEY (id);
 @   ALTER TABLE ONLY public."Estado" DROP CONSTRAINT "Estado_pkey";
       public            postgres    false    215                    2606    17677 )   GrupoDestino GrupoDestino_descripcion_key 
   CONSTRAINT     o   ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_descripcion_key" UNIQUE (descripcion);
 W   ALTER TABLE ONLY public."GrupoDestino" DROP CONSTRAINT "GrupoDestino_descripcion_key";
       public            postgres    false    217                    2606    17679    GrupoDestino GrupoDestino_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_pkey" PRIMARY KEY (id);
 L   ALTER TABLE ONLY public."GrupoDestino" DROP CONSTRAINT "GrupoDestino_pkey";
       public            postgres    false    217         
           2606    17681    animal animal_id_Animal_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_id_Animal_key" UNIQUE ("id_Animal");
 G   ALTER TABLE ONLY public.animal DROP CONSTRAINT "animal_id_Animal_key";
       public            postgres    false    219                    2606    17683    animal animal_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.animal DROP CONSTRAINT animal_pkey;
       public            postgres    false    219                    2606    17685     animales_lote animales_lote_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT animales_lote_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.animales_lote DROP CONSTRAINT animales_lote_pkey;
       public            postgres    false    221                    2606    17687     aplicacion_tp aplicacion_tp_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT aplicacion_tp_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.aplicacion_tp DROP CONSTRAINT aplicacion_tp_pkey;
       public            postgres    false    223                    2606    17689    articulo articulo_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.articulo
    ADD CONSTRAINT articulo_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.articulo DROP CONSTRAINT articulo_pkey;
       public            postgres    false    225                    2606    17691    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public            postgres    false    227                    2606    17693 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public            postgres    false    229    229         "           2606    17695 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public            postgres    false    229                    2606    17697    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public            postgres    false    227         %           2606    17699 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public            postgres    false    231    231         '           2606    17701 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public            postgres    false    231         /           2606    17703 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public            postgres    false    234         2           2606    17705 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public            postgres    false    234    234         )           2606    17707    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public            postgres    false    233         5           2606    17709 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public            postgres    false    237         8           2606    17711 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public            postgres    false    237    237         ,           2606    17713     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public            postgres    false    233         :           2606    17715    compra compra_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.compra DROP CONSTRAINT compra_pkey;
       public            postgres    false    239         <           2606    17717    comprador comprador_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.comprador
    ADD CONSTRAINT comprador_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.comprador DROP CONSTRAINT comprador_pkey;
       public            postgres    false    241         @           2606    17719 "   detalle_compra detalle_compra_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT detalle_compra_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.detalle_compra DROP CONSTRAINT detalle_compra_pkey;
       public            postgres    false    243         C           2606    17721 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public            postgres    false    245         F           2606    17723 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public            postgres    false    247    247         H           2606    17725 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public            postgres    false    247         J           2606    17727 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public            postgres    false    249         M           2606    17729 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public            postgres    false    251         Q           2606    17731     fallecimiento fallecimiento_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT fallecimiento_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.fallecimiento DROP CONSTRAINT fallecimiento_pkey;
       public            postgres    false    252         X           2606    17733    lote_venta lote_venta_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT lote_venta_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.lote_venta DROP CONSTRAINT lote_venta_pkey;
       public            postgres    false    255         [           2606    17735    marcado marcado_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT marcado_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.marcado DROP CONSTRAINT marcado_pkey;
       public            postgres    false    257         ^           2606    17737    medida medida_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.medida
    ADD CONSTRAINT medida_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.medida DROP CONSTRAINT medida_pkey;
       public            postgres    false    259         b           2606    17739    nacimiento nacimiento_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT nacimiento_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.nacimiento DROP CONSTRAINT nacimiento_pkey;
       public            postgres    false    261         e           2606    17741    palpacion palpacion_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.palpacion DROP CONSTRAINT palpacion_pkey;
       public            postgres    false    263         h           2606    17743    perdida perdida_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT perdida_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.perdida DROP CONSTRAINT perdida_pkey;
       public            postgres    false    265         k           2606    17745    raza raza_descripcion_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_descripcion_key UNIQUE (descripcion);
 C   ALTER TABLE ONLY public.raza DROP CONSTRAINT raza_descripcion_key;
       public            postgres    false    267         m           2606    17747    raza raza_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.raza DROP CONSTRAINT raza_pkey;
       public            postgres    false    267         p           2606    17749    tcurativo tcurativo_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT tcurativo_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.tcurativo DROP CONSTRAINT tcurativo_pkey;
       public            postgres    false    269         r           2606    17751    tipo_tprev tipo_tprev_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.tipo_tprev
    ADD CONSTRAINT tipo_tprev_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.tipo_tprev DROP CONSTRAINT tipo_tprev_pkey;
       public            postgres    false    271         u           2606    17753    tpreventivo tpreventivo_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.tpreventivo DROP CONSTRAINT tpreventivo_pkey;
       public            postgres    false    273         U           2606    17755    fecundacion vientre_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.fecundacion DROP CONSTRAINT vientre_pkey;
       public            postgres    false    254         �           1259    17756     Estado_descripcion_b8dc8091_like    INDEX     r   CREATE INDEX "Estado_descripcion_b8dc8091_like" ON public."Estado" USING btree (descripcion varchar_pattern_ops);
 6   DROP INDEX public."Estado_descripcion_b8dc8091_like";
       public            postgres    false    215                    1259    17757 &   GrupoDestino_descripcion_2c045343_like    INDEX     ~   CREATE INDEX "GrupoDestino_descripcion_2c045343_like" ON public."GrupoDestino" USING btree (descripcion varchar_pattern_ops);
 <   DROP INDEX public."GrupoDestino_descripcion_2c045343_like";
       public            postgres    false    217                    1259    17758    animal_estado_id_944692c8    INDEX     Q   CREATE INDEX animal_estado_id_944692c8 ON public.animal USING btree (estado_id);
 -   DROP INDEX public.animal_estado_id_944692c8;
       public            postgres    false    219                    1259    17759     animal_grupo_destino_id_b68236c3    INDEX     _   CREATE INDEX animal_grupo_destino_id_b68236c3 ON public.animal USING btree (grupo_destino_id);
 4   DROP INDEX public.animal_grupo_destino_id_b68236c3;
       public            postgres    false    219                    1259    17760    animal_id_Animal_38e077e5_like    INDEX     n   CREATE INDEX "animal_id_Animal_38e077e5_like" ON public.animal USING btree ("id_Animal" varchar_pattern_ops);
 4   DROP INDEX public."animal_id_Animal_38e077e5_like";
       public            postgres    false    219                    1259    17761    animal_raza_id_01c071b4    INDEX     M   CREATE INDEX animal_raza_id_01c071b4 ON public.animal USING btree (raza_id);
 +   DROP INDEX public.animal_raza_id_01c071b4;
       public            postgres    false    219                    1259    17762 #   animales_lote_id_Animal_id_6feaf46b    INDEX     i   CREATE INDEX "animales_lote_id_Animal_id_6feaf46b" ON public.animales_lote USING btree ("id_Animal_id");
 9   DROP INDEX public."animales_lote_id_Animal_id_6feaf46b";
       public            postgres    false    221                    1259    17763 !   animales_lote_id_Lote_id_31dec68e    INDEX     e   CREATE INDEX "animales_lote_id_Lote_id_31dec68e" ON public.animales_lote USING btree ("id_Lote_id");
 7   DROP INDEX public."animales_lote_id_Lote_id_31dec68e";
       public            postgres    false    221                    1259    17764 #   aplicacion_tp_id_Animal_id_b8e2b1fd    INDEX     i   CREATE INDEX "aplicacion_tp_id_Animal_id_b8e2b1fd" ON public.aplicacion_tp USING btree ("id_Animal_id");
 9   DROP INDEX public."aplicacion_tp_id_Animal_id_b8e2b1fd";
       public            postgres    false    223                    1259    17765 (   aplicacion_tp_id_TPreventivo_id_26ea59df    INDEX     s   CREATE INDEX "aplicacion_tp_id_TPreventivo_id_26ea59df" ON public.aplicacion_tp USING btree ("id_TPreventivo_id");
 >   DROP INDEX public."aplicacion_tp_id_TPreventivo_id_26ea59df";
       public            postgres    false    223                    1259    17766    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public            postgres    false    227                    1259    17767 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public            postgres    false    229                     1259    17768 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public            postgres    false    229         #           1259    17769 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public            postgres    false    231         -           1259    17770 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public            postgres    false    234         0           1259    17771 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public            postgres    false    234         3           1259    17772 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public            postgres    false    237         6           1259    17773 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public            postgres    false    237         *           1259    17774     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public            postgres    false    233         =           1259    17775 &   detalle_compra_id_Articulo_id_ba37a9fa    INDEX     o   CREATE INDEX "detalle_compra_id_Articulo_id_ba37a9fa" ON public.detalle_compra USING btree ("id_Articulo_id");
 <   DROP INDEX public."detalle_compra_id_Articulo_id_ba37a9fa";
       public            postgres    false    243         >           1259    17776 $   detalle_compra_id_Compra_id_43b8a91e    INDEX     k   CREATE INDEX "detalle_compra_id_Compra_id_43b8a91e" ON public.detalle_compra USING btree ("id_Compra_id");
 :   DROP INDEX public."detalle_compra_id_Compra_id_43b8a91e";
       public            postgres    false    243         A           1259    17777 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public            postgres    false    245         D           1259    17778 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public            postgres    false    245         K           1259    17779 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public            postgres    false    251         N           1259    17780 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public            postgres    false    251         O           1259    17781 #   fallecimiento_id_Animal_id_29c69888    INDEX     i   CREATE INDEX "fallecimiento_id_Animal_id_29c69888" ON public.fallecimiento USING btree ("id_Animal_id");
 9   DROP INDEX public."fallecimiento_id_Animal_id_29c69888";
       public            postgres    false    252         V           1259    17782 #   lote_venta_id_Comprador_id_3c61c9be    INDEX     i   CREATE INDEX "lote_venta_id_Comprador_id_3c61c9be" ON public.lote_venta USING btree ("id_Comprador_id");
 9   DROP INDEX public."lote_venta_id_Comprador_id_3c61c9be";
       public            postgres    false    255         Y           1259    17783    marcado_id_Animal_id_3776f20e    INDEX     ]   CREATE INDEX "marcado_id_Animal_id_3776f20e" ON public.marcado USING btree ("id_Animal_id");
 3   DROP INDEX public."marcado_id_Animal_id_3776f20e";
       public            postgres    false    257         \           1259    17784    medida_id_Animal_id_0432812b    INDEX     [   CREATE INDEX "medida_id_Animal_id_0432812b" ON public.medida USING btree ("id_Animal_id");
 2   DROP INDEX public."medida_id_Animal_id_0432812b";
       public            postgres    false    259         _           1259    17785     nacimiento_id_Animal_id_de50946e    INDEX     c   CREATE INDEX "nacimiento_id_Animal_id_de50946e" ON public.nacimiento USING btree ("id_Animal_id");
 6   DROP INDEX public."nacimiento_id_Animal_id_de50946e";
       public            postgres    false    261         `           1259    17786 !   nacimiento_id_Vientre_id_66d6bd7f    INDEX     i   CREATE INDEX "nacimiento_id_Vientre_id_66d6bd7f" ON public.nacimiento USING btree ("id_Fecundacion_id");
 7   DROP INDEX public."nacimiento_id_Vientre_id_66d6bd7f";
       public            postgres    false    261         c           1259    17787     palpacion_id_vientre_id_6762db22    INDEX     c   CREATE INDEX palpacion_id_vientre_id_6762db22 ON public.palpacion USING btree (id_fecundacion_id);
 4   DROP INDEX public.palpacion_id_vientre_id_6762db22;
       public            postgres    false    263         f           1259    17788     perdida_id_Palpacion_id_7f0a21a1    INDEX     c   CREATE INDEX "perdida_id_Palpacion_id_7f0a21a1" ON public.perdida USING btree ("id_Palpacion_id");
 6   DROP INDEX public."perdida_id_Palpacion_id_7f0a21a1";
       public            postgres    false    265         i           1259    17789    raza_descripcion_320960d6_like    INDEX     j   CREATE INDEX raza_descripcion_320960d6_like ON public.raza USING btree (descripcion varchar_pattern_ops);
 2   DROP INDEX public.raza_descripcion_320960d6_like;
       public            postgres    false    267         n           1259    17790    tcurativo_id_Animal_id_8970efeb    INDEX     a   CREATE INDEX "tcurativo_id_Animal_id_8970efeb" ON public.tcurativo USING btree ("id_Animal_id");
 5   DROP INDEX public."tcurativo_id_Animal_id_8970efeb";
       public            postgres    false    269         s           1259    17791 $   tpreventivo_id_TipoTPrev_id_9d33de50    INDEX     k   CREATE INDEX "tpreventivo_id_TipoTPrev_id_9d33de50" ON public.tpreventivo USING btree ("id_TipoTPrev_id");
 :   DROP INDEX public."tpreventivo_id_TipoTPrev_id_9d33de50";
       public            postgres    false    273         R           1259    17792    vientre_id_madre_id_b449b968    INDEX     [   CREATE INDEX vientre_id_madre_id_b449b968 ON public.fecundacion USING btree (id_madre_id);
 0   DROP INDEX public.vientre_id_madre_id_b449b968;
       public            postgres    false    254         S           1259    17793    vientre_id_padre_id_d133132e    INDEX     [   CREATE INDEX vientre_id_padre_id_d133132e ON public.fecundacion USING btree (id_padre_id);
 0   DROP INDEX public.vientre_id_padre_id_d133132e;
       public            postgres    false    254         v           2606    17794 -   animal animal_estado_id_944692c8_fk_Estado_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_estado_id_944692c8_fk_Estado_id" FOREIGN KEY (estado_id) REFERENCES public."Estado"(id) DEFERRABLE INITIALLY DEFERRED;
 Y   ALTER TABLE ONLY public.animal DROP CONSTRAINT "animal_estado_id_944692c8_fk_Estado_id";
       public          postgres    false    219    4864    215         w           2606    17799 :   animal animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id" FOREIGN KEY (grupo_destino_id) REFERENCES public."GrupoDestino"(id) DEFERRABLE INITIALLY DEFERRED;
 f   ALTER TABLE ONLY public.animal DROP CONSTRAINT "animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id";
       public          postgres    false    217    219    4869         x           2606    17804 )   animal animal_raza_id_01c071b4_fk_raza_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_raza_id_01c071b4_fk_raza_id FOREIGN KEY (raza_id) REFERENCES public.raza(id) DEFERRABLE INITIALLY DEFERRED;
 S   ALTER TABLE ONLY public.animal DROP CONSTRAINT animal_raza_id_01c071b4_fk_raza_id;
       public          postgres    false    4973    267    219         y           2606    17809 >   animales_lote animales_lote_id_Animal_id_6feaf46b_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Animal_id_6feaf46b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.animales_lote DROP CONSTRAINT "animales_lote_id_Animal_id_6feaf46b_fk_animal_id";
       public          postgres    false    221    4876    219         z           2606    17814 @   animales_lote animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id" FOREIGN KEY ("id_Lote_id") REFERENCES public.lote_venta(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.animales_lote DROP CONSTRAINT "animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id";
       public          postgres    false    4952    255    221         {           2606    17819 >   aplicacion_tp aplicacion_tp_id_Animal_id_b8e2b1fd_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT "aplicacion_tp_id_Animal_id_b8e2b1fd_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.aplicacion_tp DROP CONSTRAINT "aplicacion_tp_id_Animal_id_b8e2b1fd_fk_animal_id";
       public          postgres    false    4876    219    223         |           2606    17824 H   aplicacion_tp aplicacion_tp_id_TPreventivo_id_26ea59df_fk_tpreventivo_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT "aplicacion_tp_id_TPreventivo_id_26ea59df_fk_tpreventivo_id" FOREIGN KEY ("id_TPreventivo_id") REFERENCES public.tpreventivo(id) DEFERRABLE INITIALLY DEFERRED;
 t   ALTER TABLE ONLY public.aplicacion_tp DROP CONSTRAINT "aplicacion_tp_id_TPreventivo_id_26ea59df_fk_tpreventivo_id";
       public          postgres    false    223    273    4981         }           2606    17829 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public          postgres    false    231    4903    229         ~           2606    17834 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public          postgres    false    4892    227    229                    2606    17839 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public          postgres    false    231    247    4936         �           2606    17844 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public          postgres    false    234    227    4892         �           2606    17849 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public          postgres    false    234    233    4905         �           2606    17854 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public          postgres    false    237    231    4903         �           2606    17859 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public          postgres    false    233    237    4905         �           2606    17864 D   detalle_compra detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id" FOREIGN KEY ("id_Articulo_id") REFERENCES public.articulo(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.detalle_compra DROP CONSTRAINT "detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id";
       public          postgres    false    4887    243    225         �           2606    17869 @   detalle_compra detalle_compra_id_Compra_id_43b8a91e_fk_compra_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Compra_id_43b8a91e_fk_compra_id" FOREIGN KEY ("id_Compra_id") REFERENCES public.compra(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.detalle_compra DROP CONSTRAINT "detalle_compra_id_Compra_id_43b8a91e_fk_compra_id";
       public          postgres    false    239    4922    243         �           2606    17874 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public          postgres    false    247    245    4936         �           2606    17879 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public          postgres    false    233    4905    245         �           2606    17884 >   fallecimiento fallecimiento_id_Animal_id_29c69888_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT "fallecimiento_id_Animal_id_29c69888_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.fallecimiento DROP CONSTRAINT "fallecimiento_id_Animal_id_29c69888_fk_animal_id";
       public          postgres    false    252    219    4876         �           2606    17889 >   lote_venta lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT "lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id" FOREIGN KEY ("id_Comprador_id") REFERENCES public.comprador(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.lote_venta DROP CONSTRAINT "lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id";
       public          postgres    false    241    255    4924         �           2606    17894 2   marcado marcado_id_Animal_id_3776f20e_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT "marcado_id_Animal_id_3776f20e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 ^   ALTER TABLE ONLY public.marcado DROP CONSTRAINT "marcado_id_Animal_id_3776f20e_fk_animal_id";
       public          postgres    false    257    219    4876         �           2606    17899 0   medida medida_id_Animal_id_0432812b_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.medida
    ADD CONSTRAINT "medida_id_Animal_id_0432812b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 \   ALTER TABLE ONLY public.medida DROP CONSTRAINT "medida_id_Animal_id_0432812b_fk_animal_id";
       public          postgres    false    219    4876    259         �           2606    17904 8   nacimiento nacimiento_id_Animal_id_de50946e_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Animal_id_de50946e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 d   ALTER TABLE ONLY public.nacimiento DROP CONSTRAINT "nacimiento_id_Animal_id_de50946e_fk_animal_id";
       public          postgres    false    261    4876    219         �           2606    17909 :   nacimiento nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id" FOREIGN KEY ("id_Fecundacion_id") REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;
 f   ALTER TABLE ONLY public.nacimiento DROP CONSTRAINT "nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id";
       public          postgres    false    261    4949    254         �           2606    17914 8   palpacion palpacion_id_vientre_id_6762db22_fk_vientre_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_id_vientre_id_6762db22_fk_vientre_id FOREIGN KEY (id_fecundacion_id) REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;
 b   ALTER TABLE ONLY public.palpacion DROP CONSTRAINT palpacion_id_vientre_id_6762db22_fk_vientre_id;
       public          postgres    false    263    4949    254         �           2606    17919 8   perdida perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT "perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id" FOREIGN KEY ("id_Palpacion_id") REFERENCES public.palpacion(id) DEFERRABLE INITIALLY DEFERRED;
 d   ALTER TABLE ONLY public.perdida DROP CONSTRAINT "perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id";
       public          postgres    false    263    4965    265         �           2606    17924 6   tcurativo tcurativo_id_Animal_id_8970efeb_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT "tcurativo_id_Animal_id_8970efeb_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 b   ALTER TABLE ONLY public.tcurativo DROP CONSTRAINT "tcurativo_id_Animal_id_8970efeb_fk_animal_id";
       public          postgres    false    219    4876    269         �           2606    17929 !   tpreventivo tpreventivo_animal_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_animal_fk FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id);
 K   ALTER TABLE ONLY public.tpreventivo DROP CONSTRAINT tpreventivo_animal_fk;
       public          postgres    false    273    4876    219         �           2606    17934 A   tpreventivo tpreventivo_id_TipoTPrev_id_9d33de50_fk_tipo_tprev_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT "tpreventivo_id_TipoTPrev_id_9d33de50_fk_tipo_tprev_id" FOREIGN KEY ("id_TipoTPrev_id") REFERENCES public.tipo_tprev(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY public.tpreventivo DROP CONSTRAINT "tpreventivo_id_TipoTPrev_id_9d33de50_fk_tipo_tprev_id";
       public          postgres    false    273    271    4978         �           2606    17939 5   fecundacion vientre_id_madre_id_b449b968_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_madre_id_b449b968_fk_animal_id FOREIGN KEY (id_madre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 _   ALTER TABLE ONLY public.fecundacion DROP CONSTRAINT vientre_id_madre_id_b449b968_fk_animal_id;
       public          postgres    false    254    219    4876         �           2606    17944 5   fecundacion vientre_id_padre_id_d133132e_fk_animal_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_padre_id_d133132e_fk_animal_id FOREIGN KEY (id_padre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;
 _   ALTER TABLE ONLY public.fecundacion DROP CONSTRAINT vientre_id_padre_id_d133132e_fk_animal_id;
       public          postgres    false    254    4876    219                                    5156.dat                                                                                            0000600 0004000 0002000 00000000100 14665456514 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Vivo	2023-03-30
2	Muerto	2023-03-30
3	Vendido	2023-03-30
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                5158.dat                                                                                            0000600 0004000 0002000 00000000065 14665456514 0014275 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Engorde	2023-03-30
2	Reproducción	2023-03-30
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5160.dat                                                                                            0000600 0004000 0002000 00000000231 14665456514 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	M001	2020-03-30	M	1	2023-03-30	2	1
3	AAAO	2023-09-26	F	1	2023-09-26	1	4
4	M002	2021-03-30	M	1	2023-03-30	2	1
1	F001	2020-03-30	F	1	2023-03-30	2	1
\.


                                                                                                                                                                                                                                                                                                                                                                       5162.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5164.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5166.dat                                                                                            0000600 0004000 0002000 00000000040 14665456514 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Forraje	10	60	2023-05-18
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                5168.dat                                                                                            0000600 0004000 0002000 00000000060 14665456514 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Administrador
2	Cajero
3	Compra
4	Animal
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                5170.dat                                                                                            0000600 0004000 0002000 00000003142 14665456514 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
6	1	6
7	1	7
8	1	8
9	1	9
10	1	10
11	1	11
12	1	12
13	1	13
14	1	14
15	1	15
16	1	16
17	1	17
18	1	18
19	1	19
20	1	20
21	1	21
22	1	22
23	1	23
24	1	24
25	1	25
26	1	26
27	1	27
28	1	28
29	1	29
30	1	30
31	1	31
32	1	32
33	1	33
34	1	34
35	1	35
36	1	36
37	1	37
38	1	38
39	1	39
40	1	40
41	1	41
42	1	42
43	1	43
44	1	44
45	1	45
46	1	46
47	1	47
48	1	48
49	1	49
50	1	50
51	1	51
52	1	52
53	1	53
54	1	54
55	1	55
56	1	56
57	1	57
58	1	58
59	1	59
60	1	60
61	1	61
62	1	62
63	1	63
64	1	64
65	1	65
66	1	66
67	1	67
68	1	68
69	1	69
70	1	70
71	1	71
72	1	72
73	1	73
74	1	74
75	1	75
76	1	76
77	1	77
78	1	78
79	1	79
80	1	80
81	1	81
82	1	82
83	1	83
84	1	84
85	1	85
86	1	86
87	1	87
88	1	88
89	1	89
90	1	90
91	1	91
92	1	92
93	1	93
94	1	94
95	1	95
96	1	96
97	1	97
98	1	98
99	1	99
100	1	100
101	1	101
102	1	102
103	1	103
104	1	104
105	1	105
106	1	106
107	1	107
108	1	108
109	3	97
110	3	98
111	3	99
112	3	100
113	3	101
114	3	102
115	3	103
116	3	104
117	3	105
118	3	106
119	3	107
120	3	108
121	3	85
122	3	86
123	3	87
124	3	88
125	4	25
126	4	26
127	4	27
128	4	28
129	4	29
130	4	30
131	4	31
132	4	32
133	4	33
134	4	34
135	4	35
136	4	36
137	4	37
138	4	38
139	4	39
140	4	40
141	4	41
142	4	42
143	4	43
144	4	44
145	4	45
146	4	46
147	4	47
148	4	48
149	4	49
150	4	50
151	4	51
152	4	52
153	4	53
154	4	54
155	4	55
156	4	56
157	4	57
158	4	58
159	4	59
160	4	60
161	4	61
162	4	62
163	4	63
164	4	64
165	4	65
166	4	66
167	4	67
168	4	68
169	4	69
170	4	70
171	4	71
172	4	72
173	4	73
174	4	74
175	4	75
176	4	76
177	4	77
178	4	78
179	4	79
180	4	80
181	4	81
182	4	82
183	4	83
184	4	84
185	4	89
186	4	90
187	4	91
188	4	92
189	4	93
190	4	94
191	4	95
192	4	96
\.


                                                                                                                                                                                                                                                                                                                                                                                                                              5172.dat                                                                                            0000600 0004000 0002000 00000010403 14665456514 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add Raza	7	add_raza
26	Can change Raza	7	change_raza
27	Can delete Raza	7	delete_raza
28	Can view Raza	7	view_raza
29	Can add GrupoDestino	8	add_grupodestino
30	Can change GrupoDestino	8	change_grupodestino
31	Can delete GrupoDestino	8	delete_grupodestino
32	Can view GrupoDestino	8	view_grupodestino
33	Can add Estado	9	add_estado
34	Can change Estado	9	change_estado
35	Can delete Estado	9	delete_estado
36	Can view Estado	9	view_estado
37	Can add Animal	10	add_animal
38	Can change Animal	10	change_animal
39	Can delete Animal	10	delete_animal
40	Can view Animal	10	view_animal
41	Can add Vientre	11	add_vientre
42	Can change Vientre	11	change_vientre
43	Can delete Vientre	11	delete_vientre
44	Can view Vientre	11	view_vientre
45	Can add Palapcion	12	add_palpacion
46	Can change Palapcion	12	change_palpacion
47	Can delete Palapcion	12	delete_palpacion
48	Can view Palapcion	12	view_palpacion
49	Can add Perdida	13	add_perdida
50	Can change Perdida	13	change_perdida
51	Can delete Perdida	13	delete_perdida
52	Can view Perdida	13	view_perdida
53	Can add Nacimiento	14	add_nacimiento
54	Can change Nacimiento	14	change_nacimiento
55	Can delete Nacimiento	14	delete_nacimiento
56	Can view Nacimiento	14	view_nacimiento
57	Can add Fallecimiento	15	add_fallecimiento
58	Can change Fallecimiento	15	change_fallecimiento
59	Can delete Fallecimiento	15	delete_fallecimiento
60	Can view Fallecimiento	15	view_fallecimiento
61	Can add Marcado	16	add_marcado
62	Can change Marcado	16	change_marcado
63	Can delete Marcado	16	delete_marcado
64	Can view Marcado	16	view_marcado
65	Can add Medida	17	add_medida
66	Can change Medida	17	change_medida
67	Can delete Medida	17	delete_medida
68	Can view Medida	17	view_medida
69	Can add TipoTPrev	18	add_tipotprev
70	Can change TipoTPrev	18	change_tipotprev
71	Can delete TipoTPrev	18	delete_tipotprev
72	Can view TipoTPrev	18	view_tipotprev
73	Can add TCurativo	19	add_tcurativo
74	Can change TCurativo	19	change_tcurativo
75	Can delete TCurativo	19	delete_tcurativo
76	Can view TCurativo	19	view_tcurativo
77	Can add TPreventivo	20	add_tpreventivo
78	Can change TPreventivo	20	change_tpreventivo
79	Can delete TPreventivo	20	delete_tpreventivo
80	Can view TPreventivo	20	view_tpreventivo
81	Can add AplicacionTP	21	add_aplicaciontp
82	Can change AplicacionTP	21	change_aplicaciontp
83	Can delete AplicacionTP	21	delete_aplicaciontp
84	Can view AplicacionTP	21	view_aplicaciontp
85	Can add Comprador	22	add_comprador
86	Can change Comprador	22	change_comprador
87	Can delete Comprador	22	delete_comprador
88	Can view Comprador	22	view_comprador
89	Can add LoteVenta	23	add_loteventa
90	Can change LoteVenta	23	change_loteventa
91	Can delete LoteVenta	23	delete_loteventa
92	Can view LoteVenta	23	view_loteventa
93	Can add AnimalesLote	24	add_animaleslote
94	Can change AnimalesLote	24	change_animaleslote
95	Can delete AnimalesLote	24	delete_animaleslote
96	Can view AnimalesLote	24	view_animaleslote
97	Can add Articulo	25	add_articulo
98	Can change Articulo	25	change_articulo
99	Can delete Articulo	25	delete_articulo
100	Can view Articulo	25	view_articulo
101	Can add Compra	26	add_compra
102	Can change Compra	26	change_compra
103	Can delete Compra	26	delete_compra
104	Can view Compra	26	view_compra
105	Can add DetalleCompra	27	add_detallecompra
106	Can change DetalleCompra	27	change_detallecompra
107	Can delete DetalleCompra	27	delete_detallecompra
108	Can view DetalleCompra	27	view_detallecompra
\.


                                                                                                                                                                                                                                                             5174.dat                                                                                            0000600 0004000 0002000 00000000515 14665456514 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	pbkdf2_sha256$600000$WtdKyToEQyibvyw7TlTz3M$+WozXlKJOWM66QpJCfMi784jy21njlZC+6I8y87v+bk=	2023-12-27 19:25:09.2802-03	f	test				f	t	2023-12-27 18:05:02-03
1	pbkdf2_sha256$720000$FB59OwO0G0oOhAUpXEOF3n$6bbSATaCnvLpVm5jsZB0TATh0lzhmLQBVU4ziX0LoO0=	2024-09-02 16:43:00.076653-04	t	admin			test@mail.com	t	t	2023-11-16 21:47:11-03
\.


                                                                                                                                                                                   5175.dat                                                                                            0000600 0004000 0002000 00000000021 14665456514 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	1
2	2	3
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               5178.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5180.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5182.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5184.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5186.dat                                                                                            0000600 0004000 0002000 00000001216 14665456514 0014275 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2023-12-27 17:59:15.634895-03	1	Administrador	1	[{"added": {}}]	3	1
2	2023-12-27 18:00:07.48646-03	2	Cajero	1	[{"added": {}}]	3	1
3	2023-12-27 18:01:19.774039-03	3	Compras	1	[{"added": {}}]	3	1
4	2023-12-27 18:01:43.391406-03	3	Compra	2	[{"changed": {"fields": ["Name"]}}]	3	1
5	2023-12-27 18:02:50.80991-03	4	Anil	1	[{"added": {}}]	3	1
6	2023-12-27 18:03:08.73408-03	4	Animal	2	[{"changed": {"fields": ["Name"]}}]	3	1
7	2023-12-27 18:04:03.868271-03	1	admin	2	[{"changed": {"fields": ["Groups"]}}]	4	1
8	2023-12-27 18:05:02.855148-03	2	test	1	[{"added": {}}]	4	1
9	2023-12-27 18:05:32.955457-03	2	test	2	[{"changed": {"fields": ["Groups"]}}]	4	1
\.


                                                                                                                                                                                                                                                                                                                                                                                  5188.dat                                                                                            0000600 0004000 0002000 00000000715 14665456514 0014302 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	apl	raza
8	apl	grupodestino
9	apl	estado
10	apl	animal
11	apl	vientre
12	apl	palpacion
13	apl	perdida
14	apl	nacimiento
15	apl	fallecimiento
16	apl	marcado
17	apl	medida
18	apl	tipotprev
19	apl	tcurativo
20	apl	tpreventivo
21	apl	aplicaciontp
22	apl	comprador
23	apl	loteventa
24	apl	animaleslote
25	apl	articulo
26	apl	compra
27	apl	detallecompra
\.


                                                   5190.dat                                                                                            0000600 0004000 0002000 00000002476 14665456514 0014301 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	apl	0001_initial	2023-03-30 10:36:03.943281-04
2	apl	0002_auto_20230330_1029	2023-03-30 10:36:03.972665-04
3	contenttypes	0001_initial	2023-11-16 21:46:21.596759-03
4	auth	0001_initial	2023-11-16 21:46:21.780694-03
5	admin	0001_initial	2023-11-16 21:46:21.81734-03
6	admin	0002_logentry_remove_auto_add	2023-11-16 21:46:21.830885-03
7	admin	0003_logentry_add_action_flag_choices	2023-11-16 21:46:21.845496-03
8	contenttypes	0002_remove_content_type_name	2023-11-16 21:46:21.886081-03
9	auth	0002_alter_permission_name_max_length	2023-11-16 21:46:21.899084-03
10	auth	0003_alter_user_email_max_length	2023-11-16 21:46:21.911303-03
11	auth	0004_alter_user_username_opts	2023-11-16 21:46:21.922354-03
12	auth	0005_alter_user_last_login_null	2023-11-16 21:46:21.931334-03
13	auth	0006_require_contenttypes_0002	2023-11-16 21:46:21.936307-03
14	auth	0007_alter_validators_add_error_messages	2023-11-16 21:46:21.948159-03
15	auth	0008_alter_user_username_max_length	2023-11-16 21:46:21.971166-03
16	auth	0009_alter_user_last_name_max_length	2023-11-16 21:46:21.981859-03
17	auth	0010_alter_group_name_max_length	2023-11-16 21:46:21.993435-03
18	auth	0011_update_proxy_permissions	2023-11-16 21:46:22.020509-03
19	auth	0012_alter_user_first_name_max_length	2023-11-16 21:46:22.030067-03
20	sessions	0001_initial	2023-11-16 21:46:22.055939-03
\.


                                                                                                                                                                                                  5192.dat                                                                                            0000600 0004000 0002000 00000011063 14665456514 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        vbynjr2ob4wt1bgb5o4q6usn93kq5u6q	.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1r3n19:rMIeh3Uc6S8JP_w8Lefy5UBv2pdPThQNwj9WWyWSIR4	2023-11-30 21:47:31.12745-03
pxrj1tt9z2egdxpe59jjy5n12rihdw8c	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rXrds:Wq5KhNI5at5ywMh1WjJ8pKNs4d8ER6IbfTG2Bv14_QM	2024-02-21 17:47:48.262478-03
mxzdqqgzrw4myb155p1a5z9j5hs50304	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rfr76:sAeReYfNA2hoSWGvIUyp3NmACuYsMQSmpfuD6csONjk	2024-03-14 18:51:00.5691-03
dyzda3sk6k0phm05do1cer8ypefme9ku	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rmjY8:gVLkofCdqKlj3KINKTYG7GECu2N-JaDr9D2rq-Bc5DQ	2024-04-02 17:11:20.420434-04
edka99tmhcoys7h5ov53s7vupsvkg868	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1ruLyM:SMp8LDUU-NczcW8bI6qh_4JuNLcQiygj9s9yUWqYffM	2024-04-23 16:37:54.762978-04
q9gqhl1wr6yyj24skoie73pygpcfkkai	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rznlV:tqRF-ufV7F5g74bWIqJ_-4hHdsEI7O1fQhY7ejBe6SA	2024-05-08 17:19:09.284863-04
gzh5x0uuuuhll43inhc8qfvdyo4klzqd	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1s7mVU:JeYjqYDeK2QnvzsL1Pc_Hlxc1Cami9nq0BIwXY0eaXk	2024-05-30 17:35:36.797046-04
7kv9kor9wxdfqsuga8hrbr6s9rnr9tib	.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rDCby:KKDOKjkdsLAqjPj1gtSytArWlKAKqQs9WO545KUBPPk	2023-12-26 20:56:26.784873-03
isegctudl6tplponfaniv946hpe68dpu	.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rFNQR:ltTTw7z84LUjXh9VAHGCEGWMslN5BRi6vTFH0gbHsvk	2024-01-01 20:53:31.444866-03
xdks2z4jxp0rwd14vumbp74lfa6r8dqd	.eJxVjMsOwiAUBf-FtSHQ0lJcuvcbyH1wpWogKe3K-O_apAvdnpk5LxVhW3PcWlrizOqsrDr9bgj0SGUHfIdyq5pqWZcZ9a7ogzZ9rZyel8P9O8jQ8rfGkTsrRmggCQ5FiJEcDCOjMRwsi--B3GQooeUUwuQZHXjf9c4Gb9X7AyaEOPE:1rFOFT:nCM4GG1KYv_NXN6d0eybRJiwgOqeV-hZLlLP-9CG9ms	2024-01-01 18:46:15.025613-03
v9xp4kmivkn5xyidlcu4yghu3urvqdip	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rIfOM:S-vlicueDUcYT5XteyAkXxqgestruSr8Ld04CUkbuUw	2024-01-10 19:40:58.68598-03
qo7r9bn889tz9wo9vhi9r4z3q54n6e3r	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rLB1f:b2CbvN-xq-GsQ3wh_INiov_2FVi-H0PKj9Qwys2HjPw	2024-01-17 17:51:55.652087-03
dtfxxjhdrlc4s5l67awuf4v5wuutun09	.eJxVjEEOwiAQRe_C2hCwlIJL956BzDCDVA0kpV0Z725JutDdz38v7y0CbGsOW-MlzCQuQovT74cQn1w6oAeUe5WxlnWZUXZFHrTJWyV-XQ_3L5Ch5Z4lcGwGJoue_RQnTYh2dGetkJJ3AyflnAHWfty1faCKyrACmwbjo_h8AfuHOGc:1rSQgE:-C9jxgjA1h53wWxFMwRy4-ptJxTz9guYYsfdC8PqGGo	2024-02-06 17:59:46.839471-03
th2wda31v5v8d1h0yp2c2e4xyk6xxqn4	.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1sYatr:2K2gOQ9PdlPlxFmZBRa_EoGAFdUFZTe2vzyD9jKrRKk	2024-08-12 16:39:35.728513-04
90s7eif416ots2tgndlwsdaiqzqcl9il	.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1sdfqf:wrAYYM2YkYV_gWOki_j9Xs8s1jHCeRgldNPPlL2JqCE	2024-08-26 16:57:17.515435-04
ydruvdnu5cf1d139x8apw30mukmbf5z4	.eJxVjMsOwiAQRf-FtSEw5VFcuvcbyMAMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJE4mz0OL0uyXMD247oDu22yzz3NZlSnJX5EG7vM7Ez8vh_h1U7PVbe22hsA9FjRwIk1fMHowLeQzOG9AaPGUIDqCgsqY4sgkHzsYRD2DE-wPTHjeE:1slHdM:xgNSTsileYperrsAK2RWZKwyDqSu-jqVzSAgl_rtkXw	2024-09-16 16:43:00.09198-04
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                             5193.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5195.dat                                                                                            0000600 0004000 0002000 00000000026 14665456514 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2023-04-18	1	2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          5196.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5198.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5200.dat                                                                                            0000600 0004000 0002000 00000000225 14665456514 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1000	155	2023-03-30	1
2	1300	165	2023-03-30	2
3	1000	178	2023-09-26	3
4	1005	158	2024-05-16	1
5	1005	158	2024-05-16	1
6	1006	158	2024-05-16	1
\.


                                                                                                                                                                                                                                                                                                                                                                           5202.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014255 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5204.dat                                                                                            0000600 0004000 0002000 00000000033 14665456514 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	V	asdf	2023-04-18	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     5206.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5208.dat                                                                                            0000600 0004000 0002000 00000000130 14665456514 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Brahman	2023-03-30
2	Nelore	2023-03-30
3	Angus	2023-03-30
4	NuevoEdit	2023-09-26
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                        5210.dat                                                                                            0000600 0004000 0002000 00000000005 14665456514 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           5212.dat                                                                                            0000600 0004000 0002000 00000000027 14665456514 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	test	2024-01-30
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         5214.dat                                                                                            0000600 0004000 0002000 00000000105 14665456514 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Antiparasitarios	2024-05-01	15	TEST	2024-05-17	2	2024-06-01	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000204776 14665456514 0015424 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3
-- Dumped by pg_dump version 16.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE proyecto;
--
-- Name: proyecto; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE proyecto WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Paraguay.1252';


ALTER DATABASE proyecto OWNER TO postgres;

\connect proyecto

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Estado" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public."Estado" OWNER TO postgres;

--
-- Name: Estado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Estado_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public."Estado_id_seq" OWNER TO postgres;

--
-- Name: Estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Estado_id_seq" OWNED BY public."Estado".id;


--
-- Name: GrupoDestino; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."GrupoDestino" (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public."GrupoDestino" OWNER TO postgres;

--
-- Name: GrupoDestino_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."GrupoDestino_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public."GrupoDestino_id_seq" OWNER TO postgres;

--
-- Name: GrupoDestino_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."GrupoDestino_id_seq" OWNED BY public."GrupoDestino".id;


--
-- Name: animal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.animal (
    id bigint NOT NULL,
    "id_Animal" character varying(50) NOT NULL,
    fecha_nacimiento date NOT NULL,
    sexo character varying(1) NOT NULL,
    estado_id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    grupo_destino_id bigint NOT NULL,
    raza_id bigint NOT NULL
);


ALTER TABLE public.animal OWNER TO postgres;

--
-- Name: animal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.animal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.animal_id_seq OWNER TO postgres;

--
-- Name: animal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.animal_id_seq OWNED BY public.animal.id;


--
-- Name: animales_lote; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.animales_lote (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Lote_id" bigint NOT NULL
);


ALTER TABLE public.animales_lote OWNER TO postgres;

--
-- Name: animales_lote_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.animales_lote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.animales_lote_id_seq OWNER TO postgres;

--
-- Name: animales_lote_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.animales_lote_id_seq OWNED BY public.animales_lote.id;


--
-- Name: aplicacion_tp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aplicacion_tp (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_TPreventivo_id" bigint NOT NULL
);


ALTER TABLE public.aplicacion_tp OWNER TO postgres;

--
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aplicacion_tp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.aplicacion_tp_id_seq OWNER TO postgres;

--
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aplicacion_tp_id_seq OWNED BY public.aplicacion_tp.id;


--
-- Name: articulo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.articulo (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    "cantidadActual" integer NOT NULL,
    "cantidadIdeal" integer NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public.articulo OWNER TO postgres;

--
-- Name: articulo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.articulo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.articulo_id_seq OWNER TO postgres;

--
-- Name: articulo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.articulo_id_seq OWNED BY public.articulo.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_group ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_group_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_permission ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_user_groups ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compra (
    id bigint NOT NULL,
    "fechaCompra" date NOT NULL,
    "totalCompra" integer NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public.compra OWNER TO postgres;

--
-- Name: compra_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.compra_id_seq OWNER TO postgres;

--
-- Name: compra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compra_id_seq OWNED BY public.compra.id;


--
-- Name: comprador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comprador (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    telefono character varying(100) NOT NULL,
    mail character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public.comprador OWNER TO postgres;

--
-- Name: comprador_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comprador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.comprador_id_seq OWNER TO postgres;

--
-- Name: comprador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comprador_id_seq OWNED BY public.comprador.id;


--
-- Name: detalle_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_compra (
    id bigint NOT NULL,
    cantidad integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Articulo_id" bigint NOT NULL,
    "id_Compra_id" bigint NOT NULL
);


ALTER TABLE public.detalle_compra OWNER TO postgres;

--
-- Name: detalle_compra_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.detalle_compra_id_seq OWNER TO postgres;

--
-- Name: detalle_compra_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_compra_id_seq OWNED BY public.detalle_compra.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.django_admin_log ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.django_content_type ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: fallecimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fallecimiento (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


ALTER TABLE public.fallecimiento OWNER TO postgres;

--
-- Name: fallecimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fallecimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.fallecimiento_id_seq OWNER TO postgres;

--
-- Name: fallecimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fallecimiento_id_seq OWNED BY public.fallecimiento.id;


--
-- Name: fecundacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fecundacion (
    id bigint NOT NULL,
    fecha date NOT NULL,
    id_madre_id bigint NOT NULL,
    id_padre_id bigint NOT NULL
);


ALTER TABLE public.fecundacion OWNER TO postgres;

--
-- Name: lote_venta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lote_venta (
    id bigint NOT NULL,
    estado character varying(1) NOT NULL,
    fecha_envio date NOT NULL,
    fecha_confirmado date NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Comprador_id" bigint NOT NULL
);


ALTER TABLE public.lote_venta OWNER TO postgres;

--
-- Name: lote_venta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lote_venta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.lote_venta_id_seq OWNER TO postgres;

--
-- Name: lote_venta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lote_venta_id_seq OWNED BY public.lote_venta.id;


--
-- Name: marcado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.marcado (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


ALTER TABLE public.marcado OWNER TO postgres;

--
-- Name: marcado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.marcado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.marcado_id_seq OWNER TO postgres;

--
-- Name: marcado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.marcado_id_seq OWNED BY public.marcado.id;


--
-- Name: medida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medida (
    id bigint NOT NULL,
    "pesoKG" integer NOT NULL,
    "alturaCM" integer NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


ALTER TABLE public.medida OWNER TO postgres;

--
-- Name: medida_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.medida_id_seq OWNER TO postgres;

--
-- Name: medida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.medida_id_seq OWNED BY public.medida.id;


--
-- Name: nacimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nacimiento (
    id bigint NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "id_Fecundacion_id" bigint NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE public.nacimiento OWNER TO postgres;

--
-- Name: nacimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nacimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nacimiento_id_seq OWNER TO postgres;

--
-- Name: nacimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nacimiento_id_seq OWNED BY public.nacimiento.id;


--
-- Name: palpacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.palpacion (
    id bigint NOT NULL,
    resultado character varying(1) NOT NULL,
    observacion character varying(100),
    fecha date NOT NULL,
    id_fecundacion_id bigint NOT NULL
);


ALTER TABLE public.palpacion OWNER TO postgres;

--
-- Name: palpacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.palpacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.palpacion_id_seq OWNER TO postgres;

--
-- Name: palpacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.palpacion_id_seq OWNED BY public.palpacion.id;


--
-- Name: perdida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.perdida (
    id bigint NOT NULL,
    causa character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Palpacion_id" bigint NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE public.perdida OWNER TO postgres;

--
-- Name: perdida_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.perdida_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.perdida_id_seq OWNER TO postgres;

--
-- Name: perdida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.perdida_id_seq OWNED BY public.perdida.id;


--
-- Name: raza; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.raza (
    id bigint NOT NULL,
    descripcion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public.raza OWNER TO postgres;

--
-- Name: raza_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.raza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.raza_id_seq OWNER TO postgres;

--
-- Name: raza_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.raza_id_seq OWNED BY public.raza.id;


--
-- Name: tcurativo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcurativo (
    id bigint NOT NULL,
    observacion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_Animal_id" bigint NOT NULL,
    "fechaInicio" date NOT NULL,
    periodicidad integer NOT NULL,
    "fechaFin" date NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE public.tcurativo OWNER TO postgres;

--
-- Name: tcurativo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcurativo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tcurativo_id_seq OWNER TO postgres;

--
-- Name: tcurativo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcurativo_id_seq OWNED BY public.tcurativo.id;


--
-- Name: tipo_tprev; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_tprev (
    id bigint NOT NULL,
    "nombreTipo" character varying(100) NOT NULL,
    fecha_creacion date NOT NULL
);


ALTER TABLE public.tipo_tprev OWNER TO postgres;

--
-- Name: tipo_tprev_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_tprev_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tipo_tprev_id_seq OWNER TO postgres;

--
-- Name: tipo_tprev_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_tprev_id_seq OWNED BY public.tipo_tprev.id;


--
-- Name: tpreventivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tpreventivo (
    id bigint NOT NULL,
    nombre character varying(100) NOT NULL,
    "fechaInicio" date NOT NULL,
    periodicidad integer NOT NULL,
    observacion character varying(100) NOT NULL,
    fecha_creacion date NOT NULL,
    "id_TipoTPrev_id" bigint NOT NULL,
    "fechaFin" date NOT NULL,
    "id_Animal_id" bigint NOT NULL
);


ALTER TABLE public.tpreventivo OWNER TO postgres;

--
-- Name: tpreventivo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tpreventivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tpreventivo_id_seq OWNER TO postgres;

--
-- Name: tpreventivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tpreventivo_id_seq OWNED BY public.tpreventivo.id;


--
-- Name: vientre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vientre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.vientre_id_seq OWNER TO postgres;

--
-- Name: vientre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vientre_id_seq OWNED BY public.fecundacion.id;


--
-- Name: Estado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Estado" ALTER COLUMN id SET DEFAULT nextval('public."Estado_id_seq"'::regclass);


--
-- Name: GrupoDestino id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."GrupoDestino" ALTER COLUMN id SET DEFAULT nextval('public."GrupoDestino_id_seq"'::regclass);


--
-- Name: animal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal ALTER COLUMN id SET DEFAULT nextval('public.animal_id_seq'::regclass);


--
-- Name: animales_lote id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animales_lote ALTER COLUMN id SET DEFAULT nextval('public.animales_lote_id_seq'::regclass);


--
-- Name: aplicacion_tp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aplicacion_tp ALTER COLUMN id SET DEFAULT nextval('public.aplicacion_tp_id_seq'::regclass);


--
-- Name: articulo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articulo ALTER COLUMN id SET DEFAULT nextval('public.articulo_id_seq'::regclass);


--
-- Name: compra id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compra ALTER COLUMN id SET DEFAULT nextval('public.compra_id_seq'::regclass);


--
-- Name: comprador id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comprador ALTER COLUMN id SET DEFAULT nextval('public.comprador_id_seq'::regclass);


--
-- Name: detalle_compra id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra ALTER COLUMN id SET DEFAULT nextval('public.detalle_compra_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: fallecimiento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fallecimiento ALTER COLUMN id SET DEFAULT nextval('public.fallecimiento_id_seq'::regclass);


--
-- Name: fecundacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fecundacion ALTER COLUMN id SET DEFAULT nextval('public.vientre_id_seq'::regclass);


--
-- Name: lote_venta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lote_venta ALTER COLUMN id SET DEFAULT nextval('public.lote_venta_id_seq'::regclass);


--
-- Name: marcado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marcado ALTER COLUMN id SET DEFAULT nextval('public.marcado_id_seq'::regclass);


--
-- Name: medida id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medida ALTER COLUMN id SET DEFAULT nextval('public.medida_id_seq'::regclass);


--
-- Name: nacimiento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacimiento ALTER COLUMN id SET DEFAULT nextval('public.nacimiento_id_seq'::regclass);


--
-- Name: palpacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.palpacion ALTER COLUMN id SET DEFAULT nextval('public.palpacion_id_seq'::regclass);


--
-- Name: perdida id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perdida ALTER COLUMN id SET DEFAULT nextval('public.perdida_id_seq'::regclass);


--
-- Name: raza id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.raza ALTER COLUMN id SET DEFAULT nextval('public.raza_id_seq'::regclass);


--
-- Name: tcurativo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcurativo ALTER COLUMN id SET DEFAULT nextval('public.tcurativo_id_seq'::regclass);


--
-- Name: tipo_tprev id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_tprev ALTER COLUMN id SET DEFAULT nextval('public.tipo_tprev_id_seq'::regclass);


--
-- Name: tpreventivo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tpreventivo ALTER COLUMN id SET DEFAULT nextval('public.tpreventivo_id_seq'::regclass);


--
-- Data for Name: Estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Estado" (id, descripcion, fecha_creacion) FROM stdin;
\.
COPY public."Estado" (id, descripcion, fecha_creacion) FROM '$$PATH$$/5156.dat';

--
-- Data for Name: GrupoDestino; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."GrupoDestino" (id, descripcion, fecha_creacion) FROM stdin;
\.
COPY public."GrupoDestino" (id, descripcion, fecha_creacion) FROM '$$PATH$$/5158.dat';

--
-- Data for Name: animal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.animal (id, "id_Animal", fecha_nacimiento, sexo, estado_id, fecha_creacion, grupo_destino_id, raza_id) FROM stdin;
\.
COPY public.animal (id, "id_Animal", fecha_nacimiento, sexo, estado_id, fecha_creacion, grupo_destino_id, raza_id) FROM '$$PATH$$/5160.dat';

--
-- Data for Name: animales_lote; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.animales_lote (id, fecha_creacion, "id_Animal_id", "id_Lote_id") FROM stdin;
\.
COPY public.animales_lote (id, fecha_creacion, "id_Animal_id", "id_Lote_id") FROM '$$PATH$$/5162.dat';

--
-- Data for Name: aplicacion_tp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aplicacion_tp (id, fecha_creacion, "id_Animal_id", "id_TPreventivo_id") FROM stdin;
\.
COPY public.aplicacion_tp (id, fecha_creacion, "id_Animal_id", "id_TPreventivo_id") FROM '$$PATH$$/5164.dat';

--
-- Data for Name: articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.articulo (id, nombre, "cantidadActual", "cantidadIdeal", fecha_creacion) FROM stdin;
\.
COPY public.articulo (id, nombre, "cantidadActual", "cantidadIdeal", fecha_creacion) FROM '$$PATH$$/5166.dat';

--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.
COPY public.auth_group (id, name) FROM '$$PATH$$/5168.dat';

--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.
COPY public.auth_group_permissions (id, group_id, permission_id) FROM '$$PATH$$/5170.dat';

--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
\.
COPY public.auth_permission (id, name, content_type_id, codename) FROM '$$PATH$$/5172.dat';

--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.
COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM '$$PATH$$/5174.dat';

--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.
COPY public.auth_user_groups (id, user_id, group_id) FROM '$$PATH$$/5175.dat';

--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.
COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM '$$PATH$$/5178.dat';

--
-- Data for Name: compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.compra (id, "fechaCompra", "totalCompra", fecha_creacion) FROM stdin;
\.
COPY public.compra (id, "fechaCompra", "totalCompra", fecha_creacion) FROM '$$PATH$$/5180.dat';

--
-- Data for Name: comprador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comprador (id, nombre, telefono, mail, fecha_creacion) FROM stdin;
\.
COPY public.comprador (id, nombre, telefono, mail, fecha_creacion) FROM '$$PATH$$/5182.dat';

--
-- Data for Name: detalle_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_compra (id, cantidad, fecha_creacion, "id_Articulo_id", "id_Compra_id") FROM stdin;
\.
COPY public.detalle_compra (id, cantidad, fecha_creacion, "id_Articulo_id", "id_Compra_id") FROM '$$PATH$$/5184.dat';

--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.
COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM '$$PATH$$/5186.dat';

--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
\.
COPY public.django_content_type (id, app_label, model) FROM '$$PATH$$/5188.dat';

--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
\.
COPY public.django_migrations (id, app, name, applied) FROM '$$PATH$$/5190.dat';

--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.
COPY public.django_session (session_key, session_data, expire_date) FROM '$$PATH$$/5192.dat';

--
-- Data for Name: fallecimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fallecimiento (id, causa, fecha_creacion, "id_Animal_id") FROM stdin;
\.
COPY public.fallecimiento (id, causa, fecha_creacion, "id_Animal_id") FROM '$$PATH$$/5193.dat';

--
-- Data for Name: fecundacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fecundacion (id, fecha, id_madre_id, id_padre_id) FROM stdin;
\.
COPY public.fecundacion (id, fecha, id_madre_id, id_padre_id) FROM '$$PATH$$/5195.dat';

--
-- Data for Name: lote_venta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lote_venta (id, estado, fecha_envio, fecha_confirmado, fecha_creacion, "id_Comprador_id") FROM stdin;
\.
COPY public.lote_venta (id, estado, fecha_envio, fecha_confirmado, fecha_creacion, "id_Comprador_id") FROM '$$PATH$$/5196.dat';

--
-- Data for Name: marcado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.marcado (id, fecha_creacion, "id_Animal_id") FROM stdin;
\.
COPY public.marcado (id, fecha_creacion, "id_Animal_id") FROM '$$PATH$$/5198.dat';

--
-- Data for Name: medida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.medida (id, "pesoKG", "alturaCM", fecha_creacion, "id_Animal_id") FROM stdin;
\.
COPY public.medida (id, "pesoKG", "alturaCM", fecha_creacion, "id_Animal_id") FROM '$$PATH$$/5200.dat';

--
-- Data for Name: nacimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nacimiento (id, fecha_creacion, "id_Animal_id", "id_Fecundacion_id", fecha) FROM stdin;
\.
COPY public.nacimiento (id, fecha_creacion, "id_Animal_id", "id_Fecundacion_id", fecha) FROM '$$PATH$$/5202.dat';

--
-- Data for Name: palpacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.palpacion (id, resultado, observacion, fecha, id_fecundacion_id) FROM stdin;
\.
COPY public.palpacion (id, resultado, observacion, fecha, id_fecundacion_id) FROM '$$PATH$$/5204.dat';

--
-- Data for Name: perdida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.perdida (id, causa, fecha_creacion, "id_Palpacion_id", fecha) FROM stdin;
\.
COPY public.perdida (id, causa, fecha_creacion, "id_Palpacion_id", fecha) FROM '$$PATH$$/5206.dat';

--
-- Data for Name: raza; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.raza (id, descripcion, fecha_creacion) FROM stdin;
\.
COPY public.raza (id, descripcion, fecha_creacion) FROM '$$PATH$$/5208.dat';

--
-- Data for Name: tcurativo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcurativo (id, observacion, fecha_creacion, "id_Animal_id", "fechaInicio", periodicidad, "fechaFin", nombre) FROM stdin;
\.
COPY public.tcurativo (id, observacion, fecha_creacion, "id_Animal_id", "fechaInicio", periodicidad, "fechaFin", nombre) FROM '$$PATH$$/5210.dat';

--
-- Data for Name: tipo_tprev; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_tprev (id, "nombreTipo", fecha_creacion) FROM stdin;
\.
COPY public.tipo_tprev (id, "nombreTipo", fecha_creacion) FROM '$$PATH$$/5212.dat';

--
-- Data for Name: tpreventivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tpreventivo (id, nombre, "fechaInicio", periodicidad, observacion, fecha_creacion, "id_TipoTPrev_id", "fechaFin", "id_Animal_id") FROM stdin;
\.
COPY public.tpreventivo (id, nombre, "fechaInicio", periodicidad, observacion, fecha_creacion, "id_TipoTPrev_id", "fechaFin", "id_Animal_id") FROM '$$PATH$$/5214.dat';

--
-- Name: Estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Estado_id_seq"', 5, true);


--
-- Name: GrupoDestino_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."GrupoDestino_id_seq"', 2, true);


--
-- Name: animal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.animal_id_seq', 4, true);


--
-- Name: animales_lote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.animales_lote_id_seq', 1, false);


--
-- Name: aplicacion_tp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aplicacion_tp_id_seq', 1, false);


--
-- Name: articulo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.articulo_id_seq', 1, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 192, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 108, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 2, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: compra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compra_id_seq', 1, false);


--
-- Name: comprador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comprador_id_seq', 1, false);


--
-- Name: detalle_compra_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_compra_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 9, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 27, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 20, true);


--
-- Name: fallecimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fallecimiento_id_seq', 1, false);


--
-- Name: lote_venta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lote_venta_id_seq', 1, false);


--
-- Name: marcado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.marcado_id_seq', 1, false);


--
-- Name: medida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medida_id_seq', 6, true);


--
-- Name: nacimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nacimiento_id_seq', 1, false);


--
-- Name: palpacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.palpacion_id_seq', 1, true);


--
-- Name: perdida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.perdida_id_seq', 1, false);


--
-- Name: raza_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.raza_id_seq', 4, true);


--
-- Name: tcurativo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcurativo_id_seq', 1, false);


--
-- Name: tipo_tprev_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_tprev_id_seq', 2, true);


--
-- Name: tpreventivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tpreventivo_id_seq', 1, true);


--
-- Name: vientre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vientre_id_seq', 1, true);


--
-- Name: Estado Estado_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_descripcion_key" UNIQUE (descripcion);


--
-- Name: Estado Estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Estado"
    ADD CONSTRAINT "Estado_pkey" PRIMARY KEY (id);


--
-- Name: GrupoDestino GrupoDestino_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_descripcion_key" UNIQUE (descripcion);


--
-- Name: GrupoDestino GrupoDestino_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."GrupoDestino"
    ADD CONSTRAINT "GrupoDestino_pkey" PRIMARY KEY (id);


--
-- Name: animal animal_id_Animal_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_id_Animal_key" UNIQUE ("id_Animal");


--
-- Name: animal animal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_pkey PRIMARY KEY (id);


--
-- Name: animales_lote animales_lote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT animales_lote_pkey PRIMARY KEY (id);


--
-- Name: aplicacion_tp aplicacion_tp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT aplicacion_tp_pkey PRIMARY KEY (id);


--
-- Name: articulo articulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articulo
    ADD CONSTRAINT articulo_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: compra compra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compra
    ADD CONSTRAINT compra_pkey PRIMARY KEY (id);


--
-- Name: comprador comprador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comprador
    ADD CONSTRAINT comprador_pkey PRIMARY KEY (id);


--
-- Name: detalle_compra detalle_compra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT detalle_compra_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: fallecimiento fallecimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT fallecimiento_pkey PRIMARY KEY (id);


--
-- Name: lote_venta lote_venta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT lote_venta_pkey PRIMARY KEY (id);


--
-- Name: marcado marcado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT marcado_pkey PRIMARY KEY (id);


--
-- Name: medida medida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medida
    ADD CONSTRAINT medida_pkey PRIMARY KEY (id);


--
-- Name: nacimiento nacimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT nacimiento_pkey PRIMARY KEY (id);


--
-- Name: palpacion palpacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_pkey PRIMARY KEY (id);


--
-- Name: perdida perdida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT perdida_pkey PRIMARY KEY (id);


--
-- Name: raza raza_descripcion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_descripcion_key UNIQUE (descripcion);


--
-- Name: raza raza_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.raza
    ADD CONSTRAINT raza_pkey PRIMARY KEY (id);


--
-- Name: tcurativo tcurativo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT tcurativo_pkey PRIMARY KEY (id);


--
-- Name: tipo_tprev tipo_tprev_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_tprev
    ADD CONSTRAINT tipo_tprev_pkey PRIMARY KEY (id);


--
-- Name: tpreventivo tpreventivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_pkey PRIMARY KEY (id);


--
-- Name: fecundacion vientre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_pkey PRIMARY KEY (id);


--
-- Name: Estado_descripcion_b8dc8091_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Estado_descripcion_b8dc8091_like" ON public."Estado" USING btree (descripcion varchar_pattern_ops);


--
-- Name: GrupoDestino_descripcion_2c045343_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "GrupoDestino_descripcion_2c045343_like" ON public."GrupoDestino" USING btree (descripcion varchar_pattern_ops);


--
-- Name: animal_estado_id_944692c8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX animal_estado_id_944692c8 ON public.animal USING btree (estado_id);


--
-- Name: animal_grupo_destino_id_b68236c3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX animal_grupo_destino_id_b68236c3 ON public.animal USING btree (grupo_destino_id);


--
-- Name: animal_id_Animal_38e077e5_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "animal_id_Animal_38e077e5_like" ON public.animal USING btree ("id_Animal" varchar_pattern_ops);


--
-- Name: animal_raza_id_01c071b4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX animal_raza_id_01c071b4 ON public.animal USING btree (raza_id);


--
-- Name: animales_lote_id_Animal_id_6feaf46b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "animales_lote_id_Animal_id_6feaf46b" ON public.animales_lote USING btree ("id_Animal_id");


--
-- Name: animales_lote_id_Lote_id_31dec68e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "animales_lote_id_Lote_id_31dec68e" ON public.animales_lote USING btree ("id_Lote_id");


--
-- Name: aplicacion_tp_id_Animal_id_b8e2b1fd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "aplicacion_tp_id_Animal_id_b8e2b1fd" ON public.aplicacion_tp USING btree ("id_Animal_id");


--
-- Name: aplicacion_tp_id_TPreventivo_id_26ea59df; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "aplicacion_tp_id_TPreventivo_id_26ea59df" ON public.aplicacion_tp USING btree ("id_TPreventivo_id");


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: detalle_compra_id_Articulo_id_ba37a9fa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "detalle_compra_id_Articulo_id_ba37a9fa" ON public.detalle_compra USING btree ("id_Articulo_id");


--
-- Name: detalle_compra_id_Compra_id_43b8a91e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "detalle_compra_id_Compra_id_43b8a91e" ON public.detalle_compra USING btree ("id_Compra_id");


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: fallecimiento_id_Animal_id_29c69888; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "fallecimiento_id_Animal_id_29c69888" ON public.fallecimiento USING btree ("id_Animal_id");


--
-- Name: lote_venta_id_Comprador_id_3c61c9be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "lote_venta_id_Comprador_id_3c61c9be" ON public.lote_venta USING btree ("id_Comprador_id");


--
-- Name: marcado_id_Animal_id_3776f20e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "marcado_id_Animal_id_3776f20e" ON public.marcado USING btree ("id_Animal_id");


--
-- Name: medida_id_Animal_id_0432812b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "medida_id_Animal_id_0432812b" ON public.medida USING btree ("id_Animal_id");


--
-- Name: nacimiento_id_Animal_id_de50946e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "nacimiento_id_Animal_id_de50946e" ON public.nacimiento USING btree ("id_Animal_id");


--
-- Name: nacimiento_id_Vientre_id_66d6bd7f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "nacimiento_id_Vientre_id_66d6bd7f" ON public.nacimiento USING btree ("id_Fecundacion_id");


--
-- Name: palpacion_id_vientre_id_6762db22; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX palpacion_id_vientre_id_6762db22 ON public.palpacion USING btree (id_fecundacion_id);


--
-- Name: perdida_id_Palpacion_id_7f0a21a1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "perdida_id_Palpacion_id_7f0a21a1" ON public.perdida USING btree ("id_Palpacion_id");


--
-- Name: raza_descripcion_320960d6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX raza_descripcion_320960d6_like ON public.raza USING btree (descripcion varchar_pattern_ops);


--
-- Name: tcurativo_id_Animal_id_8970efeb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "tcurativo_id_Animal_id_8970efeb" ON public.tcurativo USING btree ("id_Animal_id");


--
-- Name: tpreventivo_id_TipoTPrev_id_9d33de50; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "tpreventivo_id_TipoTPrev_id_9d33de50" ON public.tpreventivo USING btree ("id_TipoTPrev_id");


--
-- Name: vientre_id_madre_id_b449b968; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX vientre_id_madre_id_b449b968 ON public.fecundacion USING btree (id_madre_id);


--
-- Name: vientre_id_padre_id_d133132e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX vientre_id_padre_id_d133132e ON public.fecundacion USING btree (id_padre_id);


--
-- Name: animal animal_estado_id_944692c8_fk_Estado_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_estado_id_944692c8_fk_Estado_id" FOREIGN KEY (estado_id) REFERENCES public."Estado"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: animal animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT "animal_grupo_destino_id_b68236c3_fk_GrupoDestino_id" FOREIGN KEY (grupo_destino_id) REFERENCES public."GrupoDestino"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: animal animal_raza_id_01c071b4_fk_raza_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animal
    ADD CONSTRAINT animal_raza_id_01c071b4_fk_raza_id FOREIGN KEY (raza_id) REFERENCES public.raza(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: animales_lote animales_lote_id_Animal_id_6feaf46b_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Animal_id_6feaf46b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: animales_lote animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.animales_lote
    ADD CONSTRAINT "animales_lote_id_Lote_id_31dec68e_fk_lote_venta_id" FOREIGN KEY ("id_Lote_id") REFERENCES public.lote_venta(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aplicacion_tp aplicacion_tp_id_Animal_id_b8e2b1fd_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT "aplicacion_tp_id_Animal_id_b8e2b1fd_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aplicacion_tp aplicacion_tp_id_TPreventivo_id_26ea59df_fk_tpreventivo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aplicacion_tp
    ADD CONSTRAINT "aplicacion_tp_id_TPreventivo_id_26ea59df_fk_tpreventivo_id" FOREIGN KEY ("id_TPreventivo_id") REFERENCES public.tpreventivo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: detalle_compra detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Articulo_id_ba37a9fa_fk_articulo_id" FOREIGN KEY ("id_Articulo_id") REFERENCES public.articulo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: detalle_compra detalle_compra_id_Compra_id_43b8a91e_fk_compra_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT "detalle_compra_id_Compra_id_43b8a91e_fk_compra_id" FOREIGN KEY ("id_Compra_id") REFERENCES public.compra(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fallecimiento fallecimiento_id_Animal_id_29c69888_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fallecimiento
    ADD CONSTRAINT "fallecimiento_id_Animal_id_29c69888_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lote_venta lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lote_venta
    ADD CONSTRAINT "lote_venta_id_Comprador_id_3c61c9be_fk_comprador_id" FOREIGN KEY ("id_Comprador_id") REFERENCES public.comprador(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: marcado marcado_id_Animal_id_3776f20e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marcado
    ADD CONSTRAINT "marcado_id_Animal_id_3776f20e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: medida medida_id_Animal_id_0432812b_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medida
    ADD CONSTRAINT "medida_id_Animal_id_0432812b_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nacimiento nacimiento_id_Animal_id_de50946e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Animal_id_de50946e_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nacimiento nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacimiento
    ADD CONSTRAINT "nacimiento_id_Vientre_id_66d6bd7f_fk_vientre_id" FOREIGN KEY ("id_Fecundacion_id") REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: palpacion palpacion_id_vientre_id_6762db22_fk_vientre_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.palpacion
    ADD CONSTRAINT palpacion_id_vientre_id_6762db22_fk_vientre_id FOREIGN KEY (id_fecundacion_id) REFERENCES public.fecundacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: perdida perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perdida
    ADD CONSTRAINT "perdida_id_Palpacion_id_7f0a21a1_fk_palpacion_id" FOREIGN KEY ("id_Palpacion_id") REFERENCES public.palpacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tcurativo tcurativo_id_Animal_id_8970efeb_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcurativo
    ADD CONSTRAINT "tcurativo_id_Animal_id_8970efeb_fk_animal_id" FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tpreventivo tpreventivo_animal_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT tpreventivo_animal_fk FOREIGN KEY ("id_Animal_id") REFERENCES public.animal(id);


--
-- Name: tpreventivo tpreventivo_id_TipoTPrev_id_9d33de50_fk_tipo_tprev_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tpreventivo
    ADD CONSTRAINT "tpreventivo_id_TipoTPrev_id_9d33de50_fk_tipo_tprev_id" FOREIGN KEY ("id_TipoTPrev_id") REFERENCES public.tipo_tprev(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fecundacion vientre_id_madre_id_b449b968_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_madre_id_b449b968_fk_animal_id FOREIGN KEY (id_madre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fecundacion vientre_id_padre_id_d133132e_fk_animal_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fecundacion
    ADD CONSTRAINT vientre_id_padre_id_d133132e_fk_animal_id FOREIGN KEY (id_padre_id) REFERENCES public.animal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  