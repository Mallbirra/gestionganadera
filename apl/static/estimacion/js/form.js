//$(function () {
    var formData = new FormData();
    let hoy = new Date().toLocaleDateString().split("/");
    document.getElementById("id_fechaEstimacion").value = `${hoy[2]}-${hoy[1].padStart(2, "0")}-${hoy[0].padStart(2, "0")}`;
    document.getElementById("id_fechaEstimacion").max = `${hoy[2]}-${hoy[1].padStart(2, "0")}-${hoy[0].padStart(2, "0")}`;
    document.getElementById("btn_buscar_articulo").addEventListener("click", function() {
        buscar_articulos();
        document.getElementById("filtro").addEventListener("change", function() {
            buscar_articulos();
        });
        document.getElementById("registros").addEventListener("change", function() {
            buscar_articulos();
        });
        document.getElementById("ant").addEventListener("click", function() {
            anteriorBuscadorArticulo();
        });
        document.getElementById("sig").addEventListener("click", function() {
            siguienteBuscadorArticulo();
        });
    });
    if ((window.location.href).search("update") > 0) {
        document.getElementById("frm").dataset.id = window.location.href.split("/")[window.location.href.split("/").length-2];
        document.getElementById("divDetalles").hidden = false;
        document.getElementById("id_fechaEstimacion").setAttribute("readonly", true);
        document.getElementById("btnGuardarEstimacion").disabled = true;
        buscar_detalles();
        siguiente_elemento("cantidad", "btnGuardarDetalle");
    }
    function buscar_articulos() {
        document.getElementById("formulario").hidden = true;
        document.getElementById("buscador").hidden = false;
        document.querySelector("#buscador .card-title").innerHTML = "Buscador de Artículos";
        document.querySelector("#buscador thead").innerHTML = `<tr><th>ID</th><th>Nombre</th><th>Cant. Actual</th><th>Cant. Ideal</th></tr>`;
        formData = new FormData();
        formData.append('action', 'filterdataRM');
        formData.append('text', document.getElementById("filtro").value);
        formData.append('page', document.getElementById("actual").innerHTML);
        formData.append('rpp', document.getElementById("registros").value);
        fetch('/apl/articulo/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            let filas = "";
            if (data.length > 0) {
                data.forEach(articulo => {
                    filas += `<tr><td>${articulo.id}</td><td>${articulo.nombre}</td><td>${articulo.cantidadActual}</td><td>${articulo.cantidadIdeal}</td></tr>`;
                });
            }
            document.querySelector("#buscador tbody").innerHTML = filas;
            document.querySelectorAll("#buscador tbody tr").forEach(fila => {
                fila.addEventListener("click", function(e) {
                    seleccionar_articulo(e.target.closest("tr"));
                });
            });
            if ((data.length === 0) && (Number(document.getElementById("actual").innerHTML)>1)) {
                document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
                if (Number(document.getElementById("actual").innerHTML) === 1) {
                    document.getElementById("ant").parentElement.classList.add("disabled");
                }
                buscar_articulos();
            }
        });
    }
    function seleccionar_articulo(fila) {
        document.getElementById("articulo").value = `Nombre: ${fila.children[1].innerHTML}. Cant. Actual: ${fila.children[2].innerHTML}. Cant. Ideal: ${fila.children[3].innerHTML}.`;
        document.getElementById("articulo").dataset.id = fila.children[0].innerHTML;
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
        document.getElementById("cantidad").select();
    }
    function anteriorBuscadorArticulo() {
        let actual = Number(document.getElementById("actual").innerHTML);
        if (actual > 1) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
            document.getElementById("sig").parentElement.classList.remove("disabled");
            buscar_articulos();
        }
        if (actual === 2) {
            document.getElementById("ant").parentElement.classList.add("disabled");
        }
    }
    function siguienteBuscadorArticulo() {
        let rpp = Number(document.getElementById("registros").value);
        let filas = document.querySelectorAll("#buscador tbody tr").length;
        if (filas === rpp) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)+1;
            document.getElementById("ant").parentElement.classList.remove("disabled");
            buscar_articulos();
        }
    }
    document.getElementById("btn_cerrar_buscador").addEventListener("click", function() {
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    });
    function buscar_detalles() {
        var formData = new FormData();
        formData.append('action', 'searchbyestimacion');
        formData.append('estimacion', document.getElementById("frm").dataset.id);
        $("#tbodyDetalles").find("tr:gt(0)").remove();
        fetch('/apl/estimacion_detalle/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyDetalles").append(`<tr><td class="text-right">${element.id}</td><td data-id="${element.id_articulo}">Nombre: ${element.nombre_articulo}. Cant. Actual: ${element.cantidad_actual_articulo}. Cant. Ideal: ${element.cantidad_ideal_articulo}</td><td>${element.cantidad}</td><td>${element.comprado}</td><td></td></tr>`);
                });
            }
        });
    }
    document.getElementById("btnGuardarDetalle").addEventListener("click", 
        function(e) {
            let formData = new FormData();
            formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
            formData.append("action", "add");
            formData.append("id_Estimacion", document.getElementById("frm").dataset.id);
            formData.append("id_Articulo", document.getElementById("articulo").dataset.id);
            formData.append("cantidad", document.getElementById("cantidad").value);
            fetch('/apl/estimacion_detalle/add/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
                buscar_detalles();
            });
        });
//});