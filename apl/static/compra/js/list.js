$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_compra"},
            {"data": "id_vendedor"},
            {"data": "nombre_vendedor"},
            {"data": "id_estimacion"},
            {"data": "fecha_estimacion"},
            {"data": "estado"},
            {"data": "fecha_confirmado"},
            {"data": "fecha_recibido"},
            {"data": "desc"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/apl/compra/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat"><i class="fas fa-edit"></i></a> ';
                    if (row.estado === "P") {
                        buttons += '<button onclick="confirmar_compra(' + row.id + ')" type="button" class="btn btn-success btn-xs btn-flat"><i class="fas fa-check"></i></button> ';
                    } else if (row.estado === "C") {
                        buttons += '<button onclick="recibir_compra(' + row.id + ')" type="button" class="btn btn-success btn-xs btn-flat"><i class="fas fa-cubes"></i></button> ';
                    }
                    buttons += '<a href="/apl/compra/delete/' + row.id + '/" type="button" class="btn btn-danger btn-xs btn-flat"><i class="fas fa-trash-alt"></i></a>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
function confirmar_compra(id) {
    let formData = new FormData();
    formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
    formData.append('action', 'confirmar');
    formData.append('id', id);
    fetch('/apl/compra/update/'+id+'/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        location.reload();
    });
}
function recibir_compra(id) {
    let formData = new FormData();
    formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
    formData.append('action', 'recibir');
    formData.append('id', id);
    fetch('/apl/compra/update/'+id+'/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        location.reload();
    });
}