if (window.location.href.includes("update")) {
    document.getElementById("frm").dataset.id = window.location.href.split("/")[window.location.href.split("/").length-2];
    document.getElementById("id_vendedor").disabled = true;
    document.getElementById("id_estimacion").disabled = true;
    document.getElementById("btnGuardarCompra").disabled = true;
    buscar_compra_detalles();
    actualizar_articulos_estimacion();
}
document.getElementById("id_estimacion").addEventListener("change", function(e) {
    if ((document.getElementById("id_estimacion").value !== "") && (document.getElementById("frm").dataset.id !== undefined)) {
        actualizar_articulos_estimacion();
    }
});
if ((document.getElementById("id_estimacion").value !== "") && (document.getElementById("frm").dataset.id !== undefined)) {
    actualizar_articulos_estimacion();
}
function actualizar_articulos_estimacion() {
    formData = new FormData();
    formData.append('action', 'searchbyestimacion');
    formData.append('estimacion', document.getElementById("id_estimacion").value);
    fetch('/apl/estimacion_detalle/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        document.getElementById("cantidad").value = '';
        let opciones = "<option value='' selected>----------</option>";
        if (data.length > 0) {
            data.forEach(detalle_estimacion => {
                opciones += `<option value='${detalle_estimacion.id_detalle_estimacion}' data-articulo='${detalle_estimacion.id_articulo}' data-cantidad='${detalle_estimacion.cantidad}' data-comprado='${detalle_estimacion.comprado}'>${detalle_estimacion.nombre_articulo} | Cantidad: ${detalle_estimacion.cantidad} | Comprado: ${detalle_estimacion.comprado}</option>`;
            });
        }
        document.getElementById("articulo").innerHTML = opciones;
        document.getElementById("articulo").addEventListener("change", function(e) {
            document.getElementById("cantidad").max = Number(e.target.selectedOptions[0].dataset.cantidad) - Number(e.target.selectedOptions[0].dataset.comprado);
            if ((Number(document.getElementById("cantidad").value) <= (Number(document.getElementById("articulo").selectedOptions[0].dataset.cantidad) - Number(document.getElementById("articulo").selectedOptions[0].dataset.comprado))) && (Number(document.getElementById("cantidad").value) > 0)) {
                document.getElementById("btnGuardarDetalle").disabled = false;
            } else {
                document.getElementById("btnGuardarDetalle").disabled = true;
            }
        });
        document.getElementById("cantidad").addEventListener("change", function(e) {
            setTimeout(() => {
                if ((Number(document.getElementById("cantidad").value) <= (Number(document.getElementById("articulo").selectedOptions[0].dataset.cantidad) - Number(document.getElementById("articulo").selectedOptions[0].dataset.comprado))) && (Number(document.getElementById("cantidad").value) > 0)) {
                    document.getElementById("btnGuardarDetalle").disabled = false;
                } else {
                    document.getElementById("btnGuardarDetalle").disabled = true;
                }
            }, 100);
        });
    });
}
document.getElementById("btnGuardarDetalle").addEventListener("click", 
function(e) {
    let formData = new FormData();
    formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
    formData.append("action", "add");
    formData.append("id_Compra", document.getElementById("frm").dataset.id);
    formData.append("id_Articulo", $("#articulo>option:selected")[0].dataset.articulo);
    formData.append("cantidad", document.getElementById("cantidad").value);
    fetch('/apl/compra_detalle/add/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
        actualizar_articulos_estimacion();
        buscar_compra_detalles();
    });
});
function buscar_compra_detalles() {
    var formData = new FormData();
    formData.append('action', 'searchbycompra');
    formData.append('compra', document.getElementById("frm").dataset.id);
    $("#tbodyDetalles").find("tr:gt(0)").remove();
    fetch('/apl/compra_detalle/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json();
    }).then(function(data) {
        if (data.length > 0) {
            data.forEach(element => {
                $("#tbodyDetalles").append(`<tr><td class="text-right">${element.id}</td><td>${element.nombre_articulo}</td><td>${element.cantidad}</td><td></td></tr>`);
            });
        }
    });
}