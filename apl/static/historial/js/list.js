$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "descripcion_raza"},
            {"data": "id_Animal"},
            {"data": "fecha_nacimiento"},
            {"data": "descripcion_grupo_destino"},
            {"data": "fecha_vendible"},
            {"data": "sexo",
                "render": function (data, type, row) {
                             return (data === "M") ? 'Masculino' : 'Femenino';}},
            {"render": function (data, type, row) {return ``;}},
            {"render": function (data, type, row) {return ``;}},
            {"data": "castrado",
                "render": function (data, type, row) {
                             return (data === true) ? 'Sí' : 'No';}
            },
            {"render": function (data, type, row) {
                            if ((row.sexo === "M")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)<=180)) {
                                return 'Ternero'
                            } else if ((row.sexo === "F")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)<=180)) {
                                return 'Ternera' 
                            } else if ((row.sexo === "M")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>180)&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)<=730)&&!row.castrado) {
                                return 'Torito'
                            } else if ((row.sexo === "M")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>180)&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)<=730)&&row.castrado) {
                                return 'Novillito'
                            } else if ((row.sexo === "F")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>180)&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)<=730)) {
                                return 'Vaquillona'
                            } else if ((row.sexo === "M")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>730)&&!row.castrado) {
                                return 'Toro'
                            } else if ((row.sexo === "M")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>730)&&row.castrado) {
                                return 'Novillo'
                            } else if ((row.sexo === "F")&&((Math.round(new Date()-new Date(row.fecha_nacimiento))/86400000)>730)) {
                                return 'Vaca'
                            }
            }},
            {"data": "descripcion_estado"},
            {"data": "desc"},

        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '';
                    if ((row.grupo_destino == 1) && (row.estado == 1) && (row.fecha_vendible != null)) {
                        buttons += '<a href="/apl/historial/update/' + row.id + '/?vendible=true" class="btn btn-warning btn-xs btn-flat" data-toggle="tooltip" title="Editar"><i class="fas fa-edit"></i></a> ';
                    } else if ((row.grupo_destino == 1) && (row.estado == 1) && (row.fecha_vendible == null)) {
                        buttons += '<a href="/apl/historial/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat" data-toggle="tooltip" title="Editar"><i class="fas fa-edit"></i></a> ';
                        buttons += '<button onclick="vendible(' + row.id + ')" class="btn btn-success btn-xs btn-flat" data-toggle="tooltip" title="Vendible"><i class="fas fa-check"></i></button> ';
                    } else {
                        buttons += '<a href="/apl/historial/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat" data-toggle="tooltip" title="Editar"><i class="fas fa-edit"></i></a> ';
                    }    
                    //buttons += '<a href="/apl/historial/delete/' + row.id + '/" type="button" class="btn btn-danger btn-xs btn-flat" data-toggle="tooltip" title="Dar de baja"><i class="fas fa-minus"></i></a>';
                    buttons += '<button onclick="imprimir_historial(' + row.id + ')" class="btn btn-success btn-xs btn-flat" data-toggle="tooltip" title="Imprimir"><i class="fas fa-print"></i></button>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {
            $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
        }
    });
    setTimeout(() => {
        (document.querySelectorAll("#data>tbody>tr")).forEach(fila => {
            var formData = new FormData();
            formData.append('action', 'searchbycria');
            formData.append('animal', fila.children[0].innerHTML);
            fetch('/apl/nacimiento/list/',{method: 'POST', body: formData}).then(function(response) {
                    return response.json()
                }).then(function(data) {
                    if (data.id) {
                        fila.children[7].innerHTML = `<div class="text-center"><div class="font-weight-bold">Padre</div><div><b>ID:</b> ${data.id_padre}. <b>Raza:</b> ${data.raza_padre}. <b>Nacimiento:</b> ${data.nacimiento_padre}.</div><div class="font-weight-bold">Madre</div><div><b>ID:</b> ${data.id_madre}. <b>Raza:</b> ${data.raza_madre}. <b>Nacimiento:</b> ${data.nacimiento_madre}.</div></div>`;
                    } else {
                        fila.children[7].innerHTML = ``;
                    }
            });
            formData = new FormData();
            formData.append('action', 'searchlastbyanimal');
            formData.append('animal', fila.children[0].innerHTML);
            fetch('/apl/medida/list/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                if (data.id) {
                    fila.children[8].innerHTML = `<div class="text-center"><b>Altura:</b> ${data.alturaCM}. <b>Peso:</b> ${data.pesoKG}.</div>`;
                } else {
                    fila.children[8].innerHTML = ``;
                    if (fila.querySelector(".fa-check")) {
                        fila.querySelector(".fa-check").parentElement.remove();
                    }
                }
            });
        });
    }, 500);
});

function vendible(id) {
    let formData = new FormData();
    formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
    formData.append("action", "vendible");
    formData.append("id", id);
    fetch('/apl/animal/update/'+id+'/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        location.reload();
    });
}