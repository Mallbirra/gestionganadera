//$(function () {
    if (window.location.href.includes('vendible=true')) {
        $("#id_raza").attr("disabled", true);
        $("#id_id_Animal").attr("disabled", true);
        $("#id_fecha_nacimiento").attr("disabled", true);
        $("#id_grupo_destino").attr("disabled", true);
        $("#id_sexo").attr("disabled", true);
        $("#btnGuardarAnimal").attr("disabled", true);
    }
    $('#id_estado').on('mousedown', function(e) {
        e.preventDefault();
        this.blur();
        window.focus();
    });
    if ($("#id_estado").val() === "") {
        $("#id_estado").val("1");
    } else if ($("#id_estado").val() != "1") {
        $("#id_raza").attr("disabled", true);
        $("#id_id_Animal").attr("disabled", true);
        $("#id_fecha_nacimiento").attr("disabled", true);
        $("#id_grupo_destino").attr("disabled", true);
        $("#id_sexo").attr("disabled", true);
        $("#id_castrado").attr("disabled", true);
        $("#btnGuardarAnimal").attr("disabled", true);
        $("#btnGuardarMedida").attr("disabled", true);
        $("#btnGuardarPreventivo").attr("disabled", true);
        $("#btnGuardarCurativo").attr("disabled", true);
        $("#btnGuardarFecundacion").attr("disabled", true);
        $("#btnGuardarPerdida").attr("disabled", true);
        $("#btnGuardarNacimiento").attr("disabled", true);
    }
    var formData = new FormData();
    let hoy = new Date().toLocaleDateString().split("/");
    document.getElementById("fecha_medida").value = `${hoy[2]}-${hoy[1].padStart(2, "0")}-${hoy[0].padStart(2, "0")}`;
    document.getElementById("fecha_medida").max = `${hoy[2]}-${hoy[1].padStart(2, "0")}-${hoy[0].padStart(2, "0")}`;
    document.getElementById("btn_buscar_padre").addEventListener("click", function() {
        buscar_padres();
        document.getElementById("filtro").addEventListener("change", function() {
            buscar_padres();
        });
        document.getElementById("registros").addEventListener("change", function() {
            buscar_padres();
        });
        document.getElementById("ant").addEventListener("click", function() {
            anteriorBuscadorPadre();
        });
        document.getElementById("sig").addEventListener("click", function() {
            siguienteBuscadorPadre();
        });
    });
    document.getElementById("btn_buscar_fecundacion").addEventListener("click", function() {
        buscar_fecundaciones();
        document.getElementById("filtro").addEventListener("change", function() {
            buscar_fecundaciones();
        });
        document.getElementById("registros").addEventListener("change", function() {
            buscar_fecundaciones();
        });
        document.getElementById("ant").addEventListener("click", function() {
            anteriorBuscadorFecundacion();
        });
        document.getElementById("sig").addEventListener("click", function() {
            siguienteBuscadorFecundacion();
        });
    });
    document.getElementById("btn_buscar_palpacion_perdida").addEventListener("click", function() {
        buscar_palpaciones_perdida();
        document.getElementById("filtro").addEventListener("change", function() {
            buscar_palpaciones_perdida();
        });
        document.getElementById("registros").addEventListener("change", function() {
            buscar_palpaciones_perdida();
        });
        document.getElementById("ant").addEventListener("click", function() {
            anteriorBuscadorPalpacionPerdida();
        });
        document.getElementById("sig").addEventListener("click", function() {
            siguienteBuscadorPalpacionPerdida();
        });
    });
    document.getElementById("btn_buscar_fecundacion_nacimiento").addEventListener("click", function() {
        buscar_fecundaciones_nacimiento();
        document.getElementById("filtro").addEventListener("change", function() {
            buscar_fecundaciones_nacimiento();
        });
        document.getElementById("registros").addEventListener("change", function() {
            buscar_fecundaciones_nacimiento();
        });
        document.getElementById("ant").addEventListener("click", function() {
            anteriorBuscadorFecundacionNacimiento();
        });
        document.getElementById("sig").addEventListener("click", function() {
            siguienteBuscadorFecundacionNacimiento();
        });
    });
    if ((location.href).includes("/update")) {
        buscar_medidas_animal();
        buscar_tratamiento_preventivo_animal();
        buscar_tratamiento_curativo_animal();
        buscar_fecundacion_animal();
        buscar_palpacion_animal();
        buscar_perdida_animal();
        buscar_nacimiento_animal();
        cargar_ascendencia();
        cargar_ultima_medida();
        asignar_categoria();
    }
    function cargar_ascendencia() {
        var formData = new FormData();
        formData.append('action', 'searchbycria');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        fetch('/apl/nacimiento/list/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                if (data.id) {
                    $("#divAscendencia").removeAttr("hidden");
                    document.getElementById("ascendencia").innerHTML = `<div class="text-center"><div class="font-weight-bold">Padre</div><div><b>ID:</b> ${data.id_padre}. <b>Raza:</b> ${data.raza_padre}. <b>Nacimiento:</b> ${data.nacimiento_padre}.</div><div class="font-weight-bold">Madre</div><div><b>ID:</b> ${data.id_madre}. <b>Raza:</b> ${data.raza_madre}. <b>Nacimiento:</b> ${data.nacimiento_madre}.</div></div>`;
                }
        });
    }
    function cargar_ultima_medida() {
        var formData = new FormData();
        formData.append('action', 'searchlastbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        fetch('/apl/medida/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data) {
                $("#medidasRecientes").removeAttr("hidden");
                $("#fecha_medida_reciente").html(moment(data.fecha_creacion).format("DD/MM/yyyy"));
                $("#peso_medida_reciente").html(data.pesoKG);
                $("#altura_medida_reciente").html(data.alturaCM);
            }
        });
    }
    function asignar_categoria() {
        $("#divCategoria").removeAttr("hidden");
        let sexo = $("#id_sexo").val();
        let fecha = $("#id_fecha_nacimiento").val().split("/")
        fecha = `${fecha[2]}-${fecha[1]}-${fecha[0]}`;
        let castrado = ($("id_castrado").val()=="True"?true:false);
        if ((sexo === "M")&&((Math.round(new Date()-new Date(fecha))/86400000)<=180)) {
            $("#categoria").html('Ternero');
        } else if ((sexo === "F")&&((Math.round(new Date()-new Date(fecha))/86400000)<=180)) {
            $("#categoria").html('Ternera'); 
        } else if ((sexo === "M")&&((Math.round(new Date()-new Date(fecha))/86400000)>180)&&((Math.round(new Date()-new Date(fecha))/86400000)<=730)&&!castrado) {
            $("#categoria").html('Torito');
        } else if ((sexo === "M")&&((Math.round(new Date()-new Date(fecha))/86400000)>180)&&((Math.round(new Date()-new Date(fecha))/86400000)<=730)&&castrado) {
            $("#categoria").html('Novillito');
        } else if ((sexo === "F")&&((Math.round(new Date()-new Date(fecha))/86400000)>180)&&((Math.round(new Date()-new Date(fecha))/86400000)<=730)) {
            $("#categoria").html('Vaquillona');
        } else if ((sexo === "M")&&((Math.round(new Date()-new Date(fecha))/86400000)>730)&&!castrado) {
            $("#categoria").html('Toro');
        } else if ((sexo === "M")&&((Math.round(new Date()-new Date(fecha))/86400000)>730)&&castrado) {
            $("#categoria").html('Novillo');
        } else if ((sexo === "F")&&((Math.round(new Date()-new Date(fecha))/86400000)>730)) {
            $("#categoria").html('Vaca');
        }        
    }
    function buscar_padres() {
        document.getElementById("formulario").hidden = true;
        document.getElementById("buscador").hidden = false;
        document.querySelector("#buscador .card-title").innerHTML = "Buscador de Animales Masculinos para Reproducción";
        document.querySelector("#buscador thead").innerHTML = `<tr><th>Nro</th><th>Raza</th><th>Código</th><th>Nacimiento</th></tr>`;
        formData = new FormData();
        formData.append('action', 'filterdataRM');
        formData.append('text', document.getElementById("filtro").value);
        formData.append('page', document.getElementById("actual").innerHTML);
        formData.append('rpp', document.getElementById("registros").value);
        fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            let filas = "";
            if (data.length > 0) {
                data.forEach(animal => {
                    filas += `<tr><td>${animal.id}</td><td>${animal.raza}</td><td>${animal.id_Animal}</td><td>${animal.fecha_nacimiento}</td></tr>`;
                });
            }
            document.querySelector("#buscador tbody").innerHTML = filas;
            document.querySelectorAll("#buscador tbody tr").forEach(fila => {
                fila.addEventListener("click", function(e) {
                    seleccionar_padre(e.target.closest("tr"));
                });
            });
            if ((data.length === 0) && (Number(document.getElementById("actual").innerHTML)>1)) {
                document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
                if (Number(document.getElementById("actual").innerHTML) === 1) {
                    document.getElementById("ant").parentElement.classList.add("disabled");
                }
                buscar_padres();
            }
        });
    }
    function seleccionar_padre(fila) {
        document.getElementById("padre").value = `Raza: ${fila.children[1].innerHTML}. Código: ${fila.children[2].innerHTML}. Nacimiento: ${fila.children[3].innerHTML}.`;
        document.getElementById("padre").dataset.id = fila.children[0].innerHTML;
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    }
    function anteriorBuscadorPadre() {
        let actual = Number(document.getElementById("actual").innerHTML);
        if (actual > 1) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
            document.getElementById("sig").parentElement.classList.remove("disabled");
            buscar_padres();
        }
        if (actual === 2) {
            document.getElementById("ant").parentElement.classList.add("disabled");
        }
    }
    function siguienteBuscadorPadre() {
        let rpp = Number(document.getElementById("registros").value);
        let filas = document.querySelectorAll("#buscador tbody tr").length;
        if (filas === rpp) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)+1;
            document.getElementById("ant").parentElement.classList.remove("disabled");
            buscar_padres();
        }
    }
    document.getElementById("btn_cerrar_buscador").addEventListener("click", function() {
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    });
    function buscar_medidas_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyMedidas").find("tr:gt(0)").remove();
        fetch('/apl/medida/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyMedidas").append(`<tr><td class="text-right">${element.id}</td><td>${moment(element.fecha_creacion).format("DD/MM/yyyy")}</td><td>${element.pesoKG}</td><td>${element.alturaCM}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_tratamiento_preventivo_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyPreventivos").find("tr:gt(0)").remove();
        fetch('/apl/aplicaciontp/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyPreventivos").append(`<tr><td class="text-right">${element.id}</td><td>${element.nombreTipo} | ${element.nombrePreventivo} | Cada ${element.periodicidad} día/s</td><td>${moment(element.fechaInicio).format("DD/MM/yyyy")}</td><td>${moment(element.fechaFin).format("DD/MM/yyyy")}</td><td>${element.observacion}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_tratamiento_curativo_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyCurativos").find("tr:gt(0)").remove();
        fetch('/apl/curativo/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyCurativos").append(`<tr><td class="text-right">${element.id}</td><td>${element.nombre}</td><td>${moment(element.fechaInicio).format("DD/MM/yyyy")}</td><td>${moment(element.fechaFin).format("DD/MM/yyyy")}</td><td>${element.periodicidad}</td><td>${element.observacion}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_fecundacion_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyFecundaciones").find("tr:gt(0)").remove();
        fetch('/apl/fecundacion/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyFecundaciones").append(`<tr><td class="text-right">${element.id}</td><td>${moment(element.fecha).format("DD/MM/yyyy")}</td><td hidden>${element.id_padre==(window.location.href.split("/")[window.location.href.split("/").length-2])?element.id_madre:element.id_padre}</td><td>${element.id_padre==(window.location.href.split("/")[window.location.href.split("/").length-2])?`${element.nacimiento_animal} | ${element.codigo_animal} | ${element.raza_animal} | Femenino`:`${element.nacimiento_animal} | ${element.codigo_animal} | ${element.raza_animal} | Masculino`}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_palpacion_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyPalpaciones").find("tr:gt(0)").remove();
        fetch('/apl/palpacion/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyPalpaciones").append(`<tr><td class="text-right">${element.id}</td><td>${moment(element.fecha).format("DD/MM/yyyy")}</td><td>${element.id}</td><td>${element.resultado==="E"?"Embarazo":element.resultado==="V"?"Vivo":"Muerto"}</td><td>${element.observacion}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_perdida_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyPerdidas").find("tr:gt(0)").remove();
        fetch('/apl/perdida/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyPerdidas").append(`<tr><td class="text-right">${element.id}</td><td>${moment(element.fecha).format("DD/MM/yyyy")}</td><td data-id="${element.id_Palpacion}">${element.fecha_palpacion}</td><td class="text-right">${element.causa}</td><td></td></tr>`);
                });
            }
        });
    }
    function buscar_nacimiento_animal() {
        var formData = new FormData();
        formData.append('action', 'searchbyanimal');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        $("#tbodyNacimientos").find("tr:gt(0)").remove();
        fetch('/apl/nacimiento/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            if (data.length > 0) {
                data.forEach(element => {
                    $("#tbodyNacimientos").append(`<tr><td class="text-right">${element.id}</td><td>${moment(element.fecha).format("DD/MM/yyyy")}</td><td data-id="${element.id_Fecundacion}">${element.fecha_fecundacion}</td><td data-id="${element.id_Animal}">${element.id_hijo}</td><td class="text-right">${element.raza_hijo}</td><td class="text-right">${element.sexo_hijo}</td><td class="text-right">${element.destino_hijo}</td><td></td></tr>`);
                });
            }
        });
    }
    formData.append('action', 'searchdata');
    fetch('/apl/preventivo/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        if (data.length > 0) {
            data.forEach(element => {
                $("#preventivo").append(`<option value=${element.id}>${element.nombre_tipo} | ${element.nombre} | Cada ${element.periodicidad} día/s</option>`);
            });
        }
    });
    fetch('/apl/raza/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        if (data.length > 0) {
            data.forEach(element => {
                $("#raza_nacimiento").append(`<option value=${element.id}>${element.descripcion}</option>`);
            });
        }
    });
    function buscar_fecundaciones() {
        document.getElementById("formulario").hidden = true;
        document.getElementById("buscador").hidden = false;
        document.querySelector("#buscador .card-title").innerHTML = "Buscador de Fecundaciones correspondientes al animal";
        document.querySelector("#buscador thead").innerHTML = `<tr><th>ID</th><th>Fecha</th><th>Código</th><th>Nacimiento</th><th>Raza</th><th>Sexo</th></tr>`;
        formData = new FormData();
        formData.append('action', 'filterdataRM');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        formData.append('text', document.getElementById("filtro").value);
        formData.append('page', document.getElementById("actual").innerHTML);
        formData.append('rpp', document.getElementById("registros").value);
        fetch('/apl/fecundacion/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            let filas = "";
            if (data.length > 0) {
                data.forEach(fecundacion => {
                    filas += `<tr><td>${fecundacion.id}</td><td>${fecundacion.fecha}</td><td>${fecundacion.codigo_animal}</td><td>${fecundacion.nacimiento_animal}</td><td>${fecundacion.raza_animal}</td><td>${fecundacion.sexo_animal}</td></tr>`;
                });
            }
            document.querySelector("#buscador tbody").innerHTML = filas;
            document.querySelectorAll("#buscador tbody tr").forEach(fila => {
                fila.addEventListener("click", function(e) {
                    seleccionar_fecundacion(e.target.closest("tr"));
                });
            });
            if ((data.length === 0) && (Number(document.getElementById("actual").innerHTML)>1)) {
                document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
                if (Number(document.getElementById("actual").innerHTML) === 1) {
                    document.getElementById("ant").parentElement.classList.add("disabled");
                }
                buscar_fecundaciones();
            }
        });
    }
    function seleccionar_fecundacion(fila) {
        document.getElementById("fecundacion").value = `Fecha: ${fila.children[1].innerHTML}. Código: ${fila.children[2].innerHTML}. Nacimiento: ${fila.children[3].innerHTML}. Raza: ${fila.children[4].innerHTML}. Sexo: ${fila.children[5].innerHTML}.`;
        document.getElementById("fecundacion").dataset.id = fila.children[0].innerHTML;
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    }
    function anteriorBuscadorFecundacion() {
        let actual = Number(document.getElementById("actual").innerHTML);
        if (actual > 1) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
            document.getElementById("sig").parentElement.classList.remove("disabled");
            buscar_fecundaciones();
        }
        if (actual === 2) {
            document.getElementById("ant").parentElement.classList.add("disabled");
        }
    }
    function siguienteBuscadorFecundacion() {
        let rpp = Number(document.getElementById("registros").value);
        let filas = document.querySelectorAll("#buscador tbody tr").length;
        if (filas === rpp) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)+1;
            document.getElementById("ant").parentElement.classList.remove("disabled");
            buscar_fecundaciones();
        }
    }
    function buscar_palpaciones_perdida() {
        document.getElementById("formulario").hidden = true;
        document.getElementById("buscador").hidden = false;
        document.querySelector("#buscador .card-title").innerHTML = "Buscador de Palpaciones correspondientes al animal";
        document.querySelector("#buscador thead").innerHTML = `<tr><th>ID</th><th>Fecha</th><th>Resultado</th><th>Observación</th><th>Fecha Fecundación</th><th>Código</th><th>Nacimiento</th><th>Raza</th><th>Sexo</th></tr>`;
        formData = new FormData();
        formData.append('action', 'filterdataRM');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        formData.append('text', document.getElementById("filtro").value);
        formData.append('page', document.getElementById("actual").innerHTML);
        formData.append('rpp', document.getElementById("registros").value);
        fetch('/apl/palpacion/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            let filas = "";
            if (data.length > 0) {
                data.forEach(palpacion => {
                    filas += `<tr><td>${palpacion.id}</td><td>${palpacion.fecha}</td><td>${palpacion.resultado}</td><td>${palpacion.observacion}</td><td>${palpacion.fecha_fecundacion}</td><td>${palpacion.codigo_animal}</td><td>${palpacion.nacimiento_animal}</td><td>${palpacion.raza_animal}</td><td>${palpacion.sexo_animal}</td></tr>`;
                });
            }
            document.querySelector("#buscador tbody").innerHTML = filas;
            document.querySelectorAll("#buscador tbody tr").forEach(fila => {
                fila.addEventListener("click", function(e) {
                    seleccionar_palpacion_perdida(e.target.closest("tr"));
                });
            });
            if ((data.length === 0) && (Number(document.getElementById("actual").innerHTML)>1)) {
                document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
                if (Number(document.getElementById("actual").innerHTML) === 1) {
                    document.getElementById("ant").parentElement.classList.add("disabled");
                }
                buscar_palpaciones_perdida();
            }
        });
    }
    function seleccionar_palpacion_perdida(fila) {
        document.getElementById("palpacion_perdida").value = `Fecha: ${fila.children[1].innerHTML}. Resultado: ${fila.children[2].innerHTML}. Observación: ${fila.children[3].innerHTML}. Fecha Fecundación: ${fila.children[4].innerHTML}. Código: ${fila.children[5].innerHTML}. Nacimiento: ${fila.children[6].innerHTML}. Raza: ${fila.children[7].innerHTML}. Sexo: ${fila.children[8].innerHTML}.`;
        document.getElementById("palpacion_perdida").dataset.id = fila.children[0].innerHTML;
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    }
    function anteriorBuscadorPalpacionPerdida() {
        let actual = Number(document.getElementById("actual").innerHTML);
        if (actual > 1) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
            document.getElementById("sig").parentElement.classList.remove("disabled");
            buscar_palpaciones_perdida();
        }
        if (actual === 2) {
            document.getElementById("ant").parentElement.classList.add("disabled");
        }
    }
    function siguienteBuscadorPalpacionPerdida() {
        let rpp = Number(document.getElementById("registros").value);
        let filas = document.querySelectorAll("#buscador tbody tr").length;
        if (filas === rpp) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)+1;
            document.getElementById("ant").parentElement.classList.remove("disabled");
            buscar_palpaciones_perdida();
        }
    }
    function buscar_fecundaciones_nacimiento() {
        document.getElementById("formulario").hidden = true;
        document.getElementById("buscador").hidden = false;
        document.querySelector("#buscador .card-title").innerHTML = "Buscador de Fecundaciones correspondientes al animal";
        document.querySelector("#buscador thead").innerHTML = `<tr><th>ID</th><th>Fecha</th><th>Código</th><th>Nacimiento</th><th>Raza</th><th>Sexo</th></tr>`;
        formData = new FormData();
        formData.append('action', 'filterdataRM');
        formData.append('animal', window.location.href.split("/")[window.location.href.split("/").length-2]);
        formData.append('text', document.getElementById("filtro").value);
        formData.append('page', document.getElementById("actual").innerHTML);
        formData.append('rpp', document.getElementById("registros").value);
        fetch('/apl/fecundacion/list/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            let filas = "";
            if (data.length > 0) {
                data.forEach(fecundacion => {
                    filas += `<tr><td>${fecundacion.id}</td><td>${fecundacion.fecha}</td><td>${fecundacion.codigo_animal}</td><td>${fecundacion.nacimiento_animal}</td><td>${fecundacion.raza_animal}</td><td>${fecundacion.sexo_animal}</td></tr>`;
                });
            }
            document.querySelector("#buscador tbody").innerHTML = filas;
            document.querySelectorAll("#buscador tbody tr").forEach(fila => {
                fila.addEventListener("click", function(e) {
                    seleccionar_fecundacion_nacimiento(e.target.closest("tr"));
                });
            });
            if ((data.length === 0) && (Number(document.getElementById("actual").innerHTML)>1)) {
                document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
                if (Number(document.getElementById("actual").innerHTML) === 1) {
                    document.getElementById("ant").parentElement.classList.add("disabled");
                }
                buscar_fecundaciones_nacimiento();
            }
        });
    }
    function seleccionar_fecundacion_nacimiento(fila) {
        document.getElementById("fecundacion_nacimiento").value = `Fecha: ${fila.children[1].innerHTML}. Código: ${fila.children[2].innerHTML}. Nacimiento: ${fila.children[3].innerHTML}. Raza: ${fila.children[4].innerHTML}. Sexo: ${fila.children[5].innerHTML}.`;
        document.getElementById("fecundacion_nacimiento").dataset.id = fila.children[0].innerHTML;
        document.getElementById("formulario").hidden = false;
        document.getElementById("buscador").hidden = true;
    }
    function anteriorBuscadorFecundacionNacimiento() {
        let actual = Number(document.getElementById("actual").innerHTML);
        if (actual > 1) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)-1;
            document.getElementById("sig").parentElement.classList.remove("disabled");
            buscar_fecundaciones_nacimiento();
        }
        if (actual === 2) {
            document.getElementById("ant").parentElement.classList.add("disabled");
        }
    }
    function siguienteBuscadorFecundacionNacimiento() {
        let rpp = Number(document.getElementById("registros").value);
        let filas = document.querySelectorAll("#buscador tbody tr").length;
        if (filas === rpp) {
            document.getElementById("actual").innerHTML = Number(document.getElementById("actual").innerHTML)+1;
            document.getElementById("ant").parentElement.classList.remove("disabled");
            buscar_fecundaciones_nacimiento();
        }
    }
    document.getElementById("btnGuardarMedida").addEventListener("click", 
        function(e) {
            let formData = new FormData();
            formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
            formData.append("action", "add");
            formData.append("id_Animal", window.location.href.split("/")[window.location.href.split("/").length-2]);
            formData.append("fecha_creacion", document.getElementById("fecha_medida").value);
            formData.append("pesoKG", document.getElementById("peso_medida").value);
            formData.append("alturaCM", document.getElementById("altura_medida").value);
            fetch('/apl/medida/add/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
                buscar_medidas_animal();
            });
        });
    document.getElementById("btnGuardarPreventivo").addEventListener("click", 
        function(e) {
            let formData = new FormData();
            formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
            formData.append("action", "add");
            formData.append("id_Animal", window.location.href.split("/")[window.location.href.split("/").length-2]);
            formData.append("fecha_creacion", new Date().toISOString().split("T")[0]);
            formData.append("fechaInicio", document.getElementById("inicio_preventivo").value);
            formData.append("fechaFin", document.getElementById("fin_preventivo").value);
            formData.append("observacion", document.getElementById("observacion_preventivo").value);
            formData.append("id_TPreventivo", document.getElementById("preventivo").value);
            fetch('/apl/aplicaciontp/add/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
                buscar_tratamiento_preventivo_animal();
            });
        });        
    document.getElementById("btnGuardarCurativo").addEventListener("click", 
    function(e) {
        let formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_Animal", window.location.href.split("/")[window.location.href.split("/").length-2]);
        formData.append("nombre", document.getElementById("nombre_curativo").value);
        formData.append("fecha_creacion", new Date().toISOString().split("T")[0]);
        formData.append("fechaInicio", document.getElementById("inicio_curativo").value);
        formData.append("fechaFin", document.getElementById("fin_curativo").value);
        formData.append("periodicidad", document.getElementById("periodicidad_curativo").value);
        formData.append("observacion", document.getElementById("observacion_curativo").value);
        fetch('/apl/curativo/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
            buscar_tratamiento_curativo_animal();
        });
    });
    document.getElementById("btnGuardarFecundacion").addEventListener("click", 
    function(e) {
        let formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_madre", window.location.href.split("/")[window.location.href.split("/").length-2]);
        formData.append("id_padre", document.getElementById("padre").dataset.id);
        formData.append("fecha", document.getElementById("fecha_fecundacion").value);
        fetch('/apl/fecundacion/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
            buscar_fecundacion_animal();
        });
    });
    document.getElementById("btnGuardarPalpacion").addEventListener("click", 
    function(e) {
        let formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_fecundacion", document.getElementById("fecundacion").dataset.id);
        formData.append("resultado", document.getElementById("resultado_palpacion").value);
        formData.append("observacion", document.getElementById("observacion_palpacion").value);
        formData.append("fecha", document.getElementById("fecha_palpacion").value);
        fetch('/apl/palpacion/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
            buscar_palpacion_animal();
        });
    });
    document.getElementById("btnGuardarPerdida").addEventListener("click", 
    function(e) {
        let formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_Palpacion", document.getElementById("palpacion_perdida").dataset.id);
        formData.append("causa", document.getElementById("causa_perdida").value);
        formData.append("fecha", document.getElementById("fecha_perdida").value);
        formData.append("fecha_creacion", new Date().toISOString().split("T")[0]);
        fetch('/apl/perdida/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
            buscar_perdida_animal();
        });
    });
    document.getElementById("btnGuardarNacimiento").addEventListener("click", 
    function(e) {
        let formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_Fecundacion", document.getElementById("fecundacion_nacimiento").dataset.id);
        formData.append("id_Animal", document.getElementById("id_animal").value);
        formData.append("raza", document.getElementById("raza_nacimiento").value);
        formData.append("sexo", document.getElementById("sexo").value);
        formData.append("grupo_destino", document.getElementById("grupo_destino").value);
        formData.append("fecha", document.getElementById("fecha_nacimiento").value);
        formData.append("fecha_nacimiento", document.getElementById("fecha_nacimiento").value);
        formData.append("fecha_creacion", new Date().toISOString().split("T")[0]);
        formData.append("estado", "1");
        fetch('/apl/animal/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            formData.set("id_Animal", data.id);
            fetch('/apl/nacimiento/add/',{method: 'POST', body: formData}).then(function(response) {
                return response.json()
            }).then(function(data) {
                alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
                buscar_nacimiento_animal();
            });
        });
    });
//});