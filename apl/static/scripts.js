function confirmarCierreSesion() {
    $('#bodyConfirmar').html("<p>¿Desea cerrar sesión?</p>");
    $('#btnConfirmar').attr("onclick", "location.href = '/accounts/logout/'");
    $('#confirmar').modal({});
}

function alerta(titulo, mensaje, color) {
    $("#toastAlertas").html("");
    let contenidoAlerta = `<div class="toast bg-${color}" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <strong class="mr-auto">${titulo}</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body">
      ${mensaje}
    </div>
  </div>`;
  $("#toastAlertas").append(contenidoAlerta);
  $('.toast').toast('show');
}

function imprimir_historial(id) {
  let formData = new FormData();
  formData.append("csrfmiddlewaretoken", document.cookie.replace("csrftoken=",""));
  formData.append("action", "reporte");
  formData.append("id", id);
  fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
      return response.json();
  }).then(async function(data) {
      alerta("Alerta del Sistema", "¡Generación exitosa!", "success");
      let pdfWindow = window.open("");
      pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
          encodeURI(data.pdf) + "'></iframe>"
      );
  });
}

function imprimir_vendibles() {
  let formData = new FormData();
  formData.append("csrfmiddlewaretoken", document.cookie.replace("csrftoken=",""));
  formData.append("action", "reporte_vendibles");
  fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
      return response.json();
  }).then(async function(data) {
      alerta("Alerta del Sistema", "¡Generación exitosa!", "success");
      let pdfWindow = window.open("");
      pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
          encodeURI(data.pdf) + "'></iframe>"
      );
  });
}

function imprimir_vendidos() {
  let formData = new FormData();
  formData.append("csrfmiddlewaretoken", document.cookie.replace("csrftoken=",""));
  formData.append("action", "reporte_vendidos");
  fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
      return response.json();
  }).then(async function(data) {
      alerta("Alerta del Sistema", "¡Generación exitosa!", "success");
      let pdfWindow = window.open("");
      pdfWindow.document.write(
          "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
          encodeURI(data.pdf) + "'></iframe>"
      );
  });
}