$('#id_estado').on('mousedown', function(e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
$('#id_fecha_confirmado').on('mousedown', function(e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
$('#id_fecha_envio').on('mousedown', function(e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
$('#id_fecha_recepcion').on('mousedown', function(e) {
    e.preventDefault();
    this.blur();
    window.focus();
});
actualizar_animales_vendibles();
if (window.location.href.includes("update")) {
    document.getElementById("frm").dataset.id = window.location.href.split("/")[window.location.href.split("/").length-2];
    document.getElementById("id_id_Comprador").disabled = true;
    document.getElementById("id_precioKG").disabled = true;
    document.getElementById("btnGuardarVenta").disabled = true;
    buscar_animales_lote();
} else {
    document.getElementById("myTab").parentElement.parentElement.setAttribute("hidden", true);
}
document.getElementById("animal").addEventListener("change", function(e) {
    if (e.target.value == "") {
        document.getElementById("btnGuardarDetalle").setAttribute("disabled", true);
        document.getElementById("subtotal").innerHTML = "0";
    } else {
        document.getElementById("btnGuardarDetalle").removeAttribute("disabled");
        document.getElementById("subtotal").innerHTML = (Number(document.getElementById("id_precioKG").value)*Number(e.target.querySelector("option[value='"+e.target.value+"']").dataset.peso));
    }
});
function actualizar_animales_vendibles() {
    formData = new FormData();
    formData.append('action', 'vendibles');
    fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        let opciones = "<option value='' selected>----------</option>";
        if (data.length > 0) {
            data.forEach(animal => {
                opciones += `<option data-peso='${animal.pesoKG}' value='${animal.id}'>${animal.id_Animal} | Raza: ${animal.descripcion_raza} | Sexo: ${animal.sexo=="M"?"Masculino":"Femenino"} | Peso: ${animal.pesoKG} KG | Fecha Nacimiento: ${animal.fecha_nacimiento} | Fecha Vendible: ${animal.fecha_vendible}</option>`;
            });
        }
        document.getElementById("animal").innerHTML = opciones;
        document.getElementById("animal").addEventListener("change", function(e) {
            if ((document.getElementById("animal").value !== "") && (document.getElementById("frm").dataset.id != "")) {
                document.getElementById("btnGuardarDetalle").disabled = false;
            } else {
                document.getElementById("btnGuardarDetalle").disabled = true;
            }
        });
    });
}
function guardar_detalle() {
    let formData = new FormData();
    formData.append('action', 'subtotal');
    formData.append("id_Lote", document.getElementById("frm").dataset.id);
    formData.append("id_Animal", $("#animal").val());
    fetch('/apl/animal/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(datos) {
        formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("action", "add");
        formData.append("id_Lote", document.getElementById("frm").dataset.id);
        formData.append("id_Animal", $("#animal").val());
        formData.append("subtotalGS", datos.subtotal);
        fetch('/apl/animal_lote/add/',{method: 'POST', body: formData}).then(function(response) {
            return response.json()
        }).then(function(data) {
            alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
            actualizar_animales_vendibles();
            buscar_animales_lote();
        });
    });
}
function buscar_animales_lote() {
    var formData = new FormData();
    formData.append('action', 'searchbyventa');
    formData.append('venta', document.getElementById("frm").dataset.id);
    $("#tbodyDetalles").find("tr:gt(0)").remove();
    fetch('/apl/animal_lote/list/',{method: 'POST', body: formData}).then(function(response) {
        return response.json();
    }).then(function(data) {
        if (data.length > 0) {
            total = 0;
            data.forEach(element => {
                total += Number(element.subtotalGS);
                $("#tbodyDetalles").append(`<tr><td class="text-right align-middle">${element.id}</td><td class="align-middle">${element.codigo_animal} | Raza: ${element.raza_animal} | Sexo: ${element.sexo_animal=="M"?"Masculino":"Femenino"} | Peso: ${element.pesoKG} KG | Fecha Nacimiento: ${element.nacimiento_animal} | Fecha Vendible: ${element.vendible_animal}</td><td class="text-right align-middle">${element.subtotalGS}</td><td class="align-middle"><div class="input-group"><input type="number" onchange="calcular_subtotal_recepcion(this.parentElement.parentElement.parentElement.children);" value="${element.pesoRecepcion!=null?element.pesoRecepcion:0}" class="form-control"${(element.pesoRecepcion)?' disabled':''}/></div></td><td class="align-middle"><div class="input-group"><input type="checkbox" onchange="calcular_subtotal_recepcion(this.parentElement.parentElement.parentElement.children);" class="form-control" ${element.vivoRecepcion ? "checked" : ""}${(element.pesoRecepcion)?' disabled':''}/></div></td><td class="align-middle"><div class="input-group"><input type="text" value="${element.observacionRecepcion!=null ? element.observacionRecepcion : ''}" class="form-control"${(element.pesoRecepcion)?' disabled':''}/></div></td><td class="text-right align-middle">${element.pesoRecepcion?Number(document.getElementById("id_precioKG").value)*Number(element.pesoRecepcion):0}</td><td class="text-center align-middle">${((document.getElementById("id_estado").value == 'E')&&(element.pesoRecepcion == null))?'<button type="button" class="btn btn-sm btn-success" onclick="recepcionar_detalle(this.parentElement.parentElement.children)" disabled><i class="fas fa-check"></i></button>':''}</td></tr>`);
            });
            document.getElementById("total").innerHTML = total;
            let totalRecepcion = 0;
            for (let index = 1; index < (document.getElementById("tbodyDetalles").children).length; index++) {
                const element = (document.getElementById("tbodyDetalles").children)[index];
                totalRecepcion += Number(element.children[6].innerHTML);
            }
            document.getElementById("totalRecepcion").innerHTML = totalRecepcion;
        }
    });
}
function calcular_subtotal_recepcion(columnas) {
    columnas[6].innerHTML = (Number(columnas[3].children[0].children[0].value)*Number(document.getElementById("id_precioKG").value));
    if (Number(columnas[6].innerHTML) > 0) {
        columnas[7].children[0].disabled = false;
    } else {
        columnas[7].children[0].disabled = true;
    }
    let totalRecepcion = 0;
    for (let index = 1; index < (document.getElementById("tbodyDetalles").children).length; index++) {
        const element = (document.getElementById("tbodyDetalles").children)[index];
        totalRecepcion += Number(element.children[6].innerHTML);
    }
    document.getElementById("totalRecepcion").innerHTML = totalRecepcion;
}
function recepcionar_detalle(columnas) {
    let formData = new FormData();
    formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
    formData.append("action", "recepcionar");
    formData.append("id", columnas[0].innerHTML);
    formData.append("pesoRecepcion", columnas[3].children[0].children[0].value);
    formData.append("vivoRecepcion", columnas[4].children[0].children[0].checked?"True":"False");
    formData.append("observacionRecepcion", columnas[5].children[0].children[0].value);
    fetch('/apl/animal_lote/add/',{method: 'POST', body: formData}).then(function(response) {
        return response.json()
    }).then(function(data) {
        alerta("Alerta del Sistema", "¡Guardado exitoso!", "success");
        actualizar_animales_vendibles();
        buscar_animales_lote();
    });
}