from django.contrib import admin
from apl.models import *
# Register your models here.
admin.site.register(Vendedor)
admin.site.register(Comprador)
admin.site.register(Raza)
admin.site.register(Animal)
admin.site.register(Fecundacion)
admin.site.register(Palpacion)
admin.site.register(Nacimiento)
admin.site.register(GrupoDestino)
admin.site.register(Estado)
admin.site.register(Articulo)
admin.site.register(Estimacion)
admin.site.register(DetalleEstimacion)
admin.site.register(Compra)
admin.site.register(DetalleCompra)
