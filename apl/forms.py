from django.forms import ModelForm, TextInput, forms
from apl.models import *
from datetime import date

class VendedorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['nombre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Vendedor
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'nombre': TextInput(
                attrs={
                    'placeholder': 'Ingrese un nombre para el Vendedor',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class CompradorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['nombre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Comprador
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'nombre': TextInput(
                attrs={
                    'placeholder': 'Ingrese un nombre para el Comprador',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class RazaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['descripcion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Raza
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Ingrese una descripción de la Raza',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        if not (cleaned['descripcion'].isalpha()) :
            raise forms.ValidationError('La descripcion de la Raza debe de contener letras ')
        return cleaned

class HistorialForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['raza'].widget.attrs['autofocus'] = True

    class Meta:
        model = Animal
        fields = '__all__'
        exclude = ['fecha_creacion', 'fecha_vendible', 'estado_id']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                data = (form.save()).toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        ##  Validar que no sea menor que dos semanas !
        if (cleaned['fecha_nacimiento']) > date.today():
            raise forms.ValidationError('Fecha de Nacimiento no puede ser mayor a la Fecha Actual')
        return cleaned

class AnimalForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['raza'].widget.attrs['autofocus'] = True

    class Meta:
        model = Animal
        fields = '__all__'
        exclude = ['fecha_creacion', 'fecha_vendible']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                data = (form.save()).toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        ##  Validar que no sea menor que dos semanas !
        if (cleaned['fecha_nacimiento']) > date.today():
            raise forms.ValidationError('Fecha de Nacimiento no puede ser mayor a la Fecha Actual')
        return cleaned


class FecundacionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_madre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Fecundacion
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        # Validar que no se registre en fecha futura.
        if (cleaned['fecha']) > date.today():
            raise forms.ValidationError('Fecha de Fecundacion no puede superar la fecha actual.')
        return cleaned


class PalpacionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_fecundacion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Palpacion
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        # Validar que no se registre en fecha futura.
        if (cleaned['fecha']) > date.today():
            raise forms.ValidationError('Fecha de Palpación no puede superar la fecha actual.')
        return cleaned


class NacimientoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Fecundacion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Nacimiento
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        # Validar que no se registre en fecha futura.        
        if (cleaned['fecha_creacion']) < date.today():
            raise forms.ValidationError('La Fecha de Nacimiento no puede ser más reciente que la fecha actual.')
        return cleaned


class FallecimientoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Animal'].widget.attrs['autofocus'] = True

    class Meta:
        model = Fallecimiento
        exclude = ['fecha_creacion']
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                formData = form.save()
                data = (formData).toJSON()
                animal = Animal.objects.get(id=formData.id_Animal)
                animal.estado = 'M'
                Animal.save(animal)
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class TipoTPrevForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['nombreTipo'].widget.attrs['autofocus'] = True

    class Meta:
        model = TipoTPrev
        exclude = ['fecha_creacion']
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class PreventivoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_TipoTPrev'].widget.attrs['autofocus'] = True

    class Meta:
        model = TPreventivo
        exclude = ['fecha_creacion']
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class AplicacionTPForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Animal'].widget.attrs['autofocus'] = True

    class Meta:
        model = AplicacionTP
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class CurativoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Animal'].widget.attrs['autofocus'] = True

    class Meta:
        model = TCurativo
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class PerdidaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Palpacion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Perdida
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class MedidaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Animal'].widget.attrs['autofocus'] = True

    class Meta:
        model = Medida
        fields = '__all__'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        # Validar que no se registre en fecha futura.
        if (cleaned['fecha_creacion']) > date.today():
            raise forms.ValidationError('Fecha de Medida no puede superar la fecha actual.')
        return cleaned


class GrupoDestinoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['descripcion'].widget.attrs['autofocus'] = True

    class Meta:
        model = GrupoDestino
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Ingrese una descripción de un Grupo Destino',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        if not (cleaned['descripcion'].replace(' ','').isalpha()) :
            raise forms.ValidationError('La descripcion debe de contener solo letras ')
        return cleaned


class EstadoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['descripcion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Estado
        fields = '__all__'
        exclude = ['fecha_creacion']
        widgets = {
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Ingrese una descripción de un Estado',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        if not (cleaned['descripcion'].replace(' ','').isalpha()) :
            raise forms.ValidationError('La descripcion debe de contener solo letras ')
        return cleaned


class ArticuloForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['nombre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Articulo
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class EstimacionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['fechaEstimacion'].widget.attrs['autofocus'] = True

    class Meta:
        model = Estimacion
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                data = (form.save()).toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class EstimacionDetalleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        #     self.fields['articuloId'].widget.attrs['autofocus'] = True

    class Meta:
        model = DetalleEstimacion
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class CompraForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['vendedor'].widget.attrs['autofocus'] = True

    class Meta:
        model = Compra
        fields = '__all__'
        exclude = ['fecha_creacion', 'fecha_confirmado', 'fecha_recibido', 'estado']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                data = (form.save()).toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class CompraDetalleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        #     self.fields['articuloId'].widget.attrs['autofocus'] = True

    class Meta:
        model = DetalleCompra
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class LoteVentaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['id_Comprador'].widget.attrs['autofocus'] = True

    class Meta:
        model = LoteVenta
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                data = (form.save()).toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
    
class AnimalLoteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        #     self.fields['articuloId'].widget.attrs['autofocus'] = True

    class Meta:
        model = AnimalesLote
        fields = '__all__'
        exclude = ['fecha_creacion']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data