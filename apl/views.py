import os
import time
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from datetime import datetime
### from rest_framework import serializers ###
from apl.forms import *
from apl.models import *
from fpdf import FPDF
import base64

### Inicio ###
class InicioView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "inicio/index.html")

### HISTORIAL ###
class HistorialListView(ListView):
    model = Animal
    template_name = 'historial/list.html'
    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Historial.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Animal.objects.select_related().all():
                    json = i.toJSON()
                    json['descripcion_raza'] = i.raza.descripcion
                    json['descripcion_grupo_destino'] = i.grupo_destino.descripcion
                    json['descripcion_estado'] = i.estado.descripcion
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Animales'
        context['create_url'] = reverse_lazy('apl:historial_create')
        context['list_url'] = reverse_lazy('apl:historial_list')
        context['entity'] = 'Animales'
        return context
    
class HistorialCreateView(CreateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'historial/create.html'
    success_url = reverse_lazy('apl:historial_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Historial.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:historial_list')
        context['action'] = 'add'
        return context


class HistorialUpdateView(UpdateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'historial/create.html'
    success_url = reverse_lazy('apl:historial_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Historial.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:historial_list')
        context['action'] = 'edit'
        return context


class HistorialDeleteView(DeleteView):
    model = Animal
    template_name = 'historial/delete.html'
    success_url = reverse_lazy('apl:historial_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Historial.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:historial_list')
        return context

class HistorialFormView(FormView):
    form_class = HistorialForm
    template_name = 'Historial/delete.html'
    success_url = reverse_lazy('apl:historial_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Historial.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Historial'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:historial_list')
        context['action'] = 'add'
        return context

### VENDEDOR ###
class VendedorListView(ListView):
    model = Vendedor
    template_name = 'vendedor/list.html'
    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Vendedor.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Vendedor.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Vendedor'
        context['create_url'] = reverse_lazy('apl:vendedor_create')
        context['list_url'] = reverse_lazy('apl:vendedor_list')
        context['entity'] = 'Vendedores'
        return context


class VendedorCreateView(CreateView):
    model = Vendedor
    form_class = VendedorForm
    template_name = 'vendedor/create.html'
    success_url = reverse_lazy('apl:vendedor_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Vendedor.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Vendedor'
        context['entity'] = 'Vendedores'
        context['list_url'] = reverse_lazy('apl:vendedor_list')
        context['action'] = 'add'
        return context


class VendedorUpdateView(UpdateView):
    model = Vendedor
    form_class = VendedorForm
    template_name = 'vendedor/create.html'
    success_url = reverse_lazy('apl:vendedor_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Vendedor.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Vendedor'
        context['entity'] = 'Vendedores'
        context['list_url'] = reverse_lazy('apl:vendedor_list')
        context['action'] = 'edit'
        return context


class VendedorDeleteView(DeleteView):
    model = Vendedor
    template_name = 'vendedor/delete.html'
    success_url = reverse_lazy('apl:vendedor_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Vendedor.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Vendedor'
        context['entity'] = 'Vendedores'
        context['list_url'] = reverse_lazy('apl:vendedor_list')
        return context


class VendedorFormView(FormView):
    form_class = Vendedor
    template_name = 'vendedor/delete.html'
    success_url = reverse_lazy('apl:vendedor_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Vendedor.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Vendedor'
        context['entity'] = 'Vendedor'
        context['list_url'] = reverse_lazy('apl:vendedor_list')
        context['action'] = 'add'
        return context
    
### COMPRADOR ###
class CompradorListView(ListView):
    model = Comprador
    template_name = 'comprador/list.html'
    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Comprador.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Comprador.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Comprador'
        context['create_url'] = reverse_lazy('apl:comprador_create')
        context['list_url'] = reverse_lazy('apl:comprador_list')
        context['entity'] = 'Compradores'
        return context


class CompradorCreateView(CreateView):
    model = Comprador
    form_class = CompradorForm
    template_name = 'comprador/create.html'
    success_url = reverse_lazy('apl:comprador_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Comprador.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Comprador'
        context['entity'] = 'Compradores'
        context['list_url'] = reverse_lazy('apl:comprador_list')
        context['action'] = 'add'
        return context


class CompradorUpdateView(UpdateView):
    model = Comprador
    form_class = CompradorForm
    template_name = 'comprador/create.html'
    success_url = reverse_lazy('apl:comprador_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Comprador.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Comprador'
        context['entity'] = 'Compradores'
        context['list_url'] = reverse_lazy('apl:comprador_list')
        context['action'] = 'edit'
        return context


class CompradorDeleteView(DeleteView):
    model = Comprador
    template_name = 'comprador/delete.html'
    success_url = reverse_lazy('apl:comprador_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Comprador.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Comprador'
        context['entity'] = 'Compradores'
        context['list_url'] = reverse_lazy('apl:comprador_list')
        return context


class CompradorFormView(FormView):
    form_class = Comprador
    template_name = 'comprador/delete.html'
    success_url = reverse_lazy('apl:comprador_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Comprador.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Comprador'
        context['entity'] = 'Comprador'
        context['list_url'] = reverse_lazy('apl:comprador_list')
        context['action'] = 'add'
        return context

### RAZA ###
class RazaListView(ListView):
    model = Raza
    template_name = 'raza/list.html'
    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Raza.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Raza.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Raza'
        context['create_url'] = reverse_lazy('apl:raza_create')
        context['list_url'] = reverse_lazy('apl:raza_list')
        context['entity'] = 'Razas'
        return context


class RazaCreateView(CreateView):
    model = Raza
    form_class = RazaForm
    template_name = 'raza/create.html'
    success_url = reverse_lazy('apl:raza_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Raza.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Raza'
        context['entity'] = 'Razas'
        context['list_url'] = reverse_lazy('apl:raza_list')
        context['action'] = 'add'
        return context


class RazaUpdateView(UpdateView):
    model = Raza
    form_class = RazaForm
    template_name = 'raza/create.html'
    success_url = reverse_lazy('apl:raza_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Raza.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Raza'
        context['entity'] = 'Razas'
        context['list_url'] = reverse_lazy('apl:raza_list')
        context['action'] = 'edit'
        return context


class RazaDeleteView(DeleteView):
    model = Raza
    template_name = 'raza/delete.html'
    success_url = reverse_lazy('apl:raza_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Raza.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Raza'
        context['entity'] = 'Razas'
        context['list_url'] = reverse_lazy('apl:raza_list')
        return context


class RazaFormView(FormView):
    form_class = Raza
    template_name = 'raza/delete.html'
    success_url = reverse_lazy('apl:raza_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Raza.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Raza'
        context['entity'] = 'Raza'
        context['list_url'] = reverse_lazy('apl:raza_list')
        context['action'] = 'add'
        return context


### ANIMAL ###
class AnimalListView(ListView):
    model = Animal
    template_name = 'animal/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Animal.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':     
                data = []
                for i in Animal.objects.select_related().all():
                    data.append(i.toJSON())
            elif action == 'vendibles':     
                data = []
                for i in Animal.objects.filter(grupo_destino=1, estado=1).all().exclude(fecha_vendible__isnull=True):
                    medida = Medida.objects.filter(id_Animal_id=i.id).order_by('-fecha_creacion').first()
                    if medida != None:
                        json = i.toJSON()
                        json['descripcion_raza'] = i.raza.descripcion
                        json['descripcion_grupo_destino'] = i.grupo_destino.descripcion
                        json['descripcion_estado'] = i.estado.descripcion
                        json['pesoKG'] = medida.pesoKG
                        data.append(json)
                    else :
                        data = {}
            elif action == 'subtotal':  
                id_Lote = request.POST['id_Lote']   
                id_Animal = request.POST['id_Animal']
                pesoKG = 0
                precioKG = 0  
                data = {}
                medida = Medida.objects.filter(id_Animal_id=id_Animal).order_by('-fecha_creacion').first()
                if medida != None:
                    pesoKG = medida.pesoKG
                lote = LoteVenta.objects.filter(id=id_Lote).first()
                if lote != None:
                    precioKG = lote.precioKG
                data['subtotal'] = pesoKG*precioKG
            elif action == 'filterdataRM':
                text = request.POST['text']
                page = request.POST['page']
                rpp = request.POST['rpp']
                data = []
                animales = Animal.objects.filter(sexo='M', grupo_destino=2, estado=1)
                animales = animales.filter(id_Animal__contains=text) | animales.filter(fecha_nacimiento__contains=text) | animales.filter(raza__descripcion__contains=text)
                paginator = Paginator(animales, rpp)
                try:
                    animales = paginator.page(page)
                except PageNotAnInteger:
                    animales = paginator.page(1)
                except EmptyPage:
                    animales = []
                for i in animales:
                    data.append(i.toJSON())
            elif action == 'reporte':
                id = request.POST['id']
                data = {}
                animal = (Animal.objects.select_related().filter(id=id))[0]
                class PDF(FPDF):
                    def header(self):
                        # Logo
                        self.image('static/img/logo.png', 10, 8, 17)
                        # Arial bold 15
                        self.set_font('Arial', 'B', 15)
                        # Mover a la derecha
                        self.cell(17)
                        # Título
                        self.multi_cell(30, 7, 'Sistema Ganadero', 0,'L')
                        self.set_y(12)
                        self.cell(0, 7, 'Historial del Animal', 0, 0,'C')
                        # Arial normal 10
                        self.set_font('Arial', '', 10)
                        # Usuario y fecha
                        self.set_y(8)
                        self.cell(0, 5, request.user.username, 0, 1,'R')
                        self.set_x(321)
                        self.multi_cell(25, 5, datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 0,'R')
                        self.ln()
                        # Arial bold 10
                        self.set_font('Arial', 'B', 10)
                        # Encabezado de datos básicos del animal
                        self.cell(15, 7, 'ID', 1, 0, 'L')
                        self.cell(30, 7, 'Nacimiento', 1, 0, 'L')
                        self.cell(30, 7, 'Sexo', 1, 0, 'L')
                        self.cell(45, 7, 'Raza', 1, 0, 'L')
                        self.cell(35, 7, 'Grupo Destino', 1, 0, 'L')
                        self.cell(35, 7, 'Fecha Vendible', 1, 0, 'L')
                        self.cell(20, 7, 'Castrado', 1, 0, 'L')
                        self.cell(30, 7, 'Categoría', 1, 0, 'L')
                        self.cell(0, 7, 'Estado', 1, 0, 'L')
                        self.ln()
                        # Arial bold 10
                        self.set_font('Arial', '', 10)
                        self.cell(15, 7, animal.id_Animal, 1, 0, 'L')
                        self.cell(30, 7, animal.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                        self.cell(30, 7, 'Femenino' if animal.sexo=='F' else 'Masculino', 1, 0, 'L')
                        self.cell(45, 7, animal.raza.descripcion, 1, 0, 'L')
                        grupo_destino = animal.grupo_destino.descripcion
                        if (animal.grupo_destino.id == 1) and (animal.fecha_vendible != None):
                            grupo_destino += ' (' + animal.fecha_vendible.strftime("%d/%m/%Y") + ')'
                        self.cell(35, 7, grupo_destino, 1, 0, 'L')
                        fecha_vendible = ''
                        if (animal.fecha_vendible != None):
                            fecha_vendible = animal.fecha_vendible.strftime("%d/%m/%Y")
                        self.cell(35, 7, fecha_vendible, 1, 0, 'L')
                        castrado = 'No'
                        if (animal.castrado):
                            castrado = 'Sí'
                        self.cell(20, 7, castrado, 1, 0, 'L')
                        categoria = ''
                        if ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                            categoria = 'Ternero'
                        elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                            categoria = 'Ternera' 
                        elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'No')):
                            categoria = 'Torito'
                        elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'Sí')):
                            categoria = 'Novillito'
                        elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730)):
                            categoria = 'Vaquillona'
                        elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'No')):
                            categoria = 'Toro'
                        elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'Sí')):
                            categoria = 'Novillo'
                        elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730)):
                            categoria = 'Vaca'
                        self.cell(30, 7, categoria, 1, 0, 'L')
                        self.cell(0, 7, animal.estado.descripcion, 1, 0, 'L')
                        self.ln(10)
                    def footer(self):
                        # Posición a 1.5 cm de abajo
                        self.set_y(-15)
                        # Arial italic 8
                        self.set_font('Arial', 'I', 8)
                        # Número de página
                        self.cell(0, 10, 'Página ' + str(self.page_no()) + ' de {nb}', 0, 0, 'C')
                pdf = PDF(orientation = 'L', unit = 'mm', format='Legal')
                pdf.alias_nb_pages()
                pdf.add_page()
                nacimiento = Nacimiento.objects.select_related().filter(id_Animal_id=id).first()
                if nacimiento != None:
                    pdf.set_font('Arial', 'B', 12)
                    pdf.cell(0, 7, 'PADRE', 1, 0, 'C')
                    pdf.ln()
                    pdf.set_font('Arial', 'B', 10)
                    pdf.cell(20, 7, 'ID', 1, 0, 'L')
                    pdf.cell(150, 7, 'Nacimiento', 1, 0, 'L')
                    pdf.cell(0, 7, 'Raza', 1, 0, 'L')
                    pdf.set_font('Arial', '', 10)
                    pdf.ln()
                    pdf.cell(20, 7, nacimiento.id_Fecundacion.id_padre.id_Animal, 1, 0, 'L')
                    pdf.cell(150, 7, nacimiento.id_Fecundacion.id_padre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                    pdf.cell(0, 7, nacimiento.id_Fecundacion.id_padre.raza.descripcion, 1, 0, 'L')
                    pdf.set_font('Arial', 'B', 12)
                    pdf.ln(10)
                    pdf.cell(0, 7, 'MADRE', 1, 0, 'C')
                    pdf.ln()
                    pdf.set_font('Arial', 'B', 10)
                    pdf.cell(20, 7, 'ID', 1, 0, 'L')
                    pdf.cell(150, 7, 'Nacimiento', 1, 0, 'L')
                    pdf.cell(0, 7, 'Raza', 1, 0, 'L')
                    pdf.set_font('Arial', '', 10)
                    pdf.ln()
                    pdf.cell(20, 7, nacimiento.id_Fecundacion.id_madre.id_Animal, 1, 0, 'L')
                    pdf.cell(150, 7, nacimiento.id_Fecundacion.id_madre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                    pdf.cell(0, 7, nacimiento.id_Fecundacion.id_madre.raza.descripcion, 1, 0, 'L')
                # Consultar los detalles y crear las tablas respectivas para el reporte
                medidas = Medida.objects.select_related().filter(id_Animal_id=id).order_by('-fecha_creacion', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'MEDIDAS', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(80, 7, 'Fecha', 1, 0, 'L')
                pdf.cell(60, 7, 'Peso', 1, 0, 'L')
                pdf.cell(0, 7, 'Altura', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(medidas) > 0 :
                    for medida in medidas:
                        pdf.ln()
                        pdf.cell(80, 7, medida.fecha_creacion.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(60, 7, str(medida.pesoKG), 1, 0, 'L')
                        pdf.cell(0, 7, str(medida.alturaCM), 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                aplicacionesTP = AplicacionTP.objects.select_related().filter(id_Animal_id=id).order_by('-fechaInicio', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'TRATAMIENTOS PREVENTIVOS', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(30, 7, 'Tipo', 1, 0, 'L')
                pdf.cell(30, 7, 'Nombre', 1, 0, 'L')
                pdf.cell(30, 7, 'Inicio', 1, 0, 'L')
                pdf.cell(30, 7, 'Fin', 1, 0, 'L')
                pdf.cell(25, 7, 'Periodicidad', 1, 0, 'L')
                pdf.cell(0, 7, 'Observación', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(aplicacionesTP) > 0 :
                    for aplicacionTP in aplicacionesTP:
                        pdf.ln()
                        pdf.cell(30, 7, aplicacionTP.id_TPreventivo.id_TipoTPrev.nombreTipo, 1, 0, 'L')
                        pdf.cell(30, 7, aplicacionTP.id_TPreventivo.nombre, 1, 0, 'L')
                        pdf.cell(30, 7, aplicacionTP.fechaInicio.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(30, 7, aplicacionTP.fechaFin.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(25, 7, str(aplicacionTP.id_TPreventivo.periodicidad), 1, 0, 'L')
                        pdf.cell(0, 7, aplicacionTP.observacion, 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                tCurativos = TCurativo.objects.select_related().filter(id_Animal_id=id).order_by('-fechaInicio', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'TRATAMIENTOS CURATIVOS', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(30, 7, 'Nombre', 1, 0, 'L')
                pdf.cell(30, 7, 'Inicio', 1, 0, 'L')
                pdf.cell(30, 7, 'Fin', 1, 0, 'L')
                pdf.cell(25, 7, 'Periodicidad', 1, 0, 'L')
                pdf.cell(0, 7, 'Observación', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(tCurativos) > 0 :
                    for tCurativo in tCurativos:
                        pdf.ln()
                        pdf.cell(30, 7, tCurativo.nombre, 1, 0, 'L')
                        pdf.cell(30, 7, tCurativo.fechaInicio.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(30, 7, tCurativo.fechaFin.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(25, 7, str(tCurativo.periodicidad), 1, 0, 'L')
                        pdf.cell(0, 7, tCurativo.observacion, 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                fecundaciones = (Fecundacion.objects.select_related().filter(id_madre=id) | Fecundacion.objects.select_related().filter(id_padre=id)).order_by('-fecha', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'FECUNDACIONES', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(40, 14, 'Fecha', 1, 0, 'L')
                pdf.cell(0, 7, 'Progenitor/a', 1, 1, 'C')
                pdf.set_x(50)
                pdf.cell(40, 7, 'Nacimiento', 1, 0, 'L')
                pdf.cell(30, 7, 'ID', 1, 0, 'L')
                pdf.cell(50, 7, 'Raza', 1, 0, 'L')
                pdf.cell(0, 7, 'Sexo', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(fecundaciones) > 0 :
                    for fecundacion in fecundaciones:
                        pdf.ln()
                        pdf.cell(40, 7, fecundacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(40, 7, fecundacion.id_padre.fecha_nacimiento.strftime("%d/%m/%Y") if animal.sexo=='F' else fecundacion.id_madre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(30, 7, fecundacion.id_padre.id_Animal if animal.sexo=='F' else fecundacion.id_madre.id_Animal, 1, 0, 'L')
                        pdf.cell(50, 7, fecundacion.id_padre.raza.descripcion if animal.sexo=='F' else fecundacion.id_madre.raza.descripcion, 1, 0, 'L')
                        pdf.cell(0, 7, 'Masculino' if animal.sexo=='F' else 'Femenino', 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                palpaciones = (Palpacion.objects.select_related().filter(id_fecundacion__id_madre=id) | Palpacion.objects.select_related().filter(id_fecundacion__id_padre=id)).order_by('-fecha', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'PALPACIONES', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(25, 14, 'Fecha', 1, 0, 'L')
                pdf.cell(25, 7, 'Fecundación', 1, 0, 'C')
                pdf.cell(85, 7, 'Progenitor/a', 1, 1, 'C')
                pdf.set_x(35)
                pdf.cell(25, 7, 'Fecha', 1, 0, 'L')
                pdf.cell(25, 7, 'Nacimiento', 1, 0, 'L')
                pdf.cell(15, 7, 'ID', 1, 0, 'L')
                pdf.cell(25, 7, 'Raza', 1, 0, 'L')
                pdf.cell(20, 7, 'Sexo', 1, 0, 'L')
                pdf.set_y(pdf.get_y()-7)
                pdf.set_x(145)
                pdf.cell(20, 14, 'Resultado', 1, 0, 'L')
                pdf.cell(0, 14, 'Observación', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(palpaciones) > 0 :
                    for palpacion in palpaciones:
                        pdf.ln()
                        pdf.cell(25, 7, palpacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(25, 7, palpacion.id_fecundacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(25, 7, palpacion.id_fecundacion.id_padre.fecha_nacimiento.strftime("%d/%m/%Y") if animal.sexo=='F' else palpacion.id_fecundacion.id_madre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(15, 7, palpacion.id_fecundacion.id_padre.id_Animal if animal.sexo=='F' else palpacion.id_fecundacion.id_madre.id_Animal, 1, 0, 'L')
                        pdf.cell(25, 7, palpacion.id_fecundacion.id_padre.raza.descripcion if animal.sexo=='F' else palpacion.id_fecundacion.id_madre.raza.descripcion, 1, 0, 'L')
                        pdf.cell(20, 7, 'Masculino' if animal.sexo=='F' else 'Femenino', 1, 0, 'L')
                        pdf.cell(20, 7, 'Vivo' if palpacion.resultado=='V' else 'Muerto', 1, 0, 'L')
                        pdf.cell(0, 7, palpacion.observacion, 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                perdidas = (Perdida.objects.select_related().filter(id_Palpacion__in=Palpacion.objects.select_related().filter(id_fecundacion__id_padre=id)).order_by('-fecha', '-id') | Perdida.objects.select_related().filter(id_Palpacion__in=Palpacion.objects.select_related().filter(id_fecundacion__id_madre=id))).order_by('-fecha', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'PERDIDAS', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(20, 14, 'Fecha', 1, 0, 'L')
                pdf.cell(75, 7, 'Progenitor/a', 1, 0, 'C')
                pdf.cell(22, 7, 'Fecundación', 1, 0, 'C')
                pdf.cell(60, 7, 'Palpación', 1, 1, 'C')
                pdf.set_x(30)
                pdf.cell(20, 7, 'Nacimiento', 1, 0, 'L')
                pdf.cell(15, 7, 'ID', 1, 0, 'L')
                pdf.cell(20, 7, 'Raza', 1, 0, 'L')
                pdf.cell(20, 7, 'Sexo', 1, 0, 'L')
                pdf.cell(22, 7, 'Fecha', 1, 0, 'L')
                pdf.cell(20, 7, 'Fecha', 1, 0, 'L')
                pdf.cell(20, 7, 'Resultado', 1, 0, 'L')
                pdf.cell(20, 7, 'Obs', 1, 0, 'L')
                pdf.set_y(pdf.get_y()-7)
                pdf.set_x(187)
                pdf.cell(0, 14, 'Causa', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(perdidas) > 0 :
                    for perdida in perdidas:
                        pdf.ln()
                        pdf.cell(20, 7, perdida.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(20, 7, perdida.id_Palpacion.id_fecundacion.id_padre.fecha_nacimiento.strftime("%d/%m/%Y") if animal.sexo=='F' else perdida.id_Palpacion.id_fecundacion.id_madre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(15, 7, perdida.id_Palpacion.id_fecundacion.id_padre.id_Animal if animal.sexo=='F' else perdida.id_Palpacion.id_fecundacion.id_madre.id_Animal, 1, 0, 'L')
                        pdf.cell(20, 7, perdida.id_Palpacion.id_fecundacion.id_padre.raza.descripcion if animal.sexo=='F' else perdida.id_Palpacion.id_fecundacion.id_madre.raza.descripcion, 1, 0, 'L')
                        pdf.cell(20, 7, 'Masculino' if animal.sexo=='F' else 'Femenino', 1, 0, 'L')
                        pdf.cell(20, 7, perdida.id_Palpacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(20, 7, perdida.id_Palpacion.id_fecundacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(20, 7, 'Vivo' if perdida.id_Palpacion.resultado=='V' else 'Muerto', 1, 0, 'L')
                        pdf.cell(20, 7, perdida.id_Palpacion.observacion, 1, 0, 'L')
                        pdf.cell(00, 7, perdida.causa, 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                nacimientos = (Nacimiento.objects.select_related().filter(id_Fecundacion__in=Fecundacion.objects.select_related().filter(id_madre=id)).order_by('-fecha', '-id') | Nacimiento.objects.select_related().filter(id_Fecundacion__in=Fecundacion.objects.select_related().filter(id_padre=id))).order_by('-fecha', '-id')
                pdf.set_font('Arial', 'B', 12)
                pdf.ln(10)
                pdf.cell(0, 7, 'NACIMIENTOS', 1, 0, 'C')
                pdf.ln()
                pdf.set_font('Arial', 'B', 10)
                pdf.cell(20, 14, 'Fecha', 1, 0, 'L')
                pdf.cell(75, 7, 'Progenitor/a', 1, 0, 'C')
                pdf.cell(22, 7, 'Fecundación', 1, 1, 'C')
                pdf.set_x(30)
                pdf.cell(20, 7, 'Nacimiento', 1, 0, 'L')
                pdf.cell(15, 7, 'ID', 1, 0, 'L')
                pdf.cell(20, 7, 'Raza', 1, 0, 'L')
                pdf.cell(20, 7, 'Sexo', 1, 0, 'L')
                pdf.cell(22, 7, 'Fecha', 1, 0, 'L')
                pdf.set_y(pdf.get_y()-7)
                pdf.set_x(127)
                pdf.cell(15, 14, 'ID', 1, 0, 'L')
                pdf.cell(20, 14, 'Raza', 1, 0, 'L')
                pdf.cell(20, 14, 'Sexo', 1, 0, 'L')
                pdf.cell(0, 14, 'Grupo', 1, 0, 'L')
                pdf.set_font('Arial', '', 10)
                if len(nacimientos) > 0 :
                    for nacimiento in nacimientos:
                        pdf.ln()
                        pdf.cell(20, 7, nacimiento.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(20, 7, nacimiento.id_Fecundacion.id_padre.fecha_nacimiento.strftime("%d/%m/%Y") if animal.sexo=='F' else nacimiento.id_Fecundacion.id_madre.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(15, 7, nacimiento.id_Fecundacion.id_padre.id_Animal if animal.sexo=='F' else nacimiento.id_Fecundacion.id_madre.id_Animal, 1, 0, 'L')
                        pdf.cell(20, 7, nacimiento.id_Fecundacion.id_padre.raza.descripcion if animal.sexo=='F' else nacimiento.id_Fecundacion.id_madre.raza.descripcion, 1, 0, 'L')
                        pdf.cell(20, 7, 'Masculino' if animal.sexo=='F' else 'Femenino', 1, 0, 'L')
                        pdf.cell(20, 7, nacimiento.id_Fecundacion.fecha.strftime("%d/%m/%Y"), 1, 0, 'L')
                        pdf.cell(15, 7, nacimiento.id_Animal.id_Animal, 1, 0, 'L')
                        pdf.cell(20, 7, nacimiento.id_Animal.raza.descripcion, 1, 0, 'L')
                        pdf.cell(20, 7, 'Femenino' if nacimiento.id_Animal.sexo=='F' else 'Masculino', 1, 0, 'L')
                        pdf.cell(0, 7, nacimiento.id_Animal.grupo_destino.descripcion, 1, 0, 'L')
                else:
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                pdf.output(name='historial.pdf', dest='F')
                with open("historial.pdf", "rb") as pdf_file:
                    data['pdf'] = base64.b64encode(pdf_file.read()).decode("ascii")
                os.remove("historial.pdf")
            elif action == 'reporte_vendibles':
                data = {}
                class PDF(FPDF):
                    def header(self):
                        # Logo
                        self.image('static/img/logo.png', 10, 8, 17)
                        # Arial bold 15
                        self.set_font('Arial', 'B', 15)
                        # Mover a la derecha
                        self.cell(17)
                        # Título
                        self.multi_cell(30, 7, 'Sistema Ganadero', 0,'L')
                        self.set_y(12)
                        self.cell(0, 7, 'Historial del Animal', 0, 0,'C')
                        # Arial normal 10
                        self.set_font('Arial', '', 10)
                        # Usuario y fecha
                        self.set_y(8)
                        self.cell(0, 5, request.user.username, 0, 1,'R')
                        self.set_x(321)
                        self.multi_cell(25, 5, datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 0,'R')
                        self.ln()
                        # Arial bold 10
                        self.set_font('Arial', 'B', 10)
                        # Encabezado de datos básicos del animal
                        self.cell(15, 7, 'ID', 1, 0, 'L')
                        self.cell(30, 7, 'Nacimiento', 1, 0, 'L')
                        self.cell(30, 7, 'Sexo', 1, 0, 'L')
                        self.cell(40, 7, 'Raza', 1, 0, 'L')
                        self.cell(37, 7, 'Grupo Destino', 1, 0, 'L')
                        self.cell(35, 7, 'Fecha Vendible', 1, 0, 'L')
                        self.cell(20, 7, 'Castrado', 1, 0, 'L')
                        self.cell(30, 7, 'Categoría', 1, 0, 'L')
                        self.cell(40, 7, 'Medidas', 1, 0, 'L')
                        self.cell(40, 7, 'Ascendencia', 1, 0, 'L')
                        self.cell(0, 7, 'Estado', 1, 0, 'L')
                    def footer(self):
                        # Posición a 1.5 cm de abajo
                        self.set_y(-15)
                        # Arial italic 8
                        self.set_font('Arial', 'I', 8)
                        # Número de página
                        self.cell(0, 10, 'Página ' + str(self.page_no()) + ' de {nb}', 0, 0, 'C')
                pdf = PDF(orientation = 'L', unit = 'mm', format='Legal')
                pdf.alias_nb_pages()
                pdf.add_page()
                pdf.set_font('Arial', 'B', 10)
                # Consultar los animales y renderizar las filas
                animales = Animal.objects.select_related().filter(grupo_destino=1, estado=1).order_by('-fecha_vendible', '-id')
                pdf.set_font('Arial', '', 10)
                no_vendible = 0
                if len(animales) > 0 :
                    for animal in animales:
                        if animal.fecha_vendible is not None:
                            pdf.ln()
                            pdf.cell(15, 7, animal.id_Animal, 1, 0, 'L')
                            pdf.cell(30, 7, animal.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                            pdf.cell(30, 7, 'Femenino' if animal.sexo=='F' else 'Masculino', 1, 0, 'L')
                            pdf.cell(40, 7, animal.raza.descripcion, 1, 0, 'L')
                            grupo_destino = animal.grupo_destino.descripcion
                            if (animal.grupo_destino.id == 1) and (animal.fecha_vendible != None):
                                grupo_destino += ' (' + animal.fecha_vendible.strftime("%d/%m/%Y") + ')'
                            pdf.cell(37, 7, grupo_destino, 1, 0, 'L')
                            fecha_vendible = ''
                            if (animal.fecha_vendible != None):
                                fecha_vendible = animal.fecha_vendible.strftime("%d/%m/%Y")
                            pdf.cell(35, 7, fecha_vendible, 1, 0, 'L')
                            castrado = 'No'
                            if (animal.castrado):
                                castrado = 'Sí'
                            pdf.cell(20, 7, castrado, 1, 0, 'L')
                            categoria = ''
                            if ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                                categoria = 'Ternero'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                                categoria = 'Ternera' 
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'No')):
                                categoria = 'Torito'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'Sí')):
                                categoria = 'Novillito'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730)):
                                categoria = 'Vaquillona'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'No')):
                                categoria = 'Toro'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'Sí')):
                                categoria = 'Novillo'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730)):
                                categoria = 'Vaca'
                            pdf.cell(30, 7, categoria, 1, 0, 'L')
                            medidas = Medida.objects.select_related().filter(id_Animal_id=animal.id).order_by('-fecha_creacion', '-id')
                            if len(medidas) > 0 :
                                medida = medidas[0]
                                pdf.cell(40, 7, 'Peso: '+str(medida.pesoKG)+'. Altura: '+str(medida.alturaCM), 1, 0, 'L')
                            else:
                                pdf.cell(40, 7, '', 1, 0, 'C')
                            nacimiento = Nacimiento.objects.select_related().filter(id_Animal_id=animal.id).first()
                            if nacimiento != None:
                                pdf.cell(40, 7, 'Padre: '+nacimiento.id_Fecundacion.id_padre.id_Animal+'. Madre: '+nacimiento.id_Fecundacion.id_madre.id_Animal, 1, 0, 'L')
                            else:
                                pdf.cell(40, 7, '', 1, 0, 'L')
                            pdf.cell(0, 7, animal.estado.descripcion, 1, 0, 'L')
                        else:
                            no_vendible+=1
                elif ((len(animales) > 0) and (no_vendible==len(animales))) or (len(animales) == 0) :
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                pdf.output(name='vendibles.pdf', dest='F')
                with open("vendibles.pdf", "rb") as pdf_file:
                    data['pdf'] = base64.b64encode(pdf_file.read()).decode("ascii")
                os.remove("vendibles.pdf")
            elif action == 'reporte_vendidos':
                data = {}
                class PDF(FPDF):
                    def header(self):
                        # Logo
                        self.image('static/img/logo.png', 10, 8, 17)
                        # Arial bold 15
                        self.set_font('Arial', 'B', 15)
                        # Mover a la derecha
                        self.cell(17)
                        # Título
                        self.multi_cell(30, 7, 'Sistema Ganadero', 0,'L')
                        self.set_y(12)
                        self.cell(0, 7, 'Historial del Animal', 0, 0,'C')
                        # Arial normal 10
                        self.set_font('Arial', '', 10)
                        # Usuario y fecha
                        self.set_y(8)
                        self.cell(0, 5, request.user.username, 0, 1,'R')
                        self.set_x(321)
                        self.multi_cell(25, 5, datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 0,'R')
                        self.ln()
                        # Arial bold 10
                        self.set_font('Arial', 'B', 10)
                        # Encabezado de datos básicos del animal
                        self.cell(15, 7, 'ID', 1, 0, 'L')
                        self.cell(30, 7, 'Nacimiento', 1, 0, 'L')
                        self.cell(30, 7, 'Sexo', 1, 0, 'L')
                        self.cell(40, 7, 'Raza', 1, 0, 'L')
                        self.cell(37, 7, 'Grupo Destino', 1, 0, 'L')
                        self.cell(35, 7, 'Fecha Vendible', 1, 0, 'L')
                        self.cell(20, 7, 'Castrado', 1, 0, 'L')
                        self.cell(30, 7, 'Categoría', 1, 0, 'L')
                        self.cell(40, 7, 'Medidas', 1, 0, 'L')
                        self.cell(40, 7, 'Ascendencia', 1, 0, 'L')
                        self.cell(0, 7, 'Estado', 1, 0, 'L')
                    def footer(self):
                        # Posición a 1.5 cm de abajo
                        self.set_y(-15)
                        # Arial italic 8
                        self.set_font('Arial', 'I', 8)
                        # Número de página
                        self.cell(0, 10, 'Página ' + str(self.page_no()) + ' de {nb}', 0, 0, 'C')
                pdf = PDF(orientation = 'L', unit = 'mm', format='Legal')
                pdf.alias_nb_pages()
                pdf.add_page()
                pdf.set_font('Arial', 'B', 10)
                # Consultar los animales y renderizar las filas
                animales = Animal.objects.select_related().filter(grupo_destino=1, estado=3).order_by('-fecha_vendible', '-id')
                pdf.set_font('Arial', '', 10)
                no_vendible = 0
                if len(animales) > 0 :
                    for animal in animales:
                        if animal.fecha_vendible is not None:
                            pdf.ln()
                            pdf.cell(15, 7, animal.id_Animal, 1, 0, 'L')
                            pdf.cell(30, 7, animal.fecha_nacimiento.strftime("%d/%m/%Y"), 1, 0, 'L')
                            pdf.cell(30, 7, 'Femenino' if animal.sexo=='F' else 'Masculino', 1, 0, 'L')
                            pdf.cell(40, 7, animal.raza.descripcion, 1, 0, 'L')
                            grupo_destino = animal.grupo_destino.descripcion
                            if (animal.grupo_destino.id == 1) and (animal.fecha_vendible != None):
                                grupo_destino += ' (' + animal.fecha_vendible.strftime("%d/%m/%Y") + ')'
                            pdf.cell(37, 7, grupo_destino, 1, 0, 'L')
                            fecha_vendible = ''
                            if (animal.fecha_vendible != None):
                                fecha_vendible = animal.fecha_vendible.strftime("%d/%m/%Y")
                            pdf.cell(35, 7, fecha_vendible, 1, 0, 'L')
                            castrado = 'No'
                            if (animal.castrado):
                                castrado = 'Sí'
                            pdf.cell(20, 7, castrado, 1, 0, 'L')
                            categoria = ''
                            if ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                                categoria = 'Ternero'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=180)):
                                categoria = 'Ternera' 
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'No')):
                                categoria = 'Torito'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730) and (castrado == 'Sí')):
                                categoria = 'Novillito'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>180) and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)<=730)):
                                categoria = 'Vaquillona'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'No')):
                                categoria = 'Toro'
                            elif ((animal.sexo == "M") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730) and (castrado == 'Sí')):
                                categoria = 'Novillo'
                            elif ((animal.sexo == "F") and (round((time.time() - time.mktime((animal.fecha_nacimiento).timetuple()))/86400)>730)):
                                categoria = 'Vaca'
                            pdf.cell(30, 7, categoria, 1, 0, 'L')
                            medidas = Medida.objects.select_related().filter(id_Animal_id=animal.id).order_by('-fecha_creacion', '-id')
                            if len(medidas) > 0 :
                                medida = medidas[0]
                                pdf.cell(40, 7, 'Peso: '+str(medida.pesoKG)+'. Altura: '+str(medida.alturaCM), 1, 0, 'L')
                            else:
                                pdf.cell(40, 7, '', 1, 0, 'C')
                            nacimiento = Nacimiento.objects.select_related().filter(id_Animal_id=animal.id).first()
                            if nacimiento != None:
                                pdf.cell(40, 7, 'Padre: '+nacimiento.id_Fecundacion.id_padre.id_Animal+'. Madre: '+nacimiento.id_Fecundacion.id_madre.id_Animal, 1, 0, 'L')
                            else:
                                pdf.cell(40, 7, '', 1, 0, 'L')
                            pdf.cell(0, 7, animal.estado.descripcion, 1, 0, 'L')
                        else:
                            no_vendible+=1
                elif ((len(animales) > 0) and (no_vendible==len(animales))) or (len(animales) == 0) :
                    pdf.ln()
                    pdf.cell(0, 7, "No se han encontrado registros", 1, 0, 'C')
                pdf.output(name='vendidos.pdf', dest='F')
                with open("vendidos.pdf", "rb") as pdf_file:
                    data['pdf'] = base64.b64encode(pdf_file.read()).decode("ascii")
                os.remove("vendidos.pdf")
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Animal'
        context['create_url'] = reverse_lazy('apl:animal_create')
        context['list_url'] = reverse_lazy('apl:animal_list')
        context['entity'] = 'Animales'
        return context


class AnimalCreateView(CreateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'animal/create.html'
    success_url = reverse_lazy('apl:animal_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Animal.add_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                form.save()
                return JsonResponse(form.instance.toJSON())
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:animal_list')
        context['action'] = 'add'
        return context


class AnimalUpdateView(UpdateView):
    model = Animal
    form_class = AnimalForm
    template_name = 'animal/create.html'
    success_url = reverse_lazy('apl:animal_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Animal.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            idAnimal = request.POST['id']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            elif action == 'vendible':
                animal = Animal.objects.filter(id=idAnimal).first()
                animal.fecha_vendible = datetime.now()
                data = (animal.save()).toJSON()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:animal_list')
        context['action'] = 'edit'
        return context


class AnimalDeleteView(DeleteView):
    model = Animal
    template_name = 'animal/delete.html'
    success_url = reverse_lazy('apl:animal_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Animal.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('apl:animal_list')
        return context


class AnimalFormView(FormView):
    form_class = Animal
    template_name = 'animal/delete.html'
    success_url = reverse_lazy('apl:animal_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Animal.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Animal'
        context['entity'] = 'Animales'
        context['list_url'] = reverse_lazy('sistema:animal_list')
        context['action'] = 'add'
        return context


### Fecundacion ###
class FecundacionListView(ListView):
    model = Fecundacion
    template_name = 'fecundacion/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Fecundacion.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = []
        try:
            action = request.POST['action']
            if action == 'searchdata':
                id = request.POST['id']
                if id != '':
                    for i in Fecundacion.objects.all():
                        data.append(i.toJSON())
                else:
                    for i in Fecundacion.objects.all():
                        data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                for i in Fecundacion.objects.select_related().filter(id_madre=animal):
                    json = i.toJSON()
                    json['raza_animal'] = i.id_padre.raza.descripcion
                    json['nacimiento_animal'] = i.id_padre.fecha_nacimiento
                    json['codigo_animal'] = i.id_padre.id_Animal
                    json['sexo_animal'] = i.id_padre.sexo
                    data.append(json)
                for i in Fecundacion.objects.select_related().filter(id_padre=animal):
                    json = i.toJSON()
                    json['raza_animal'] = i.id_madre.raza.descripcion
                    json['nacimiento_animal'] = i.id_madre.fecha_nacimiento
                    json['codigo_animal'] = i.id_madre.id_Animal
                    json['sexo_animal'] = i.id_madre.sexo
                    data.append(json)
            elif action == 'filterdataRM':
                animal = request.POST['animal']
                text = request.POST['text']
                page = request.POST['page']
                rpp = request.POST['rpp']
                data = []
                fecundaciones = Fecundacion.objects.filter(id_madre=animal)
                if fecundaciones.exists() :
                    fecundaciones = fecundaciones.filter(fecha__contains=text) | fecundaciones.filter(id_padre__id_Animal__contains=text) | fecundaciones.filter(id_padre__fecha_nacimiento__contains=text) | fecundaciones.filter(id_padre__raza__descripcion__contains=text)
                else :
                    fecundaciones = Fecundacion.objects.filter(id_padre=animal)
                    if fecundaciones.exists() :
                        fecundaciones = fecundaciones.filter(fecha__contains=text) | fecundaciones.filter(id_madre__id_Animal__contains=text) | fecundaciones.filter(id_madre__fecha_nacimiento__contains=text) | fecundaciones.filter(id_madre__raza__descripcion__contains=text)                
                paginator = Paginator(fecundaciones, rpp)
                try:
                    fecundaciones = paginator.page(page)
                except PageNotAnInteger:
                    fecundaciones = paginator.page(1)
                except EmptyPage:
                    fecundaciones = []
                for i in fecundaciones:
                    json = i.toJSON()
                    if i.id_padre.id_Animal == animal :
                        json['raza_animal'] = i.id_madre.raza.descripcion
                        json['nacimiento_animal'] = i.id_madre.fecha_nacimiento
                        json['codigo_animal'] = i.id_madre.id_Animal
                        json['sexo_animal'] = i.id_madre.sexo
                    else :
                        json['raza_animal'] = i.id_padre.raza.descripcion
                        json['nacimiento_animal'] = i.id_padre.fecha_nacimiento
                        json['codigo_animal'] = i.id_padre.id_Animal
                        json['sexo_animal'] = i.id_padre.sexo
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de fecundaciones'
        context['create_url'] = reverse_lazy('apl:fecundacion_create')
        context['list_url'] = reverse_lazy('apl:fecundacion_list')
        context['entity'] = 'fecundaciones'
        return context


class FecundacionCreateView(CreateView):
    model = Fecundacion
    form_class = FecundacionForm
    template_name = 'fecundacion/create.html'
    success_url = reverse_lazy('apl:fecundacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fecundacion.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Fecundacion'
        context['entity'] = 'fecundaciones'
        context['list_url'] = reverse_lazy('apl:fecundacion_list')
        context['action'] = 'add'
        return context


class FecundacionUpdateView(UpdateView):
    model = Fecundacion
    form_class = FecundacionForm
    template_name = 'fecundacion/create.html'
    success_url = reverse_lazy('apl:fecundacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fecundacion.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Fecundacion'
        context['entity'] = 'fecundaciones'
        context['list_url'] = reverse_lazy('apl:fecundacion_list')
        context['action'] = 'edit'
        return context


class FecundacionDeleteView(DeleteView):
    model = Fecundacion
    template_name = 'fecundacion/delete.html'
    success_url = reverse_lazy('apl:fecundacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fecundacion.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Fecundacion'
        context['entity'] = 'fecundaciones'
        context['list_url'] = reverse_lazy('apl:fecundacion_list')
        return context


### Palpacion ###
class PalpacionListView(ListView):
    model = Palpacion
    template_name = 'palpacion/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Palpacion.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = []
        try:
            action = request.POST['action']
            if action == 'searchdata':
                id = request.POST['id']
                if id != '':
                    for i in Palpacion.objects.all():
                        data.append(i.toJSON())
                else:
                    for i in Palpacion.objects.all():
                        data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                for i in Palpacion.objects.select_related().filter(id_fecundacion__id_madre=animal):
                    data.append(i.toJSON())
                for i in Palpacion.objects.select_related().filter(id_fecundacion__id_padre=animal):
                    data.append(i.toJSON())
            elif action == 'filterdataRM':
                animal = request.POST['animal']
                text = request.POST['text']
                page = request.POST['page']
                rpp = request.POST['rpp']
                data = []
                palpaciones = Palpacion.objects.filter(id_fecundacion__id_madre=animal)
                if palpaciones.exists() :
                    palpaciones = palpaciones.filter(fecha__contains=text) | palpaciones.filter(resultado__contains=text) | palpaciones.filter(observacion__contains=text) | palpaciones.filter(id_fecundacion__fecha__contains=text) | palpaciones.filter(id_fecundacion__id_padre__id_Animal__contains=text) | palpaciones.filter(id_fecundacion__id_padre__fecha_nacimiento__contains=text) | palpaciones.filter(id_fecundacion__id_padre__raza__descripcion__contains=text)
                else :
                    palpaciones = Palpacion.objects.filter(id_fecundacion__id_padre=animal)
                    if palpaciones.exists() :
                        palpaciones = palpaciones.filter(fecha__contains=text) | palpaciones.filter(resultado__contains=text) | palpaciones.filter(observacion__contains=text) | palpaciones.filter(id_fecundacion__fecha__contains=text) | palpaciones.filter(id_fecundacion__id_madre__id_Animal__contains=text) | palpaciones.filter(id_fecundacion__id_madre__fecha_nacimiento__contains=text) | palpaciones.filter(id_fecundacion__id_madre__raza__descripcion__contains=text)                
                paginator = Paginator(palpaciones, rpp)
                try:
                    palpaciones = paginator.page(page)
                except PageNotAnInteger:
                    palpaciones = paginator.page(1)
                except EmptyPage:
                    palpaciones = []
                for i in palpaciones:
                    json = i.toJSON()
                    json['fecha_fecundacion'] = i.id_fecundacion.fecha
                    if i.id_fecundacion.id_padre.id_Animal == animal :
                        json['raza_animal'] = i.id_fecundacion.id_madre.raza.descripcion
                        json['nacimiento_animal'] = i.id_fecundacion.id_madre.fecha_nacimiento
                        json['codigo_animal'] = i.id_fecundacion.id_madre.id_Animal
                        json['sexo_animal'] = i.id_fecundacion.id_madre.sexo
                    else :
                        json['raza_animal'] = i.id_fecundacion.id_padre.raza.descripcion
                        json['nacimiento_animal'] = i.id_fecundacion.id_padre.fecha_nacimiento
                        json['codigo_animal'] = i.id_fecundacion.id_padre.id_Animal
                        json['sexo_animal'] = i.id_fecundacion.id_padre.sexo
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Palpacion'
        context['create_url'] = reverse_lazy('apl:palpacion_create')
        context['list_url'] = reverse_lazy('apl:palpacion_list')
        context['entity'] = 'Palpaciones'
        return context


class PalpacionCreateView(CreateView):
    model = Palpacion
    form_class = PalpacionForm
    template_name = 'palpacion/create.html'
    success_url = reverse_lazy('apl:palpacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Palpacion.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Palpacion'
        context['entity'] = 'Palpacion'
        context['list_url'] = reverse_lazy('apl:palpacion_list')
        context['action'] = 'add'
        return context


class PalpacionUpdateView(UpdateView):
    model = Palpacion
    form_class = PalpacionForm
    template_name = 'palpacion/create.html'
    success_url = reverse_lazy('apl:Palpacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Palpacion.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Palpacion'
        context['entity'] = 'Palpaciones'
        context['list_url'] = reverse_lazy('apl:palpacion_list')
        context['action'] = 'edit'
        return context


class PalpacionDeleteView(DeleteView):
    model = Palpacion
    template_name = 'palpacion/delete.html'
    success_url = reverse_lazy('apl:palpacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Palpacion.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Palpacion'
        context['entity'] = 'Palpaciones'
        context['list_url'] = reverse_lazy('apl:palpacion_list')
        return context


### NACIMIENTO ###
class NacimientoListView(ListView):
    model = Nacimiento
    template_name = 'nacimiento/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Nacimiento.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = []
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Nacimiento.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                for i in Nacimiento.objects.select_related().filter(id_Fecundacion__in=Fecundacion.objects.select_related().filter(id_madre=animal)):
                    json = i.toJSON()
                    json['id_padre'] = i.id_Fecundacion.id_padre.id_Animal
                    json['id_fecundacion'] = i.id_Fecundacion.id
                    json['fecha_fecundacion'] = i.id_Fecundacion.fecha
                    json['raza_padre'] = i.id_Fecundacion.id_padre.raza.descripcion
                    json['nacimiento_padre'] = i.id_Fecundacion.id_padre.fecha_nacimiento
                    json['id_hijo'] = i.id_Animal.id_Animal
                    json['raza_hijo'] = i.id_Animal.raza.descripcion
                    json['sexo_hijo'] = i.id_Animal.sexo
                    json['destino_hijo'] = i.id_Animal.grupo_destino.descripcion
                    json['nacimiento_hijo'] = i.id_Animal.fecha_nacimiento
                    data.append(json)
                for i in Nacimiento.objects.select_related().filter(id_Fecundacion__in=Fecundacion.objects.select_related().filter(id_padre=animal)):
                    json = i.toJSON()
                    json['id_madre'] = i.id_Fecundacion.id_madre
                    json['id_fecundacion'] = i.id_Fecundacion.id
                    json['fecha_fecundacion'] = i.id_Fecundacion.fecha
                    json['raza_madre'] = i.id_Fecundacion.id_madre.raza.descripcion
                    json['nacimiento_madre'] = i.id_Fecundacion.id_madre.fecha_nacimiento
                    json['id_hijo'] = i.id_Animal.id_Animal
                    json['raza_hijo'] = i.id_Animal.raza.descripcion
                    json['sexo_hijo'] = i.id_Animal.sexo
                    json['destino_hijo'] = i.id_Animal.grupo_destino.descripcion
                    json['nacimiento_hijo'] = i.id_Animal.fecha_nacimiento
                    data.append(json)
            elif action == 'searchbycria':
                animal = request.POST['animal']
                for i in Nacimiento.objects.select_related().filter(id_Animal_id=animal):
                    json = i.toJSON()
                    json['id_fecundacion'] = i.id_Fecundacion.id
                    json['fecha_fecundacion'] = i.id_Fecundacion.fecha
                    json['id_padre'] = i.id_Fecundacion.id_padre.id_Animal
                    json['raza_padre'] = i.id_Fecundacion.id_padre.raza.descripcion
                    json['nacimiento_padre'] = i.id_Fecundacion.id_padre.fecha_nacimiento
                    json['id_madre'] = i.id_Fecundacion.id_madre.id_Animal
                    json['raza_madre'] = i.id_Fecundacion.id_madre.raza.descripcion
                    json['nacimiento_madre'] = i.id_Fecundacion.id_madre.fecha_nacimiento
                    data = json
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Nacimiento'
        context['create_url'] = reverse_lazy('apl:nacimiento_create')
        context['list_url'] = reverse_lazy('apl:nacimiento_list')
        context['entity'] = 'Nacimientos'
        return context


class NacimientoCreateView(CreateView):
    model = Nacimiento
    form_class = NacimientoForm
    template_name = 'nacimiento/create.html'
    success_url = reverse_lazy('apl:nacimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Nacimiento.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
                return JsonResponse(data)
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Nacimiento'
        context['entity'] = 'Nacimiento'
        context['list_url'] = reverse_lazy('apl:nacimiento_list')
        context['action'] = 'add'
        return context


class NacimientoUpdateView(UpdateView):
    model = Nacimiento
    form_class = NacimientoForm
    template_name = 'nacimiento/create.html'
    success_url = reverse_lazy('apl:nacimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Nacimiento.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Nacimiento'
        context['entity'] = 'Nacimiento'
        context['list_url'] = reverse_lazy('apl:nacimiento_list')
        context['action'] = 'edit'
        return context


class NacimientoDeleteView(DeleteView):
    model = Nacimiento
    template_name = 'nacimiento/delete.html'
    success_url = reverse_lazy('apl:nacimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Nacimiento.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Nacimiento'
        context['entity'] = 'Nacimientos'
        context['list_url'] = reverse_lazy('apl:nacimiento_list')
        return context


### Fallecimiento ###
class FallecimientoListView(ListView):
    model = Fallecimiento
    template_name = 'fallecimiento/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Fallecimiento.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Fallecimiento.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Fallecimiento'
        context['create_url'] = reverse_lazy('apl:fallecimiento_create')
        context['list_url'] = reverse_lazy('apl:fallecimiento_list')
        context['entity'] = 'Fallecimientos'
        return context


class FallecimientoCreateView(CreateView):
    model = Fallecimiento
    form_class = FallecimientoForm
    template_name = 'fallecimiento/create.html'
    success_url = reverse_lazy('apl:Fallecimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fallecimiento.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
                id_animal = data['id_Animal']
                animal = Animal.objects.filter(id=id_animal).first()
                animal.estado = Estado.objects.filter(id=2).first()
                animal.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Fallecimiento'
        context['entity'] = 'Nacimiento'
        context['list_url'] = reverse_lazy('apl:fallecimiento_list')
        context['action'] = 'add'
        return context


class FallecimientoUpdateView(UpdateView):
    model = Fallecimiento
    form_class = FallecimientoForm
    template_name = 'fallecimiento/create.html'
    success_url = reverse_lazy('apl:fallecimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fallecimiento.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Fallecimiento'
        context['entity'] = 'Fallecimiento'
        context['list_url'] = reverse_lazy('apl:fallecimiento_list')
        context['action'] = 'edit'
        return context


class FallecimientoDeleteView(DeleteView):
    model = Fallecimiento
    template_name = 'fallecimiento/delete.html'
    success_url = reverse_lazy('apl:fallecimiento_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Fallecimiento.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Fallecimiento'
        context['entity'] = 'Fallecimientos'
        context['list_url'] = reverse_lazy('apl:fallecimiento_list')
        return context


### TipoTPrev ###
class TipoTPrevListView(ListView):
    model = TipoTPrev
    template_name = 'tipotprev/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('TipoTPrev.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in TipoTPrev.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Tipo de Tratamiento Preventivo'
        context['create_url'] = reverse_lazy('apl:tipotprev_create')
        context['list_url'] = reverse_lazy('apl:tipotprev_list')
        context['entity'] = 'TipoTPrev'
        return context


class TipoTPrevCreateView(CreateView):
    model = TipoTPrev
    form_class = TipoTPrevForm
    template_name = 'tipotprev/create.html'
    success_url = reverse_lazy('apl:tipotprev_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('TipoTPrev.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Tipo de Tratamiento Preventivo'
        context['entity'] = 'Nacimiento'
        context['list_url'] = reverse_lazy('apl:tipotprev_list')
        context['action'] = 'add'
        return context


class TipoTPrevUpdateView(UpdateView):
    model = TipoTPrev
    form_class = TipoTPrevForm
    template_name = 'tipotprev/create.html'
    success_url = reverse_lazy('apl:tipotprev_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('TipoTPrev.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Tipo de Tratamiento Preventivo'
        context['entity'] = 'TipoTPrev'
        context['list_url'] = reverse_lazy('apl:tipotprev_list')
        context['action'] = 'edit'
        return context


class TipoTPrevDeleteView(DeleteView):
    model = TipoTPrev
    template_name = 'tipotprev/delete.html'
    success_url = reverse_lazy('apl:tipotprev_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('TipoTPrev.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Tipo de Tratamiento Preventivo'
        context['entity'] = 'TipoTPrev'
        context['list_url'] = reverse_lazy('apl:tipotprev_list')
        return context


### Preventivo ###
class PreventivoListView(ListView):
    model = TPreventivo
    template_name = 'preventivo/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Preventivo.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in TPreventivo.objects.all():
                    fila = i.toJSON()
                    fila["nombre_tipo"] = i.id_TipoTPrev.nombreTipo
                    data.append(fila)
            else:
                data = {}
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data = {}
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Tratamiento Preventivo'
        context['create_url'] = reverse_lazy('apl:preventivo_create')
        context['list_url'] = reverse_lazy('apl:preventivo_list')
        context['entity'] = 'TPreventivo'
        return context


class PreventivoCreateView(CreateView):
    model = TPreventivo
    form_class = PreventivoForm
    template_name = 'preventivo/create.html'
    success_url = reverse_lazy('apl:preventivo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Preventivo.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Tratamiento Preventivo'
        context['entity'] = 'TPreventivo'
        context['list_url'] = reverse_lazy('apl:preventivo_list')
        context['action'] = 'add'
        return context


class PreventivoUpdateView(UpdateView):
    model = TPreventivo
    form_class = PreventivoForm
    template_name = 'preventivo/create.html'
    success_url = reverse_lazy('apl:preventivo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Preventivo.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Tratamiento Preventivo'
        context['entity'] = 'TPreventivo'
        context['list_url'] = reverse_lazy('apl:preventivo_list')
        context['action'] = 'edit'
        return context


class PreventivoDeleteView(DeleteView):
    model = TPreventivo
    template_name = 'preventivo/delete.html'
    success_url = reverse_lazy('apl:preventivo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Preventivo.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Tratamiento Preventivo'
        context['entity'] = 'TPreventivo'
        context['list_url'] = reverse_lazy('apl:preventivo_list')
        return context
    
### AplicacionTP ###
class AplicacionTPListView(ListView):
    model = AplicacionTP
    template_name = 'aplicaciontp/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('AplicacionTP.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in AplicacionTP.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                data = []
                for i in AplicacionTP.objects.filter(id_Animal_id=animal):
                    json = i.toJSON()
                    json['nombreTipo'] = i.id_TPreventivo.id_TipoTPrev.nombreTipo
                    json['nombrePreventivo'] = i.id_TPreventivo.nombre
                    json['periodicidad'] = i.id_TPreventivo.periodicidad
                    data.append(json)
            else:
                data = {}
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data = {}
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Tratamiento Preventivo de Animal'
        context['create_url'] = reverse_lazy('apl:aplicaciontp_create')
        context['list_url'] = reverse_lazy('apl:aplicaciontp_list')
        context['entity'] = 'AplicacionTP'
        return context


class AplicacionTPCreateView(CreateView):
    model = AplicacionTP
    form_class = AplicacionTPForm
    template_name = 'aplicaciontp/create.html'
    success_url = reverse_lazy('apl:AplicacionTP_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AplicacionTP.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Tratamiento Preventivo de Animal'
        context['entity'] = 'AplicacionTP'
        context['list_url'] = reverse_lazy('apl:aplicaciontp_list')
        context['action'] = 'add'
        return context


class AplicacionTPUpdateView(UpdateView):
    model = AplicacionTP
    form_class = AplicacionTPForm
    template_name = 'aplicaciontp/create.html'
    success_url = reverse_lazy('apl:aplicaciontp_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AplicacionTP.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Tratamiento Preventivo de Animal'
        context['entity'] = 'AplicacionTP'
        context['list_url'] = reverse_lazy('apl:aplicaciontp_list')
        context['action'] = 'edit'
        return context


class AplicacionTPDeleteView(DeleteView):
    model = AplicacionTP
    template_name = 'aplicaciontp/delete.html'
    success_url = reverse_lazy('apl:aplicaciontp_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AplicacionTP.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Tratamiento Preventivo de Animal'
        context['entity'] = 'AplicacionTP'
        context['list_url'] = reverse_lazy('apl:aplicaciontp_list')
        return context

### Curativo ###
class CurativoListView(ListView):
    model = TCurativo
    template_name = 'curativo/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Curativo.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in TCurativo.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                data = []
                for i in TCurativo.objects.filter(id_Animal=animal):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Tratamiento Curativo'
        context['create_url'] = reverse_lazy('apl:curativo_create')
        context['list_url'] = reverse_lazy('apl:curativo_list')
        context['entity'] = 'TCurativo'
        return context
    
class CurativoCreateView(CreateView):
    model = TCurativo
    form_class = CurativoForm
    template_name = 'curativo/create.html'
    success_url = reverse_lazy('apl:curativo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Curativo.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Tratamiento Curativo'
        context['entity'] = 'TCurativo'
        context['list_url'] = reverse_lazy('apl:curativo_list')
        context['action'] = 'add'
        return context


class CurativoUpdateView(UpdateView):
    model = TCurativo
    form_class = CurativoForm
    template_name = 'curativo/create.html'
    success_url = reverse_lazy('apl:curativo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Curativo.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Tratamiento Curativo'
        context['entity'] = 'TCurativo'
        context['list_url'] = reverse_lazy('apl:curativo_list')
        context['action'] = 'edit'
        return context


class CurativoDeleteView(DeleteView):
    model = TCurativo
    template_name = 'curativo/delete.html'
    success_url = reverse_lazy('apl:curativo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Curativo.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Tratamiento Curativo'
        context['entity'] = 'TCurativo'
        context['list_url'] = reverse_lazy('apl:curativo_list')
        return context
    
### Perdida ###
class PerdidaListView(ListView):
    model = Perdida
    template_name = 'perdida/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Perdida.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = []
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Perdida.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                for i in Perdida.objects.select_related().filter(id_Palpacion__in=Palpacion.objects.select_related().filter(id_fecundacion__id_madre=animal)):
                    json = i.toJSON()
                    json['fecha_palpacion'] = i.id_Palpacion.fecha
                    json['resultado_palpacion'] = i.id_Palpacion.op_resultado
                    json['observacion_palpacion'] = i.id_Palpacion.observacion
                    data.append(json)
                for i in Perdida.objects.select_related().filter(id_Palpacion__in=Palpacion.objects.select_related().filter(id_fecundacion__id_padre=animal)):
                    json = i.toJSON()
                    json['fecha_palpacion'] = i.id_Palpacion.fecha
                    json['resultado_palpacion'] = i.id_Palpacion.op_resultado
                    json['observacion_palpacion'] = i.id_Palpacion.observacion
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Perdidas'
        context['create_url'] = reverse_lazy('apl:perdida_create')
        context['list_url'] = reverse_lazy('apl:perdida_list')
        context['entity'] = 'Perdidas'
        return context


class PerdidaCreateView(CreateView):
    model = Perdida
    form_class = PerdidaForm
    template_name = 'perdida/create.html'
    success_url = reverse_lazy('apl:perdida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Perdida.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Pérdida'
        context['entity'] = 'Perdida'
        context['list_url'] = reverse_lazy('apl:perdida_list')
        context['action'] = 'add'
        return context


class PerdidaUpdateView(UpdateView):
    model = Perdida
    form_class = PerdidaForm
    template_name = 'perdida/create.html'
    success_url = reverse_lazy('apl:perdida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Perdida.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición Perdida'
        context['entity'] = 'Perdida'
        context['list_url'] = reverse_lazy('apl:perdida_list')
        context['action'] = 'edit'
        return context


class PerdidaDeleteView(DeleteView):
    model = Perdida
    template_name = 'perdida/delete.html'
    success_url = reverse_lazy('apl:perdida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Perdida.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Perdida'
        context['entity'] = 'Perdida'
        context['list_url'] = reverse_lazy('apl:perdida_list')
        return context


### Medida ###
class MedidaListView(ListView):
    model = Medida
    template_name = 'medida/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Medida.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Medida.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyanimal':
                animal = request.POST['animal']
                data = []
                for i in Medida.objects.filter(id_Animal_id=animal):
                    data.append(i.toJSON())
            elif action == 'searchlastbyanimal':
                animal = request.POST['animal']
                data = {}
                data = Medida.objects.filter(id_Animal_id=animal).order_by('-fecha_creacion').first().toJSON()
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Medidas'
        context['create_url'] = reverse_lazy('apl:medida_create')
        context['list_url'] = reverse_lazy('apl:medida_list')
        context['entity'] = 'Medidas'
        return context


class MedidaCreateView(CreateView):
    model = Medida
    form_class = MedidaForm
    template_name = 'medida/create.html'
    success_url = reverse_lazy('apl:medida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Medida.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Medida'
        context['entity'] = 'Medida'
        context['list_url'] = reverse_lazy('apl:medida_list')
        context['action'] = 'add'
        return context


class MedidaUpdateView(UpdateView):
    model = Medida
    form_class = MedidaForm
    template_name = 'medida/create.html'
    success_url = reverse_lazy('apl:medida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Medida.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición Medida'
        context['entity'] = 'Medida'
        context['list_url'] = reverse_lazy('apl:medida_list')
        context['action'] = 'edit'
        return context


class MedidaDeleteView(DeleteView):
    model = Medida
    template_name = 'medida/delete.html'
    success_url = reverse_lazy('apl:medida_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Medida.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Medida'
        context['entity'] = 'Medida'
        context['list_url'] = reverse_lazy('apl:medida_list')
        return context


### Grupo Destino ###
class GrupoDestinoListView(ListView):
    model = GrupoDestino
    template_name = 'grupodestino/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('GrupoDestino.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in GrupoDestino.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Grupo Destino'
        context['create_url'] = reverse_lazy('apl:grupodestino_create')
        context['list_url'] = reverse_lazy('apl:grupodestino_list')
        context['entity'] = 'GrupoDestinos'
        return context


class GrupoDestinoCreateView(CreateView):
    model = GrupoDestino
    form_class = GrupoDestinoForm
    template_name = 'grupodestino/create.html'
    success_url = reverse_lazy('apl:grupodestino_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('GrupoDestino.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Grupo Destino'
        context['entity'] = 'GrupoDestinos'
        context['list_url'] = reverse_lazy('apl:grupodestino_list')
        context['action'] = 'add'
        return context


class GrupoDestinoUpdateView(UpdateView):
    model = GrupoDestino
    form_class = GrupoDestinoForm
    template_name = 'grupodestino/create.html'
    success_url = reverse_lazy('apl:grupodestino_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('GrupoDestino.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Grupo Destino'
        context['entity'] = 'GrupoDestinos'
        context['list_url'] = reverse_lazy('apl:grupodestino_list')
        context['action'] = 'edit'
        return context


class GrupoDestinoDeleteView(DeleteView):
    model = GrupoDestino
    template_name = 'grupodestino/delete.html'
    success_url = reverse_lazy('apl:grupodestino_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('GrupoDestino.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Grupo Destino'
        context['entity'] = 'GrupoDestinos'
        context['list_url'] = reverse_lazy('apl:grupodestino_list')
        return context


class GrupoDestinoFormView(FormView):
    form_class = GrupoDestino
    template_name = 'grupodestino/delete.html'
    success_url = reverse_lazy('apl:grupodestino_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('GrupoDestino.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Grupo Destino'
        context['entity'] = 'GrupoDestino'
        context['list_url'] = reverse_lazy('apl:grupodestino_list')
        context['action'] = 'add'
        return context


### Estado ###
class EstadoListView(ListView):
    model = Estado
    template_name = 'estado/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Estado.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Estado.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Estado'
        context['create_url'] = reverse_lazy('apl:estado_create')
        context['list_url'] = reverse_lazy('apl:estado_list')
        context['entity'] = 'Estados'
        return context


class EstadoCreateView(CreateView):
    model = Estado
    form_class = EstadoForm
    template_name = 'estado/create.html'
    success_url = reverse_lazy('apl:estado_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estado.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Estado'
        context['entity'] = 'Estados'
        context['list_url'] = reverse_lazy('apl:estado_list')
        context['action'] = 'add'
        return context


class EstadoUpdateView(UpdateView):
    model = Estado
    form_class = EstadoForm
    template_name = 'estado/create.html'
    success_url = reverse_lazy('apl:estado_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estado.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Estado'
        context['entity'] = 'Estados'
        context['list_url'] = reverse_lazy('apl:estado_list')
        context['action'] = 'edit'
        return context


class EstadoDeleteView(DeleteView):
    model = Estado
    template_name = 'estado/delete.html'
    success_url = reverse_lazy('apl:estado_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estado.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Estado'
        context['entity'] = 'Estados'
        context['list_url'] = reverse_lazy('apl:estado_list')
        return context


class EstadoFormView(FormView):
    form_class = Estado
    template_name = 'estado/delete.html'
    success_url = reverse_lazy('apl:estado_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estado.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Estado'
        context['entity'] = 'Estado'
        context['list_url'] = reverse_lazy('apl:estado_list')
        context['action'] = 'add'
        return context

### Articulo ###
class ArticuloListView(ListView):
    model = Articulo
    template_name = 'articulo/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Articulo.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Articulo'
        context['create_url'] = reverse_lazy('apl:articulo_create')
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['entity'] = 'Articulos'
        return context


class ArticuloCreateView(CreateView):
    model = Articulo
    form_class = ArticuloForm
    template_name = 'articulo/create.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Artículo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'add'
        return context


class ArticuloUpdateView(UpdateView):
    model = Articulo
    form_class = ArticuloForm
    template_name = 'articulo/create.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Articulo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'edit'
        return context


class ArticuloDeleteView(DeleteView):
    model = Articulo
    template_name = 'articulo/delete.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Articulo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        return context


class ArticuloFormView(FormView):
    form_class = Articulo
    template_name = 'articulo/delete.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Articulo'
        context['entity'] = 'Articulo'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'add'
        return context

### Articulo ###
class ArticuloListView(ListView):
    model = Articulo
    template_name = 'articulo/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Articulo.objects.all():
                    data.append(i.toJSON())
            elif action == 'filterdataRM':
                text = request.POST['text']
                page = request.POST['page']
                rpp = request.POST['rpp']
                data = []
                articulos = Articulo.objects.filter()
                articulos = articulos.filter(nombre__contains=text)
                paginator = Paginator(articulos, rpp)
                try:
                    articulos = paginator.page(page)
                except PageNotAnInteger:
                    articulos = paginator.page(1)
                except EmptyPage:
                    articulos = []
                for i in articulos:
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Articulo'
        context['create_url'] = reverse_lazy('apl:articulo_create')
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['entity'] = 'Articulos'
        return context


class ArticuloCreateView(CreateView):
    model = Articulo
    form_class = ArticuloForm
    template_name = 'articulo/create.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Artículo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'add'
        return context


class ArticuloUpdateView(UpdateView):
    model = Articulo
    form_class = ArticuloForm
    template_name = 'articulo/create.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Articulo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'edit'
        return context


class ArticuloDeleteView(DeleteView):
    model = Articulo
    template_name = 'articulo/delete.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Articulo'
        context['entity'] = 'Articulos'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        return context


class ArticuloFormView(FormView):
    form_class = Articulo
    template_name = 'articulo/delete.html'
    success_url = reverse_lazy('apl:articulo_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Articulo.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Articulo'
        context['entity'] = 'Articulo'
        context['list_url'] = reverse_lazy('apl:articulo_list')
        context['action'] = 'add'
        return context

### Estimacion ###
class EstimacionListView(ListView):
    model = Estimacion
    template_name = 'estimacion/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Estimacion.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Estimacion.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Estimación'
        context['create_url'] = reverse_lazy('apl:estimacion_create')
        context['list_url'] = reverse_lazy('apl:estimacion_list')
        context['entity'] = 'Estimaciones'
        return context


class EstimacionCreateView(CreateView):
    model = Estimacion
    form_class = EstimacionForm
    template_name = 'estimacion/create.html'
    success_url = reverse_lazy('apl:estimacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estimacion.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Estimación'
        context['entity'] = 'Estimaciones'
        context['list_url'] = reverse_lazy('apl:estimacion_list')
        context['action'] = 'add'
        return context


class EstimacionUpdateView(UpdateView):
    model = Estimacion
    form_class = EstimacionForm
    template_name = 'estimacion/create.html'
    success_url = reverse_lazy('apl:estimacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estimacion.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Estimación'
        context['entity'] = 'Estimaciones'
        context['list_url'] = reverse_lazy('apl:estimacion_list')
        context['action'] = 'edit'
        return context


class EstimacionDeleteView(DeleteView):
    model = Estimacion
    template_name = 'estimacion/delete.html'
    success_url = reverse_lazy('apl:estimacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estimacion.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Estimación'
        context['entity'] = 'Estimaciones'
        context['list_url'] = reverse_lazy('apl:estimacion_list')
        return context


class EstimacionFormView(FormView):
    form_class = Estimacion
    template_name = 'estimacion/delete.html'
    success_url = reverse_lazy('apl:estimacion_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Estimacion.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Estimación'
        context['entity'] = 'Estimacion'
        context['list_url'] = reverse_lazy('apl:estimacion_list')
        context['action'] = 'add'
        return context
    
### Estimacion Detalle ###
class EstimacionDetalleListView(ListView):
    model = DetalleEstimacion
    template_name = 'estimacion_detalle/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('EstimacionDetalle.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in DetalleEstimacion.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyestimacion':
                estimacion = request.POST['estimacion']
                data = []
                for i in DetalleEstimacion.objects.filter(id_Estimacion_id=estimacion):
                    json = i.toJSON()
                    json['id_detalle_estimacion'] = i.id
                    json['id_articulo'] = i.id_Articulo.id
                    json['nombre_articulo'] = i.id_Articulo.nombre
                    json['cantidad_actual_articulo'] = i.id_Articulo.cantidadActual
                    json['cantidad_ideal_articulo'] = i.id_Articulo.cantidadIdeal
                    json['id_estimacion'] = i.id_Estimacion.id
                    json['fecha_creacion'] = i.fecha_creacion
                    json['cantidad'] = i.cantidad
                    json['comprado'] = i.comprado
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Detalle Estimación'
        context['create_url'] = reverse_lazy('apl:estimacion_detalle_create')
        context['list_url'] = reverse_lazy('apl:estimacion_detalle_list')
        context['entity'] = 'Detalles de Estimaciones'
        return context


class EstimacionDetalleCreateView(CreateView):
    model = DetalleEstimacion
    form_class = EstimacionDetalleForm
    template_name = 'estimacion_detalle/create.html'
    success_url = reverse_lazy('apl:estimacion_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('EstimacionDetalle.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e.with_traceback())
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Detalle una Estimación'
        context['entity'] = 'EstimacionesDetalles'
        context['list_url'] = reverse_lazy('apl:estimacion_detalle_list')
        context['action'] = 'add'
        return context


class EstimacionDetalleUpdateView(UpdateView):
    model = DetalleEstimacion
    form_class = EstimacionDetalleForm
    template_name = 'estimacion_detalle/create.html'
    success_url = reverse_lazy('apl:estimacion_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('EstimacionDetalle.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Detalle de una Estimación'
        context['entity'] = 'EstimacionesDetalles'
        context['list_url'] = reverse_lazy('apl:estimacion_detalle_list')
        context['action'] = 'edit'
        return context


class EstimacionDetalleDeleteView(DeleteView):
    model = DetalleEstimacion
    template_name = 'estimacion_detalle/delete.html'
    success_url = reverse_lazy('apl:estimacion_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('EstimacionDetalle.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Detalle de una Estimación'
        context['entity'] = 'EstimacionesDetalles'
        context['list_url'] = reverse_lazy('apl:estimacion_detalle_list')
        return context


class EstimacionDetalleFormView(FormView):
    form_class = DetalleEstimacion
    template_name = 'estimacion_detalle/delete.html'
    success_url = reverse_lazy('apl:estimacion_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('EstimacionDetalle.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Detalle de Estimación'
        context['entity'] = 'EstimacionDetalle'
        context['list_url'] = reverse_lazy('apl:estimacion_detalle_list')
        context['action'] = 'add'
        return context
    
### Compra ###
class CompraListView(ListView):
    model = Compra
    template_name = 'compra/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Compra.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Compra.objects.all():
                    json = i.toJSON()
                    json['id_compra'] = i.id
                    json['id_vendedor'] = i.vendedor.id
                    json['nombre_vendedor'] = i.vendedor.nombre
                    json['id_estimacion'] = i.estimacion.id
                    json['fecha_estimacion'] = i.estimacion.fechaEstimacion
                    json['estado'] = i.estado
                    json['fecha_confirmado'] = i.fecha_confirmado
                    json['fecha_recibido'] = i.fecha_recibido
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Compra'
        context['create_url'] = reverse_lazy('apl:compra_create')
        context['list_url'] = reverse_lazy('apl:compra_list')
        context['entity'] = 'Compras'
        return context


class CompraCreateView(CreateView):
    model = Compra
    form_class = CompraForm
    template_name = 'compra/create.html'
    success_url = reverse_lazy('apl:compra_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Compra.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Compra'
        context['entity'] = 'Compras'
        context['list_url'] = reverse_lazy('apl:compra_list')
        context['action'] = 'add'
        return context


class CompraUpdateView(UpdateView):
    model = Compra
    form_class = CompraForm
    template_name = 'compra/create.html'
    success_url = reverse_lazy('apl:compra_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Compra.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            elif action == 'confirmar':
                id_compra = request.POST['id']
                compra = Compra.objects.filter(id=id_compra).first()
                compra.fecha_confirmado = datetime.now()
                compra.estado = 'C'
                compra.save()
            elif action == 'recibir':
                id_compra = request.POST['id']
                compra = Compra.objects.filter(id=id_compra).first()
                compra.fecha_recibido = datetime.now()
                compra.estado = 'R'
                compra.save()
                for i in DetalleCompra.objects.filter(id_Compra_id=id_compra):
                    articulo = Articulo.objects.filter(id=i.id_Articulo_id).first()
                    articulo.cantidadActual = (articulo.cantidadActual + i.cantidad)
                    articulo.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Compra'
        context['entity'] = 'Compras'
        context['list_url'] = reverse_lazy('apl:compra_list')
        context['action'] = 'edit'
        return context


class CompraDeleteView(DeleteView):
    model = Compra
    template_name = 'compra/delete.html'
    success_url = reverse_lazy('apl:compra_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Compra.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Compra'
        context['entity'] = 'Compras'
        context['list_url'] = reverse_lazy('apl:compra_list')
        return context


class CompraFormView(FormView):
    form_class = Compra
    template_name = 'compra/delete.html'
    success_url = reverse_lazy('apl:compra_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Compra.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Compra'
        context['entity'] = 'Compra'
        context['list_url'] = reverse_lazy('apl:compra_list')
        context['action'] = 'add'
        return context
    
### Compra Detalle ###
class CompraDetalleListView(ListView):
    model = DetalleCompra
    template_name = 'compra_detalle/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in DetalleCompra.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbycompra':
                compra = request.POST['compra']
                data = []
                for i in DetalleCompra.objects.filter(id_Compra_id=compra):
                    json = i.toJSON()
                    json['id'] = i.id
                    json['id_articulo'] = i.id_Articulo.id
                    json['nombre_articulo'] = i.id_Articulo.nombre
                    json['cantidad_actual_articulo'] = i.id_Articulo.cantidadActual
                    json['cantidad_ideal_articulo'] = i.id_Articulo.cantidadIdeal
                    json['id_compra'] = i.id_Compra.id
                    json['fecha_creacion'] = i.fecha_creacion
                    json['cantidad'] = i.cantidad
                    # Agregar los demás campos...
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Detalle Compra'
        context['create_url'] = reverse_lazy('apl:compra_detalle_create')
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        context['entity'] = 'Detalles de Compras'
        return context


class CompraDetalleCreateView(CreateView):
    model = DetalleCompra
    form_class = CompraDetalleForm
    template_name = 'compra_detalle/create.html'
    success_url = reverse_lazy('apl:compra_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e.with_traceback())
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Detalle una Compra'
        context['entity'] = 'ComprasDetalles'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        context['action'] = 'add'
        return context


class CompraDetalleUpdateView(UpdateView):
    model = DetalleCompra
    form_class = CompraDetalleForm
    template_name = 'compra_detalle/create.html'
    success_url = reverse_lazy('apl:compra_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Detalle de una Compra'
        context['entity'] = 'ComprasDetalles'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        context['action'] = 'edit'
        return context


class CompraDetalleDeleteView(DeleteView):
    model = DetalleCompra
    template_name = 'compra_detalle/delete.html'
    success_url = reverse_lazy('apl:compra_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Detalle de una Compra'
        context['entity'] = 'ComprasDetalles'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        return context


class CompraDetalleFormView(FormView):
    form_class = DetalleCompra
    template_name = 'compra_detalle/delete.html'
    success_url = reverse_lazy('apl:compra_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Detalle de Compra'
        context['entity'] = 'CompraDetalle'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        context['action'] = 'add'
        return context
    
### Loteventa ###
class LoteventaListView(ListView):
    model = LoteVenta
    template_name = 'loteventa/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('Loteventa.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in LoteVenta.objects.all():
                    recepcionado = True
                    for r in AnimalesLote.objects.filter(id_Lote=i.id).all():
                        if r.pesoRecepcion == None:
                            recepcionado = False
                    json = i.toJSON()
                    json['id_loteventa'] = i.id
                    json['id_comprador'] = i.id_Comprador.id
                    json['nombre_comprador'] = i.id_Comprador.nombre
                    json['estado'] = i.estado
                    json['fecha_confirmado'] = i.fecha_confirmado
                    json['fecha_envio'] = i.fecha_envio
                    json['recepcionado'] = recepcionado
                    data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Pedido de Lote'
        context['create_url'] = reverse_lazy('apl:loteventa_create')
        context['list_url'] = reverse_lazy('apl:loteventa_list')
        context['entity'] = 'Loteventas'
        return context


class LoteventaCreateView(CreateView):
    model = LoteVenta
    form_class = LoteVentaForm
    template_name = 'loteventa/create.html'
    success_url = reverse_lazy('apl:loteventa_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Loteventa.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Pedido de Lote'
        context['entity'] = 'Loteventas'
        context['list_url'] = reverse_lazy('apl:loteventa_list')
        context['action'] = 'add'
        return context


class LoteventaUpdateView(UpdateView):
    model = LoteVenta
    form_class = LoteVentaForm
    template_name = 'loteventa/create.html'
    success_url = reverse_lazy('apl:loteventa_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Loteventa.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            elif action == 'confirmar':
                id_loteventa = request.POST['id']
                loteventa = LoteVenta.objects.filter(id=id_loteventa).first()
                loteventa.fecha_confirmado = datetime.now()
                loteventa.estado = 'C'
                loteventa.save()
            elif action == 'enviar':
                id_loteventa = request.POST['id']
                loteventa = LoteVenta.objects.filter(id=id_loteventa).first()
                loteventa.fecha_envio = datetime.now()
                loteventa.estado = 'E'
                loteventa.save()
            elif action == 'finalizar':
                id_loteventa = request.POST['id']
                loteventa = LoteVenta.objects.filter(id=id_loteventa).first()
                loteventa.fecha_recepcion = datetime.now()
                loteventa.estado = 'F'
                loteventa.save()
                for i in AnimalesLote.objects.filter(id_Lote_id=id_loteventa):
                    animal = Animal.objects.filter(id=i.id_Animal_id).first()
                    animal.estado = Estado.objects.filter(id=3).first()
                    animal.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Pedido de Lote'
        context['entity'] = 'LoteVenta'
        context['list_url'] = reverse_lazy('apl:loteventa_list')
        context['action'] = 'edit'
        return context


class LoteventaDeleteView(DeleteView):
    model = LoteVenta
    template_name = 'loteventa/delete.html'
    success_url = reverse_lazy('apl:loteventa_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Loteventa.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Pedido de Lote'
        context['entity'] = 'Loteventas'
        context['list_url'] = reverse_lazy('apl:loteventa_list')
        return context


class LoteventaFormView(FormView):
    form_class = LoteVenta
    template_name = 'loteventa/delete.html'
    success_url = reverse_lazy('apl:loteventa_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('Loteventa.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Pedido de Lote'
        context['entity'] = 'Loteventa'
        context['list_url'] = reverse_lazy('apl:loteventa_list')
        context['action'] = 'add'
        return context
    
### Animales Lote ###
class AnimalLoteListView(ListView):
    model = AnimalesLote
    template_name = 'animal_lote/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    @method_decorator(permission_required('AnimalLote.view_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in AnimalesLote.objects.all():
                    data.append(i.toJSON())
            elif action == 'searchbyventa':
                venta = request.POST['venta']
                data = []
                for i in AnimalesLote.objects.filter(id_Lote_id=venta):
                    medida = Medida.objects.filter(id_Animal_id=i.id_Animal.id).order_by('-fecha_creacion').first()
                    if medida != None:
                        json = i.toJSON()
                        json['id'] = i.id
                        json['id_animal'] = i.id_Animal.id
                        json['codigo_animal'] = i.id_Animal.id_Animal
                        json['raza_animal'] = i.id_Animal.raza.descripcion
                        json['sexo_animal'] = i.id_Animal.sexo
                        json['pesoKG'] = medida.pesoKG
                        json['nacimiento_animal'] = i.id_Animal.fecha_nacimiento
                        json['vendible_animal'] = i.id_Animal.fecha_vendible
                        json['id_lote'] = i.id_Lote.id
                        json['fecha_creacion'] = i.fecha_creacion
                        data.append(json)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Lote de Animales'
        context['create_url'] = reverse_lazy('apl:animal_lote_create')
        context['list_url'] = reverse_lazy('apl:animal_lote_list')
        context['entity'] = 'AnimalesLote'
        return context


class AnimalLoteCreateView(CreateView):
    model = AnimalesLote
    form_class = AnimalLoteForm
    template_name = 'animal_lote/create.html'
    success_url = reverse_lazy('apl:animal_lote_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AnimalLote.add_env',raise_exception=True))
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            elif action == 'recepcionar':
                idAnimalLote = request.POST['id']
                pesoRecepcion = request.POST['pesoRecepcion']
                vivoRecepcion = request.POST['vivoRecepcion']
                observacionRecepcion = request.POST['observacionRecepcion']
                animalLote = AnimalesLote.objects.filter(id=idAnimalLote).first()
                animalLote.pesoRecepcion = pesoRecepcion
                animalLote.vivoRecepcion = vivoRecepcion
                animalLote.observacionRecepcion = observacionRecepcion
                animalLote.subtotalGSRecepcion = int(pesoRecepcion) * int(animalLote.id_Lote.precioKG)
                animalLote = animalLote.save()
                data = animalLote.toJSON() if animalLote else {} 
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e.with_traceback())
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de un Lote de Animal'
        context['entity'] = 'AnimalesLote'
        context['list_url'] = reverse_lazy('apl:animal_lote_list')
        context['action'] = 'add'
        return context


class AnimalLoteUpdateView(UpdateView):
    model = AnimalesLote
    form_class = AnimalLoteForm
    template_name = 'animal_lote/create.html'
    success_url = reverse_lazy('apl:animal_lote_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AnimalLote.change_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Lote de Animal'
        context['entity'] = 'AnimalesLote'
        context['list_url'] = reverse_lazy('apl:animal_lote_list')
        context['action'] = 'edit'
        return context


class AnimalLoteDeleteView(DeleteView):
    model = AnimalesLote
    template_name = 'animal_lote/delete.html'
    success_url = reverse_lazy('apl:animal_lote_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('AnimalLote.delete_env',raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Detalle de una Compra'
        context['entity'] = 'ComprasDetalles'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        return context


class CompraDetalleFormView(FormView):
    form_class = DetalleCompra
    template_name = 'compra_detalle/delete.html'
    success_url = reverse_lazy('apl:compra_detalle_list')

    @method_decorator(login_required)
    @method_decorator(permission_required('CompraDetalle.view_env',raise_exception=True))
    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Form | Detalle de Compra'
        context['entity'] = 'CompraDetalle'
        context['list_url'] = reverse_lazy('apl:compra_detalle_list')
        context['action'] = 'add'
        return context