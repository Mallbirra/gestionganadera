from django.db import models
from datetime import datetime
from django.forms import model_to_dict


class Raza(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', unique=True)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha de Creación')
    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.descripcion

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Raza'
        verbose_name_plural = 'Razas'
        db_table = 'raza'
        ordering = ['id']


class GrupoDestino(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', unique=True)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha de Creación')
    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.descripcion

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'GrupoDestino'
        verbose_name_plural = 'GrupoDestinos'
        db_table = 'GrupoDestino'
        ordering = ['id']


class Estado(models.Model):
    descripcion = models.CharField(max_length=100, verbose_name='Descripción', unique=True)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha de Creación')
    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.descripcion

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        db_table = 'Estado'
        ordering = ['id']


class Animal(models.Model):
    raza = models.ForeignKey(Raza, on_delete=models.PROTECT, verbose_name='Raza')
    id_Animal = models.CharField(max_length=50, verbose_name='Id Animal', unique=True)
    fecha_nacimiento = models.DateField(default=datetime.now, verbose_name='Fecha Nacimiento')
    fecha_vendible = models.DateField(null=True, verbose_name='Fecha Vendible')
    grupo_destino = models.ForeignKey(GrupoDestino, on_delete=models.PROTECT, related_name='destino', verbose_name='Grupo Destino')
    op_sexo = (
        ('M', 'MASCULINO'),
        ('F', 'FEMENINO')
    )
    sexo = models.CharField(max_length=1, choices=op_sexo, default='M', verbose_name='Sexo')
    op_estado = (
        ('V', 'VIVO'),
        ('M', 'MUERTO'),
        ('E', 'VENDIDO')
    )
    estado = models.ForeignKey(Estado, on_delete=models.PROTECT, related_name='estado', verbose_name='Estado')
    op_castrado = (
        (True, 'Sí'),
        (False, 'No')
    )
    castrado = models.BooleanField(choices=op_castrado, verbose_name='Castrado', null=False, default=False)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')
    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.id_Animal

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Animal'
        verbose_name_plural = 'Animales'
        db_table = 'animal'
        ordering = ['id']


class Fecundacion(models.Model):
    id_madre = models.ForeignKey(Animal, on_delete=models.PROTECT, related_name='id_madre_id', verbose_name='Madre')
    id_padre = models.ForeignKey(Animal, on_delete=models.PROTECT, related_name='id_padre_id', verbose_name='Padre')
    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(str(self.id))

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Fecundacion'
        verbose_name_plural = 'Fecundacion'
        db_table = 'fecundacion'
        ordering = ['id']


class Palpacion(models.Model):
    id_fecundacion = models.ForeignKey(Fecundacion, on_delete=models.PROTECT, verbose_name='Fecundacion')
    op_resultado = (
        ('V', 'VIVO'),
        ('M', 'MUERTO')
    )
    resultado = models.CharField(max_length=1, choices=op_resultado, default='E', verbose_name='Resultado')
    observacion = models.CharField(max_length=100, verbose_name='Observación', null=True, blank=True)
    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')

    #    operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(str(self.id))

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Palpacion'
        verbose_name_plural = 'Palpacion'
        db_table = 'palpacion'
        ordering = ['id']


class Perdida(models.Model):
    id_Palpacion = models.ForeignKey(Palpacion, on_delete=models.PROTECT, verbose_name='Palpacion')
    causa = models.CharField(max_length=100, verbose_name='Causa')
    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Perdida'
        verbose_name_plural = 'Perdidas'
        db_table = 'perdida'
        ordering = ['id']


class Nacimiento(models.Model):
    id_Fecundacion = models.ForeignKey(Fecundacion, on_delete=models.PROTECT, verbose_name='Fecundacion')
    # Agregar el ID del animal una vez creado el registro.
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    fecha = models.DateField(default=datetime.now, verbose_name='Fecha')
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Nacimiento'
        verbose_name_plural = 'Nacimientos'
        db_table = 'nacimiento'
        ordering = ['id']


class Fallecimiento(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    causa = models.CharField(max_length=100, verbose_name='Causa', null=False)

    fecha = models.DateField(verbose_name='Fecha')
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Fallecimiento'
        verbose_name_plural = 'Fallecimientos'
        db_table = 'fallecimiento'
        ordering = ['id']


class Marcado(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Marcado'
        verbose_name_plural = 'Marcados'
        db_table = 'marcado'
        ordering = ['id']


class Medida(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    # fecha = models.DateField(default=datetime.now, verbose_name='Fecha', null=False)
    pesoKG = models.IntegerField(verbose_name='Peso en Kg')
    alturaCM = models.IntegerField(verbose_name='Altura en cm')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Medida'
        verbose_name_plural = 'Medidas'
        db_table = 'medida'
        ordering = ['id']


class TipoTPrev(models.Model):
    nombreTipo = models.CharField(max_length=100, verbose_name='Nombre Tipo')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.nombreTipo)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TipoTPrev'
        verbose_name_plural = 'TipoTPrevs'
        db_table = 'tipo_tprev'
        ordering = ['id']


class TCurativo(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    observacion = models.CharField(max_length=100, verbose_name='Observacion')
    fechaInicio = models.DateField(default=datetime.now, verbose_name='Fecha Inicio', null=False)
    fechaFin = models.DateField(verbose_name='Fecha Fin', null=False)
    periodicidad = models.IntegerField(verbose_name='Periodicidad')
    observacion = models.CharField(max_length=100, verbose_name='Observacion', blank=True)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TCurativo'
        verbose_name_plural = 'TCurativos'
        db_table = 'tcurativo'
        ordering = ['id']


class TPreventivo(models.Model):
    id_TipoTPrev = models.ForeignKey(TipoTPrev, on_delete=models.PROTECT, verbose_name='TipoTPrev')
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    periodicidad = models.IntegerField(verbose_name='Periodicidad')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.nombre)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'TPreventivo'
        verbose_name_plural = 'TPreventivos'
        db_table = 'tpreventivo'
        ordering = ['id']


class AplicacionTP(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    id_TPreventivo = models.ForeignKey(TPreventivo, on_delete=models.PROTECT, verbose_name='TPreventivo')
    fechaInicio = models.DateField(default=datetime.now, verbose_name='Fecha Inicio', null=False)
    fechaFin = models.DateField(verbose_name='Fecha Fin', null=False)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')
    observacion = models.CharField(max_length=100, verbose_name='Observacion', blank=True)

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'AplicacionTP'
        verbose_name_plural = 'AplicacionTPs'
        db_table = 'aplicacion_tp'
        ordering = ['id']


class Comprador(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    telefono = models.CharField(max_length=50, verbose_name='Telefono', null=False)
    mail = models.CharField(max_length=100, verbose_name='Mail', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.nombre)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Comprador'
        verbose_name_plural = 'Compradores'
        db_table = 'comprador'
        ordering = ['id']


class LoteVenta(models.Model):
    id_Comprador = models.ForeignKey(Comprador, on_delete=models.PROTECT, verbose_name='Comprador')
    precioKG = models.DecimalField(verbose_name='Precio por KG', null=False, decimal_places=0, max_digits=10)
    op_estado = (
        ('P', 'PREPARADO'),
        ('C', 'CONFIRMADO'),
        ('E', 'ENVIADO'),
        ('F', 'FINALIZADO'),
    )
    estado = models.CharField(max_length=1, choices=op_estado, default='P', verbose_name='Estado')
    fecha_envio = models.DateField(verbose_name='Fecha Envío', blank=True, null=True)
    fecha_recepcion = models.DateField(verbose_name='Fecha Recepción', blank=True, null=True)
    fecha_confirmado = models.DateField(verbose_name='Fecha Confirmado', blank=True, null=True)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'LoteVenta'
        verbose_name_plural = 'LoteVentas'
        db_table = 'lote_venta'
        ordering = ['id']


class AnimalesLote(models.Model):
    id_Animal = models.ForeignKey(Animal, on_delete=models.PROTECT, verbose_name='Animal')
    id_Lote = models.ForeignKey(LoteVenta, on_delete=models.PROTECT, verbose_name='Lote Venta')
    subtotalGS = models.DecimalField(verbose_name='Subtotal GS', null=False, decimal_places=0, max_digits=12)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')
    pesoRecepcion = models.IntegerField(verbose_name='Peso en Kg', blank=True, null=True)
    vivoRecepcion = models.BooleanField(verbose_name='Vivo', null=False, default=True)
    observacionRecepcion = models.CharField(max_length=100, verbose_name='Observación', blank=True, null=False)
    subtotalGSRecepcion = models.DecimalField(verbose_name='Subtotal GS Recepción', blank=True, default=0, decimal_places=0, max_digits=12)

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'AnimalesLote'
        verbose_name_plural = 'AnimalesLotes'
        db_table = 'animales_lote'
        ordering = ['id']


class Articulo(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    cantidadActual = models.IntegerField(verbose_name='Cantidad Actual', default=0, null=False)
    cantidadIdeal = models.IntegerField(verbose_name='Cantidad Ideal', default=0, null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Articulo'
        verbose_name_plural = 'Articulos'
        db_table = 'articulo'
        ordering = ['id']

class Estimacion(models.Model):
    fechaEstimacion = models.DateField(default=datetime.now, verbose_name='Fecha Estimacion')

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    def __str__(self):
        return str(self.fechaEstimacion)

    def toJSON(self):
        estimacion = model_to_dict(self)
        return estimacion

    class Meta:
        verbose_name = 'Estimación'
        verbose_name_plural = 'Estimaciones'
        db_table = 'estimacion'
        ordering = ['id']


class DetalleEstimacion(models.Model):
    id_Estimacion = models.ForeignKey(Estimacion, on_delete=models.PROTECT, verbose_name='Estimación')
    id_Articulo = models.ForeignKey(Articulo, on_delete=models.PROTECT, verbose_name='Artículo')
    cantidad = models.IntegerField(verbose_name='Cantidad', null=False)
    comprado = models.IntegerField(verbose_name='Comprado', null=False)
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'DetalleEstimacion'
        verbose_name_plural = 'DetalleEstimaciones'
        db_table = 'detalle_estimacion'
        ordering = ['id']

class Vendedor(models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre', null=False)
    telefono = models.CharField(max_length=50, verbose_name='Telefono', null=False)
    mail = models.CharField(max_length=100, verbose_name='Mail', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Vendedor'
        verbose_name_plural = 'Vendedores'
        db_table = 'vendedor'
        ordering = ['id']

class Compra(models.Model):
    vendedor = models.ForeignKey(Vendedor, on_delete=models.PROTECT, related_name='vendedor', verbose_name='Vendedor')
    estimacion = models.ForeignKey(Estimacion, on_delete=models.PROTECT, related_name='estimacion', verbose_name='Estimación')
    op_estado = (
        ('P', 'PENDIENTE'),
        ('C', 'CONFIRMADO'),
        ('R', 'RECIBIDO')
    )
    estado = models.CharField(max_length=1, choices=op_estado, default='P', verbose_name='Estado')
    fecha_confirmado = models.DateField(verbose_name='Fecha Confirmado')
    fecha_recibido = models.DateField(verbose_name='Fecha Recibido')
    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'
        db_table = 'compra'
        ordering = ['id']


class DetalleCompra(models.Model):
    id_Compra = models.ForeignKey(Compra, on_delete=models.PROTECT, verbose_name='Compra')
    id_Articulo = models.ForeignKey(Articulo, on_delete=models.PROTECT, verbose_name='Articulo')
    cantidad = models.IntegerField(verbose_name='Cantidad', null=False)

    fecha_creacion = models.DateField(default=datetime.now, verbose_name='Fecha Creación')

    # operador_creacion = CharField(max_length=100, verbose_name='Operador de Creación')

    def __str__(self):
        return str(self.id)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'DetalleCompra'
        verbose_name_plural = 'DetalleCompras'
        db_table = 'detalle_compra'
        ordering = ['id']
