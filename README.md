# Gestion Ganadera
## Para modificar datos de conexión a la base de datos debemos modificar el archivo SG/db.py.
## Buscamos y reemplazamos la ruta C:\Python311\ por la de la instalación local de python.
## Buscamos y reemplazamos la ruta C:\Users\dev\Documents\Python Projects\ por la de la carpeta local en donde se encuentra el proyecto.
## Utilizamos el comando activateENV.bat para activar el entorno virtual con la terminal CMD.
## Utilizamos el comando deactivateENV.bat para desactivar el entorno virtual con la terminal CMD.
## En caso de no poder ejecutar comandos pip, ejecutamos la siguiente sentencia: python -m pip install --upgrade pip
### Paquetes para instalar
- django
- pillow
- asgiref
- django-widget-tweaks
- pip
- psycopg2
- pytz
- setuptools
- six
- sqlparse
### Comandos
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver