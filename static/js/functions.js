function message_error(obj) {
    var html = '';
    if (typeof (obj) === 'object') {
        html = '<ul style="text-align: left;">';
        $.each(obj, function (key, value) {
            html += '<li>' + key + ': ' + value + '</li>';
        });
        html += '</ul>';
    }
    else{
        html = '<p>'+obj+'</p>';
    }
    Swal.fire({
        title: 'Error!',
        html: html,
        icon: 'error'
    });
}

function siguiente_elemento(actual, siguiente) {
    document.getElementById(actual).addEventListener("keyup", function(e) {
        if (e.key === "Enter") {
            e.preventDefault();
            if (["SELECT", "BUTTON"].includes(document.getElementById(siguiente).tagName)) {
                document.getElementById(siguiente).focus();
            } else {
                document.getElementById(siguiente).select();
            }
        }
    });
}